package de.jstojconverter.util;
import junit.framework.*;
import org.junit.runner.*;
import java.util.*;
import org.junit.runner.notification.*;

public class JUnitTestHelper extends RunListener {
	
	private static final String LOG_TAG = "JUnitTestHelper";
	
	private List<Class> mClasses;
	
	public JUnitTestHelper(Class<?> pClass) {
		mClasses = new ArrayList<Class>();
		mClasses.add(pClass);
	}
	
	public void run() {
		JUnitCore junit = new JUnitCore();
		junit.addListener(this);
		Class[] classes = new Class[mClasses.size()];
		mClasses.toArray(classes);
		junit.runClasses(classes);
	}
	
	@Override
	public void testRunStarted(Description pDescription) throws Exception {
		Log.d(LOG_TAG, "Test run started: " + pDescription.toString());
	}
	
	@Override
    public void testRunFinished(Result pResult) throws Exception {
		
	}
	
	@Override
    public void testStarted(Description pDescription) throws Exception {
		
	}
	
	@Override
    public void testFinished(Description pDescription) throws Exception {
		
	}
	
	@Override
    public void testFailure(Failure pFailure) throws Exception {
		
	}
	
	@Override
    public void testAssumptionFailure(Failure pFailure) {
		
	}
	
	@Override
    public void testIgnored(Description pDescription) throws Exception {
		
	}
}
