package de.jstojconverter.javacore.value;

import de.jstojconverter.javacore.info.JavaInfoException;
import junit.framework.TestCase;

/**
 * @author Felix Jordan
 * @since 30.07.2015 - 23:10:04
 * @version 1.0
 */
public class JavaArrayValueTest extends TestCase {
	
	public void testOneDimensionInitialValues() throws JavaInfoException {
		JavaArrayValue arrayValue = new JavaArrayValue("int", 5);
		assertEquals(arrayValue.getValue(), "new int[5]");
	}
	
	public void testMultiDimensionInitialValues() throws JavaInfoException {
		JavaArrayValue arrayValue = new JavaArrayValue("int[][]", 5, 2);
		assertEquals(arrayValue.getValue(), "new int[5][2][]");
	}
	
	public void testOneDimensionalEmptyValues() throws JavaInfoException {
		JavaArrayValue arrayValue = new JavaArrayValue("int");
		assertEquals(arrayValue.getValue(), "new int[] {}");
	}
	
	public void testOneDimensionalCustomValues() throws JavaInfoException {
		JavaArrayValue arrayValue = new JavaArrayValue("int",
				new JavaConstantValue("4", "int"),
				new JavaConstantValue("2", "int"));
		assertEquals(arrayValue.getValue(), "new int[] {4, 2}");
	}
}
