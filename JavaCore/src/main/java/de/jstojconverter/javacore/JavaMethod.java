package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaMethodInfo;
import de.jstojconverter.javacore.util.JavaParameter;

public class JavaMethod extends JavaCodeBlock {
	
	private JavaMethodInfo mInfo;
	private String mVisibility;
	private String mModifier;

	protected JavaMethod(String pVisibility, String pModifier, JavaMethodInfo pInfo) {
		super(pInfo);
		mVisibility = pVisibility;
		mModifier = pModifier;
		mInfo = pInfo;
	}

	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append(mVisibility);
		beginBuilder.append(" ");
		if (mModifier != null) {
			beginBuilder.append(mModifier);
			beginBuilder.append(" ");
		}
		beginBuilder.append(mInfo.getReturnType());
		beginBuilder.append(" ");
		beginBuilder.append(mInfo.getName());
		beginBuilder.append("(");
		
		JavaParameter[] parameters = mInfo.getParameters();
		for (int i=0; i<parameters.length; i++) {
			JavaParameter parameter = parameters[i];
			beginBuilder.append(parameter.getType());
			beginBuilder.append(" ");
			beginBuilder.append(parameter.getName());
			if (i < parameters.length - 1) {
				beginBuilder.append(",");
			}
		}
		beginBuilder.append(")");
		return beginBuilder.toString();
	}
}
