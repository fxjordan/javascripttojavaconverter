package de.jstojconverter.javacore;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.javacore.info.IJavaVariableContainer;
import de.jstojconverter.javacore.info.JavaInfoException;
import de.jstojconverter.javacore.info.JavaMethodInfo;
import de.jstojconverter.javacore.info.JavaTypeInfo;
import de.jstojconverter.javacore.info.JavaTypeManager;
import de.jstojconverter.javacore.info.JavaVariableInfo;
import de.jstojconverter.javacore.operators.JavaAssignmentOperator;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaAssignment;
import de.jstojconverter.javacore.value.JavaMethodInvocation;
import de.jstojconverter.javacore.value.JavaConstantValue;

public abstract class JavaCodeBlock extends JavaContent {
	
	private JavaCodeBlockInfo mInfo;
	private List<JavaContent> mContent;
	
	protected JavaCodeBlock(JavaCodeBlockInfo pInfo) {
		mInfo = pInfo;
		mContent = new ArrayList<JavaContent>();
	}
	
	public JavaCodeBlockInfo getInfo() {
		return mInfo;
	}
	
	public JavaDeclarationStatement createDeclarationStatement(String pName, String pType) throws JavaInfoException {
		JavaVariableInfo variable = mInfo.createVariable(pName, pType);
		JavaDeclarationStatement declaration =  new JavaDeclarationStatement(variable);
		super.attatchChild(declaration);
		mContent.add(declaration);
		return declaration;
	}
	
	public JavaDeclarationStatement createDeclarationStatement(String pName, String pType, IJavaValue pValue)
			throws JavaCoreException {
		JavaVariableInfo variable = mInfo.createVariable(pName, pType);
		if (!variable.getType().equals(pValue.getType())) {
			throw new JavaCoreException("Type mismatch: Cannot convert from " + pValue.getType() + " to " + variable.getType());
		}
		JavaDeclarationStatement declaration =  new JavaDeclarationStatement(variable, pValue);
		super.attatchChild(declaration);
		mContent.add(declaration);
		return declaration;
	}
	
	public JavaAssignmentStatement createAssignmentStatement(String pVariableName,
			JavaAssignmentOperator pOperator, IJavaValue pValue) throws JavaCoreException {
		JavaAssignmentStatement statement = new JavaAssignmentStatement(createAssignment(pVariableName, pOperator, pValue));
		super.attatchChild(statement);
		mContent.add(statement);
		return statement;
	}
	
	public JavaAssignment createAssignment(String pVariableName,
			JavaAssignmentOperator pOperator, IJavaValue pValue) throws JavaCoreException {
		String signature = JavaVariableInfo.createSignature(pVariableName);
		JavaVariableInfo variableInfo = mInfo.getVariable(signature);
		if (!variableInfo.getType().equals(pValue.getType())) {
			throw new JavaCoreException("Type mismatch: Cannot convert from " + pValue.getType() + " to " + variableInfo.getType());
		}
		return new JavaAssignment(pVariableName, pOperator, pValue);
	}
	
	public JavaMethodInvocationStatement createMethodInvocationStatement(String pTarget, String pMethodName,
			IJavaValue... pParameters) throws JavaCoreException {
		JavaMethodInvocation methodInvocation = createMethodInvocation(pTarget, pMethodName, pParameters);
		JavaMethodInvocationStatement statement = new JavaMethodInvocationStatement(methodInvocation);
		super.attatchChild(statement);
		mContent.add(statement);
		return statement;
	}
	
	public JavaMethodInvocationStatement createMethodInvocationStatement(
			JavaMethodInvocation pMethodInvocation) throws JavaCoreException {
		JavaMethodInvocationStatement statement = new JavaMethodInvocationStatement(pMethodInvocation);
		super.attatchChild(statement);
		mContent.add(statement);
		return statement;
	}
	
	public JavaMethodInvocation createMethodInvocation(String pTarget,
			String pMethodName, IJavaValue... pParameters) throws JavaCoreException {
		JavaTypeInfo targetType = getTargetType(mInfo, pTarget);
		JavaMethodInfo methodInfo = targetType.getMethod(pMethodName, JavaConstantValue.getTypeArray(pParameters));
		return new JavaMethodInvocation(pTarget, methodInfo.getName(), methodInfo.getReturnType(), pParameters);
	}
	
	private JavaTypeInfo getTargetType(IJavaVariableContainer pContainer, String pTarget) throws JavaCoreException {
		if (pTarget.length() == 0) {
			return pContainer.getParentType();
		}
		String[] parts = pTarget.split("\\.");
		String firstPart = parts[0];
		
		if (firstPart.equals("this")) {
			return pContainer.getParentType();
		}
		if (firstPart.equals("super")) {
			throw new JavaCoreException("Super calls are not supported yet!");
		}
		
		String newTarget;
		if (firstPart.equals(pTarget)) {
			newTarget = "";
		} else {
			newTarget = pTarget.substring(firstPart.length()+1);
		}
		
		if (pContainer.existVariable(firstPart)) {
			String variableType = pContainer.getVariable(firstPart).getType();
			return getTargetType(JavaTypeManager.getManager().getType(variableType), newTarget);
		} else {
			for (int i=0; i<parts.length; i++) {
				StringBuilder builder = new StringBuilder();
				for (int j=0; j<i+1; j++) {
					builder.append(parts[j]);
					if (j < i) {
						builder.append(".");
					}
				}
				String type = builder.toString();
				if (JavaTypeManager.getManager().existType(type)) {
					if (type.equals(pTarget)) {
						newTarget = "";
					} else {
						newTarget = pTarget.substring(type.length()+1);
					}
					return getTargetType(JavaTypeManager.getManager().getType(builder.toString()), newTarget);
				}
			}
			throw new JavaCoreException("Cannot find variable or class '" + firstPart + "'");
		}	
	}
	
	public JavaIfCondition createIf(IJavaValue pCondition) throws JavaCoreException {
		checkConditionType(pCondition);
		JavaIfCondition ifCondition =  new JavaIfCondition(mInfo.createCodeBlock(), pCondition);
		super.attatchChild(ifCondition);
		mContent.add(ifCondition);
		return ifCondition;
	}
	
	
	public JavaElseCondition createElse() {
		JavaElseCondition elseCondition = new JavaElseCondition(mInfo.createCodeBlock());
		super.attatchChild(elseCondition);
		mContent.add(elseCondition);
		return elseCondition;
	}
	
	
	public JavaElseIfCondition createElseIf(IJavaValue pCondition) throws JavaCoreException {
		checkConditionType(pCondition);
		JavaElseIfCondition elseIfCondition = new JavaElseIfCondition(mInfo.createCodeBlock(), pCondition);
		super.attatchChild(elseIfCondition);
		mContent.add(elseIfCondition);
		return elseIfCondition;
	}
	
	
	public JavaWhileLoop createWhile(IJavaValue pCondition) throws JavaCoreException {
		checkConditionType(pCondition);
		JavaWhileLoop whileLoop = new JavaWhileLoop(mInfo.createCodeBlock(), pCondition);
		super.attatchChild(whileLoop);
		mContent.add(whileLoop);
		return whileLoop;
	}
	
	// TODO Change condition to type JavaValue and check if type is boolean
	public JavaDoWhileLoop createDoWhile(String pCondition) {
		JavaDoWhileLoop doWhileLoop = new JavaDoWhileLoop(mInfo.createCodeBlock(), pCondition);
		super.attatchChild(doWhileLoop);
		mContent.add(doWhileLoop);
		return doWhileLoop;
	}
	
	// TODO Change condition to type JavaValue and check if type is boolean
	public JavaForLoop createFor(String pPreStatement, String pCondition, String pPostStatement) {
		JavaForLoop forLoop = new JavaForLoop(mInfo.createCodeBlock(), pPreStatement, pCondition, pPostStatement);
		super.attatchChild(forLoop);
		mContent.add(forLoop);
		return forLoop;
	}
	
	private void checkConditionType(IJavaValue pCondition) throws JavaCoreException {
		if (!pCondition.getType().equals("boolean")) {
			throw new JavaCoreException("Condition type must be type boolean");
		}
	}

	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(getBlockBegin());
		srcBuilder.append(" {\n    ");
		
		for (int i=0; i<mContent.size(); i++) {
			JavaContent content = mContent.get(i);
			String bodyText = content.getSourceCode().replace("\n", "\n    ");
			srcBuilder.append(bodyText);
		}
		srcBuilder.delete(srcBuilder.length()-5, srcBuilder.length());
		srcBuilder.append("\n}");
		String blockEnd = getBlockEnd();
		if (blockEnd != null) {
			srcBuilder.append(" ");
			srcBuilder.append(blockEnd);
		}
		srcBuilder.append("\n");
		return srcBuilder.toString();
	}
	
	protected abstract String getBlockBegin();
	
	protected String getBlockEnd() {
		return null;
	}
}