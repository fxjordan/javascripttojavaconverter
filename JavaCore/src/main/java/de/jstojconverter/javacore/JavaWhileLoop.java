package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.javacore.value.IJavaValue;

public class JavaWhileLoop extends JavaCodeBlock {
	
	private IJavaValue mCondition;
	
	protected JavaWhileLoop(JavaCodeBlockInfo pInfo, IJavaValue pCondition) {
		super(pInfo);
		mCondition = pCondition;
	}

	@Override
	protected String getBlockBegin() {
		return "while (" + mCondition.getValue() + ")";
	}
}
