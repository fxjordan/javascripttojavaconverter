package de.jstojconverter.javacore.value;


public class JavaVariable extends JavaAbstractValue {
	
	private String mName;
	private String mType;
	
	public JavaVariable(String pName, String pType) {
		mName = pName;
		mType = pType;
	}

	@Override
	public String getValue() {
		return mName;
	}

	@Override
	public String getType() {
		return mType;
	}

	@Override
	public boolean isVariable() {
		return true;
	}
}
