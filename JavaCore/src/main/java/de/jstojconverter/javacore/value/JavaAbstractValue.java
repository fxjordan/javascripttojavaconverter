package de.jstojconverter.javacore.value;

/**
 * @author Felix Jordan
 * @since 27.07.2015 - 23:09:46
 * @version 1.0
 */
public abstract class JavaAbstractValue implements IJavaValue {
	
	@Override
	public String toString() {
		return getValue();
	}
	
	@Override
	public boolean isVariable() {
		return false;
	}
	
	@Override
	public abstract String getValue();

	@Override
	public abstract String getType();
}
