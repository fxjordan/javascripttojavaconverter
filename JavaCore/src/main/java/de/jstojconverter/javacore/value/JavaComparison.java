package de.jstojconverter.javacore.value;

import de.jstojconverter.javacore.operators.JavaComparisonOperator;

public class JavaComparison extends JavaAbstractValue {
	
	private IJavaValue mValueA;
	private IJavaValue mValueB;
	private JavaComparisonOperator mOperator;
	
	public JavaComparison(IJavaValue pValueA, IJavaValue pValueB, JavaComparisonOperator pOperator) {
		mValueA = pValueA;
		mValueB = pValueB;
		mOperator = pOperator;
	}

	@Override
	public String getValue() {
		return mValueA.getValue() + " " + mOperator.toString() + " " + mValueB.getValue();
	}

	@Override
	public String getType() {
		return "boolean";
	}

	@Override
	public boolean isVariable() {
		return false;
	}

}
