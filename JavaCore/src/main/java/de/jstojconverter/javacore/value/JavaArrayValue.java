package de.jstojconverter.javacore.value;

import de.jstojconverter.javacore.info.JavaInfoException;
import de.jstojconverter.javacore.info.JavaTypeInfo;
import de.jstojconverter.javacore.info.JavaTypeManager;
import de.jstojconverter.util.StringUtils;

/**
 * @author Felix Jordan
 * @since 31.07.2015 - 22:24:03
 * @version 1.0
 */
public class JavaArrayValue extends JavaAbstractValue {
	
	private JavaTypeInfo mType;
	private IJavaValue[] mElements;
	
	/** Content type of the lowest element in the element hierarchy
	 * if the array is multidimensional. Otherwise this type equals
	 * the element type. */
	private JavaTypeInfo mBasicContentType;
	private int mDimensions;
	private int[] mLengths;
	
	public JavaArrayValue(String pElementType) throws JavaInfoException {
		mType = JavaTypeManager.getManager().getType(pElementType + "[]");
		mElements = new IJavaValue[0];
	}
	
	public JavaArrayValue(String pElementType, int... pLengths) throws JavaInfoException {
		JavaTypeManager typeManager = JavaTypeManager.getManager();
		mType = typeManager.getType(pElementType + "[]");
		mBasicContentType = typeManager.getType(pElementType.replace("[]", ""));
		if (pLengths.length == 0) {
			throw new IllegalArgumentException("There must be at least a length for the first array dimension.");
		}
		mDimensions = StringUtils.countSequence(mType.getName(), "[]");
		if (pLengths.length > mDimensions) {
			throw new IllegalArgumentException("Too many length argument for a " + mDimensions
					+ " dimensional array.");
		}
		mLengths = pLengths;
	}
	
	public JavaArrayValue(String pElementType, IJavaValue... pElements) throws JavaInfoException {
		mType = JavaTypeManager.getManager().getType(pElementType + "[]");
		for (IJavaValue element : pElements) {
			if (!element.getType().equals(pElementType)) {
				throw new IllegalArgumentException("All elements must be of type '" + pElementType + "'");
			}
		}
		mElements = pElements;
	}
	
	public boolean hasElements() {
		return mLengths == null;
	}
	
	public int getLengths() {
		if (mLengths == null) {
			return mElements.length;
		}
		return mLengths[0];
	}
	
	public IJavaValue getElement(int pIndex) {
		if (mElements == null) {
			throw new IllegalStateException("This array value has no elements");
		}
		return mElements[pIndex];
	}
	
	@Override
	public String getValue() {
		StringBuilder valueBuilder = new StringBuilder();
		valueBuilder.append("new ");
		if (mLengths != null) {
			valueBuilder.append(mBasicContentType.getName());
			for (int i=0; i<mDimensions; i++) {
				valueBuilder.append('[');
				if (i < mLengths.length) {
					valueBuilder.append(mLengths[i]);
				}
				valueBuilder.append(']');
			}
		} else {
			valueBuilder.append(mType.getName());
			valueBuilder.append(" {");
			for (int i=0; i<mElements.length; i++) {
				valueBuilder.append(mElements[i].getValue());
				if (i < mElements.length - 1) {
					valueBuilder.append(", ");
				}
			}
			valueBuilder.append('}');
		}
		return valueBuilder.toString();
	}

	@Override
	public String getType() {
		return mType.getName();
	}
}
