package de.jstojconverter.javacore.value;

import de.jstojconverter.javacore.operators.JavaAssignmentOperator;

public class JavaAssignment extends JavaAbstractValue {
	
	private String mVariableName;
	private IJavaValue mAssignemntValue;
	private JavaAssignmentOperator mAssignmentOperator;
	
	public JavaAssignment(String pVariableName, JavaAssignmentOperator pOperator, IJavaValue pAssignmentValue) {
		mVariableName = pVariableName;
		mAssignmentOperator = pOperator;
		mAssignemntValue = pAssignmentValue;
	}
	
	@Override
	public String getValue() {
		StringBuilder valueBuilder = new StringBuilder();
		valueBuilder.append(mVariableName);
		valueBuilder.append(" ");
		valueBuilder.append(mAssignmentOperator);
		valueBuilder.append(" ");
		valueBuilder.append(mAssignemntValue.getValue());
		return valueBuilder.toString();
	}

	@Override
	public String getType() {
		return mAssignemntValue.getType();
	}

	@Override
	public boolean isVariable() {
		return false;
	}
}
