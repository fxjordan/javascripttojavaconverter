package de.jstojconverter.javacore.value;


public class JavaMethodInvocation extends JavaAbstractValue {
	
	private String mTarget;
	private String mMethodName;
	private String mReturnType;
	private IJavaValue[] mParameters;
	
	public JavaMethodInvocation(String pTarget, String pName, String pReturnType, IJavaValue... pParameters) {
		mTarget = pTarget;
		mMethodName = pName;
		mReturnType = pReturnType;
		mParameters = pParameters;
	}

	@Override
	public String getValue() {
		StringBuilder valueBuilder = new StringBuilder();
		valueBuilder.append(mTarget);
		valueBuilder.append(".");
		valueBuilder.append(mMethodName);
		valueBuilder.append("(");
		for (int i=0; i<mParameters.length; i++) {
			valueBuilder.append(mParameters[i].getValue());
			if (i < mParameters.length - 1) {
				valueBuilder.append(", ");
			}
		}
		valueBuilder.append(")");
		return valueBuilder.toString();
	}

	@Override
	public String getType() {
		return mReturnType;
	}

	@Override
	public boolean isVariable() {
		return false;
	}
}
