package de.jstojconverter.javacore.value;


public class JavaConstantValue extends JavaAbstractValue {
	
	@Deprecated
	public static final JavaConstantValue NULL = new JavaConstantValue("null", "null");
	
	private String mValue;
	private String mType;
	
	public JavaConstantValue(String pValue, String pType) {
		mValue = pValue;
		mType = pType;
	}
	
	public String getValue() {
		return mValue;
	}
	
	public String getType() {
		return mType;
	}
	
	public static String[] getTypeArray(JavaConstantValue[] pValues) {
		String[] types = new String[pValues.length];
		for (int i=0; i<pValues.length; i++) {
			types[i] = pValues[i].getType();
		}
		return types;
	}
	
	public static String[] getValueArray(JavaConstantValue[] pValues) {
		String[] types = new String[pValues.length];
		for (int i=0; i<pValues.length; i++) {
			types[i] = pValues[i].getValue();
		}
		return types;
	}
	
	@Override
	public boolean isVariable() {
		return false;
	}
	
	public static String[] getTypeArray(IJavaValue[] pValues) {
		String[] types = new String[pValues.length];
		for (int i=0; i<pValues.length; i++) {
			types[i] = pValues[i].getType();
		}
		return types;
	}
	
	public static String[] getValueArray(IJavaValue[] pValues) {
		String[] types = new String[pValues.length];
		for (int i=0; i<pValues.length; i++) {
			types[i] = pValues[i].getValue();
		}
		return types;
	}
}
