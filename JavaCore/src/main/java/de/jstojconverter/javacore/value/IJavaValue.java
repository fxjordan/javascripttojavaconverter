package de.jstojconverter.javacore.value;

public interface IJavaValue {
	
	public String getValue();
	
	public String getType();
	
	public boolean isVariable();
	
//	These methods moved to the class JavaValue for compatibility to Java 1.7
//	
//	public static String[] getTypeArray(IJavaValue[] pValues) {
//		String[] types = new String[pValues.length];
//		for (int i=0; i<pValues.length; i++) {
//			types[i] = pValues[i].getType();
//		}
//		return types;
//	}
//	
//	public static String[] getValueArray(IJavaValue[] pValues) {
//		String[] types = new String[pValues.length];
//		for (int i=0; i<pValues.length; i++) {
//			types[i] = pValues[i].getValue();
//		}
//		return types;
//	}
}
