package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaVariableInfo;
import de.jstojconverter.javacore.value.IJavaValue;

public class JavaDeclarationStatement extends JavaContent {
	
	private JavaVariableInfo mVariableInfo;
	private IJavaValue mValue;
	
	public JavaDeclarationStatement(JavaVariableInfo pVariableInfo, IJavaValue pValue) {
		mVariableInfo = pVariableInfo;
		mValue = pValue;
	}
	
	public JavaDeclarationStatement(JavaVariableInfo pVariableInfo) {
		this(pVariableInfo, null);
	}

	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(mVariableInfo.getType());
		srcBuilder.append(" ");
		srcBuilder.append(mVariableInfo.getName());
		if (mValue != null) {
			srcBuilder.append(" = ");
			srcBuilder.append(mValue.getValue());
		}
		srcBuilder.append(";\n");
		return srcBuilder.toString();
	}
}
