package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaConstructorInfo;
import de.jstojconverter.javacore.util.JavaParameter;

public class JavaConstructor extends JavaCodeBlock {
	
	private JavaConstructorInfo mConstructorInfo;
	
	public JavaConstructor(JavaConstructorInfo pTypeInfo) {
		super(pTypeInfo);
		mConstructorInfo = pTypeInfo;
	}

	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append("public ");
		beginBuilder.append(mConstructorInfo.getType());
		beginBuilder.append("(");
		JavaParameter[] parameters = mConstructorInfo.getParameters();
		for (int i=0; i<mConstructorInfo.getParameters().length; i++) {
			JavaParameter parameter = parameters[i];
			beginBuilder.append(parameter.getType());
			beginBuilder.append(" ");
			beginBuilder.append(parameter.getName());
			if (i < parameters.length - 1) {
				beginBuilder.append(",");
			}
		}
		beginBuilder.append(")");
		return beginBuilder.toString();
	}
}
