package de.jstojconverter.javacore;

/**
 * @author Felix Jordan
 * @since 24.07.2015 - 22:15:38
 * @version 1.0
 */
public class JavaCoreException extends Exception {
	
	private static final long serialVersionUID = -2545270580383563226L;
	
	public JavaCoreException() {
	}
	
	public JavaCoreException(String pMessage) {
		super(pMessage);
	}
	
	public JavaCoreException(Throwable pThrowable) {
		super(pThrowable);
	}
	
	public JavaCoreException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
