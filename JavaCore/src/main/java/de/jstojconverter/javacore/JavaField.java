package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaFieldInfo;

public class JavaField extends JavaContent {
	
	private JavaFieldInfo mFieldInfo;
	private String mValue;

	public JavaField(JavaFieldInfo pFieldInfo, String pValue) {
		mFieldInfo = pFieldInfo;
		mValue = pValue;
	}
	
	public JavaField(JavaFieldInfo pFieldInfo) {
		mFieldInfo = pFieldInfo;
	}

	@Override
	public String getSourceCode() {
		StringBuilder fieldBuilder = new StringBuilder();
		fieldBuilder.append("public");
		fieldBuilder.append(" ");
		fieldBuilder.append(mFieldInfo.getType());
		fieldBuilder.append(" ");
		fieldBuilder.append(mFieldInfo.getName());
		if (mValue != null) {
			fieldBuilder.append(" = ");
			fieldBuilder.append(mValue);
		}
		fieldBuilder.append(";");
		fieldBuilder.append("\n");
		return fieldBuilder.toString();
	}

	@Override
	public boolean isContainer() {
		return false;
	}
}
