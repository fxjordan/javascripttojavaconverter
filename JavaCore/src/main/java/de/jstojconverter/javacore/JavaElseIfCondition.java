package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.javacore.value.IJavaValue;

public class JavaElseIfCondition extends JavaCodeBlock {
	
	private IJavaValue mCondition;

	protected JavaElseIfCondition(JavaCodeBlockInfo pInfo, IJavaValue pCondition) {
		super(pInfo);
		mCondition = pCondition;
	}

	@Override
	protected String getBlockBegin() {
		return "else if (" + mCondition.getValue() + ")";
	}

}
