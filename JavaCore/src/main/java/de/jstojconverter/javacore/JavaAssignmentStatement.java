package de.jstojconverter.javacore;

import de.jstojconverter.javacore.operators.JavaAssignmentOperator;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaAssignment;

public class JavaAssignmentStatement extends JavaContent {
	
	private JavaAssignment mAssignemnt;
	
	protected JavaAssignmentStatement(JavaAssignment pAssignment) {
		mAssignemnt = pAssignment;
	}
	
	protected JavaAssignmentStatement(String pVariableName, JavaAssignmentOperator pOperator, IJavaValue pAssignmentValue) {
		this(new JavaAssignment(pVariableName, pOperator, pAssignmentValue));
	}

	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(mAssignemnt.getValue());
		srcBuilder.append(";\n");
		return srcBuilder.toString();
	}

}
