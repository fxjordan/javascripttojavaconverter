package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;

public class JavaDoWhileLoop extends JavaCodeBlock {
	
	private String mCondition;

	protected JavaDoWhileLoop(JavaCodeBlockInfo pInfo, String pCondition) {
		super(pInfo);
		mCondition = pCondition;
	}

	@Override
	protected String getBlockBegin() {
		return "do";
	}
	
	@Override
	protected String getBlockEnd() {
		return "while (" + mCondition + ");";
	}
}
