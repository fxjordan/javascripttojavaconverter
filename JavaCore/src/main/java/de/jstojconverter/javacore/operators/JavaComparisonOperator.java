package de.jstojconverter.javacore.operators;

import de.jstojconverter.javacore.value.IJavaValue;

public enum JavaComparisonOperator implements IJavaOperator {
	
	EQUAL("==") {
		@Override
		public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
			if (pValueA.getType().equals(pValueB.getType())) {
				return "boolean";
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
			if (pValueA.getType().equals(pValueB.getType())) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	NOT_EQUAL("!=") {
		@Override
		public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
			if (pValueA.getType().equals(pValueB.getType())) {
				return "boolean";
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
			if (pValueA.getType().equals(pValueB.getType())) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	GREATER(">"),
	LESS("<"),
	GREATER_OR_EQUAL(">="),
	LESS_OR_EQUAL("<=")
	;
	
	private static final String[] VALID_TYPES = new String[] {"short", "int", "long", "float", "double"};
	
	private String mOperator;
	
	private JavaComparisonOperator(String pOperator) {
		mOperator = pOperator;
	}
	
	@Override
	public String toString() {
		return mOperator;
	}

	@Override
	public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
		checkValid(pValueA, pValueB);
		return "boolean";
	}
	
	@Override
	public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
		return isValidType(pValueA.getType()) && isValidType(pValueB.getType());
	}
	
	private void checkValid(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
		if (!isValidFor(pValueA, pValueB)) {
			throw new JavaOperatorException("The operator " + mOperator + " is undefined for the argument type(s) "
					+ pValueA.getType() + ", " + pValueB.getType());
		}
	}
	
	private boolean isValidType(String pType) {
		for (String validType : VALID_TYPES) {
			if (validType.equals(pType)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isOperator(String pString) {
		for (JavaComparisonOperator operator : values()) {
			if (operator.toString().equals(pString)) {
				return true;
			}
		}
		return false;
	}
	
	public static JavaComparisonOperator getOperator(String pOperator) {
		for (JavaComparisonOperator operator : values()) {
			if (operator.toString().equals(pOperator)) {
				return operator;
			}
		}
		throw new IllegalArgumentException("No math operator with value '" + pOperator + "' exist!");
	}
	
	public static String[] stringValues() {
		JavaComparisonOperator[] values = values();
		String[] stringValues = new String[values.length];
		for (int i=0; i<values.length; i++) {
			stringValues[i] = values[i].toString();
		}
		return stringValues;
	}
	
	@Override
	public boolean isAssignemntOperator() {
		return false;
	}

	@Override
	public boolean isComparisonOperator() {
		return true;
	}

	@Override
	public boolean isMathOperator() {
		return false;
	}
}
