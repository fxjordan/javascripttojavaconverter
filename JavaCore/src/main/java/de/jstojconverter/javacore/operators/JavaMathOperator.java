package de.jstojconverter.javacore.operators;

import de.jstojconverter.javacore.value.IJavaValue;

public enum JavaMathOperator implements IJavaOperator {
	
	ADD("+") {
		@Override
		public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
			if (pValueA.getType().equals("java.lang.String") || pValueB.getType().equals("java.lang.String")) {
				return "java.lang.String";
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
			if (pValueA.getType().equals("java.lang.String") || pValueB.getType().equals("java.lang.String")) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	
	SUB("-"),
	MUL("*"),
	DIV("/");
	
	private static final String[] VALID_TYPES = new String[] {"short", "int", "long", "float", "double"};
	
	private String mOperator;
	
	private JavaMathOperator(String pOperator) {
		mOperator = pOperator;
	}
	
	@Override
	public String toString() {
		return mOperator;
	}
	
	@Override
	public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
		checkValid(pValueA, pValueB);
		String typeA = pValueA.getType();
		String typeB = pValueB.getType();
		if (typeA.equals("double") || typeB.equals("double")) {
			return "double";
		}
		if (typeA.equals("float") || typeB.equals("float")) {
			return "float";
		}
		if (typeA.equals("long") || typeB.equals("long")) {
			return "long";
		}
		if (typeA.equals("int") || typeB.equals("int")) {
			return "int";
		}
		if (typeA.equals("short") || typeB.equals("short")) {
			return "short";
		}
		throw new RuntimeException("Wrong operator types. Should never happen!");
	}

	@Override
	public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
		return isValidType(pValueA.getType()) && isValidType(pValueB.getType());
	}
	
	private boolean isValidType(String pType) {
		for (String validType : VALID_TYPES) {
			if (validType.equals(pType)) {
				return true;
			}
		}
		return false;
	}
	
	private void checkValid(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
		if (!isValidFor(pValueA, pValueB)) {
			throw new JavaOperatorException("The operator " + mOperator + " is undefined for the argument type(s) "
					+ pValueA.getType() + ", " + pValueB.getType());
		}
	}
	
	public static boolean isOperator(String pString) {
		for (JavaMathOperator operator : values()) {
			if (operator.toString().equals(pString)) {
				return true;
			}
		}
		return false;
	}
	
	public static JavaMathOperator getOperator(String pOperator) {
		for (JavaMathOperator operator : values()) {
			if (operator.toString().equals(pOperator)) {
				return operator;
			}
		}
		throw new IllegalArgumentException("No math operator with value '" + pOperator + "' exist!");
	}
	
	public static String[] stringValues() {
		JavaMathOperator[] values = values();
		String[] stringValues = new String[values.length];
		for (int i=0; i<values.length; i++) {
			stringValues[i] = values[i].toString();
		}
		return stringValues;
	}

	@Override
	public boolean isAssignemntOperator() {
		return false;
	}

	@Override
	public boolean isComparisonOperator() {
		return false;
	}

	@Override
	public boolean isMathOperator() {
		return true;
	}
}
