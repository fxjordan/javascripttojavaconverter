package de.jstojconverter.javacore.operators;

/**
 * This class is a temporary container for the static fields and methods of {@link IJavaOperator}.
 * For compatibility to Java 1.7 where interfaces cannot contain static fields and methods.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 02:28:32
 * @version 1.0
 */
public class JavaOperator {
	
	private JavaOperator() {
	}
	
	public static boolean isOperator(String pString) {
		return JavaAssignmentOperator.isOperator(pString) || JavaMathOperator.isOperator(pString)
				|| JavaComparisonOperator.isOperator(pString);
	}
	
	public static IJavaOperator getOperator(String pOperator) throws JavaOperatorException {
		if (JavaAssignmentOperator.isOperator(pOperator)) {
			return JavaAssignmentOperator.getOperator(pOperator);
		} else if (JavaMathOperator.isOperator(pOperator)) {
			return JavaMathOperator.getOperator(pOperator);
		} else if(JavaComparisonOperator.isOperator(pOperator)) {
			return JavaComparisonOperator.getOperator(pOperator);
		}
		throw new IllegalArgumentException("No operator with value '" + pOperator + "' exist!");
	}
	
	/** Use this constant instead of {@link #stringValues()}. */
	public static String[] STRING_VALUES = stringValues();
	
	/**
	 * Returns an array with all operators that exist. The array is recalculated
	 * every time this method is called, so it's recommended to use the {@link #STRING_VALUES}
	 * constant instead. There the value is cached and only caluculated once.
	 * 
	 * @return A string array with all operators that exist.
	 */
	public static String[] stringValues() {
		String[] assignemntValues = JavaAssignmentOperator.stringValues();
		String[] mathValues = JavaMathOperator.stringValues();
		String[] compariosonValues = JavaComparisonOperator.stringValues();
		int length = assignemntValues.length + mathValues.length + compariosonValues.length;
		String[] stringValues = new String[length];
		for (int i=0; i<assignemntValues.length; i++) {
			stringValues[i] = assignemntValues[i];
		}
		for (int i=0; i<mathValues.length; i++) {
			stringValues[assignemntValues.length + i] = mathValues[i];
		}
		for (int i=0; i<compariosonValues.length; i++) {
			stringValues[assignemntValues.length + mathValues.length + i] = compariosonValues[i];
		}

		// Sort by length
		int begin = 0;
		int end = stringValues.length - 1;
		int best = begin;
		String cache;
		
		while (begin != end) {
			for (int pointer=begin+1; pointer<end+1; pointer++) {
				if (stringValues[pointer].length() > stringValues[best].length()) {
					best = pointer;
				}
			}
			cache = stringValues[begin];
			stringValues[begin] = stringValues[best];
			stringValues[best] = cache;
			
			begin++;
			best = begin;
		}
		return stringValues;
	}
}
