package de.jstojconverter.javacore.operators;
import de.jstojconverter.javacore.*;

/**
 * @author Felix Jordan
 * @since 27.07.2015 - 22:46:57
 * @version 1.0
 */
public class JavaOperatorException extends JavaCoreException {
	
	private static final long serialVersionUID = 2628193731810120547L;
	
	public JavaOperatorException() {
	}
	
	public JavaOperatorException(String pMessage) {
		super(pMessage);
	}
	
	public JavaOperatorException(Throwable pThrowable) {
		super(pThrowable);
	}
	
	public JavaOperatorException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
