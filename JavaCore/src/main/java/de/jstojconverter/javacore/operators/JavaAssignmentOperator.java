package de.jstojconverter.javacore.operators;

import de.jstojconverter.javacore.value.IJavaValue;

public enum JavaAssignmentOperator implements IJavaOperator {
	
	NORMAL("=") {
		@Override
		public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
			// TODO Remove this code. 'undefined' is not a real java type
			if (pValueA.getType().equals("undefined") && pValueA.isVariable()
					&& !pValueB.getType().equals("undefined")) {
				return pValueB.getType();
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
			// TODO Remove this code. 'undefined' is not a real java type
			if (pValueA.getType().equals("undefined") && pValueA.isVariable()
					&& !pValueB.getType().equals("undefined")) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	ADD("+=") {
		@Override
		public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
			if (pValueA.getType().equals("String")) {
				return "String";
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
			if (pValueA.getType().equals("String")) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	SUB("-="),
	MUL("*="),
	DIV("/=");
	
	private final String mOperator;
	
	private JavaAssignmentOperator(String pOperator) {
		mOperator = pOperator;
	}
	
	@Override
	public String toString() {
		return mOperator;
	}
	
	@Override
	public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
		checkValid(pValueA, pValueB);
		return pValueA.getType();
	}

	@Override
	public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB) {
		return pValueA.getType().equals(pValueB.getType()) && pValueA.isVariable();
	}
	
	private void checkValid(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException {
		if (!pValueA.isVariable()) {
			throw new JavaOperatorException("The left hand side of an assignment must be a variable! " + pValueA.getValue() + " "
					+ mOperator + " " + pValueB.getValue());
		}
		if (!isValidFor(pValueA, pValueB)) {
			throw new JavaOperatorException("The operator " + mOperator + " is undefined for the argument type(s) "
					+ pValueA.getType() + ", " + pValueB.getType());
		}
	}
	
	public static boolean isOperator(String pString) {
		for (JavaAssignmentOperator operator : values()) {
			if (operator.toString().equals(pString)) {
				return true;
			}
		}
		return false;
	}
	
	public static JavaAssignmentOperator getOperator(String pOperator) throws JavaOperatorException {
		for (JavaAssignmentOperator operator : values()) {
			if (operator.toString().equals(pOperator)) {
				return operator;
			}
		}
		throw new IllegalArgumentException("No assignment operator with value '" + pOperator + "' exists!");
	}
	
	public static String[] stringValues() {
		JavaAssignmentOperator[] values = values();
		String[] stringValues = new String[values.length];
		for (int i=0; i<values.length; i++) {
			stringValues[i] = values[i].toString();
		}
		return stringValues;
	}
	
	@Override
	public boolean isAssignemntOperator() {
		return true;
	}

	@Override
	public boolean isComparisonOperator() {
		return false;
	}

	@Override
	public boolean isMathOperator() {
		return false;
	}
}
