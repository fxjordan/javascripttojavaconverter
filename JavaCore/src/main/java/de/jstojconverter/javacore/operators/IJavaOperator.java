package de.jstojconverter.javacore.operators;

import de.jstojconverter.javacore.value.IJavaValue;

public interface IJavaOperator {
	
	public String toString();
	
	public String getResultTypeFor(IJavaValue pValueA, IJavaValue pValueB) throws JavaOperatorException;
	
	public boolean isValidFor(IJavaValue pValueA, IJavaValue pValueB);
	
	public boolean isAssignemntOperator();
	
	public boolean isComparisonOperator();
	
	public boolean isMathOperator();
	
//	These fields and methods moved to the class JavaOperator. See javadoc of this class for more detail.
//	
//	public static boolean isOperator(String pString) {
//		return AssignmentOperator.isOperator(pString) || MathOperator.isOperator(pString)
//				|| ComparisonOperator.isOperator(pString);
//	}
//	
//	public static IJavaOperator getOperator(String pOperator) throws ConverterException {
//		if (AssignmentOperator.isOperator(pOperator)) {
//			return AssignmentOperator.getOperator(pOperator);
//		} else if (MathOperator.isOperator(pOperator)) {
//			return MathOperator.getOperator(pOperator);
//		} else if(ComparisonOperator.isOperator(pOperator)) {
//			return ComparisonOperator.getOperator(pOperator);
//		}
//		throw new IllegalArgumentException("No operator with value '" + pOperator + "' exist!");
//	}
//	
//	/** Use this constant instead of {@link #stringValues()}. */
//	public static String[] STRING_VALUES = stringValues();
//	
//	/**
//	 * Returns an array with all operators that exist. The array is recalculated
//	 * every time this method is called, so it's recommended to use the {@link #STRING_VALUES}
//	 * constant instead. There the value is cached and only caluculated once.
//	 * 
//	 * @return A string array with all operators that exist.
//	 */
//	public static String[] stringValues() {
//		String[] assignemntValues = AssignmentOperator.stringValues();
//		String[] mathValues = MathOperator.stringValues();
//		String[] compariosonValues = ComparisonOperator.stringValues();
//		int length = assignemntValues.length + mathValues.length + compariosonValues.length;
//		String[] stringValues = new String[length];
//		for (int i=0; i<assignemntValues.length; i++) {
//			stringValues[i] = assignemntValues[i];
//		}
//		for (int i=0; i<mathValues.length; i++) {
//			stringValues[assignemntValues.length + i] = mathValues[i];
//		}
//		for (int i=0; i<compariosonValues.length; i++) {
//			stringValues[assignemntValues.length + mathValues.length + i] = compariosonValues[i];
//		}
//
//		// Sort by length
//		int begin = 0;
//		int end = stringValues.length - 1;
//		int best = begin;
//		String cache;
//		
//		while (begin != end) {
//			for (int pointer=begin+1; pointer<end+1; pointer++) {
//				if (stringValues[pointer].length() > stringValues[best].length()) {
//					best = pointer;
//				}
//			}
//			cache = stringValues[begin];
//			stringValues[begin] = stringValues[best];
//			stringValues[best] = cache;
//			
//			begin++;
//			best = begin;
//		}
//		return stringValues;
//	}
}
