package de.jstojconverter.javacore.util;

public class JavaParameter {
	
	private String mName;
	private String mType;
	
	public JavaParameter(String pName, String pType) {
		mName = pName;
		mType = pType;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getType() {
		return mType;
	}
	
	public static String[] getTypeArray(JavaParameter[] pParameters) {
		String[] types = new String[pParameters.length];
		for (int i=0; i<pParameters.length; i++) {
			types[i] = pParameters[i].getType();
		}
		return types;
	}
}
