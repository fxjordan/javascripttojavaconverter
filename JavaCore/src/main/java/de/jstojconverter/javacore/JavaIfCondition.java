package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.javacore.value.IJavaValue;

public class JavaIfCondition extends JavaCodeBlock {
	
	private IJavaValue mCondition;

	protected JavaIfCondition(JavaCodeBlockInfo pInfo, IJavaValue pCondition) {
		super(pInfo);
		mCondition = pCondition;
	}

	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append("if (");
		beginBuilder.append(mCondition.getValue());
		beginBuilder.append(")");
		return beginBuilder.toString();
	}
}
