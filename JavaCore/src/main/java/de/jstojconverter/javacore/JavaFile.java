package de.jstojconverter.javacore;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.javacore.info.JavaInfoException;
import de.jstojconverter.javacore.info.JavaTypeManager;
import de.jstojconverter.util.JavaTypeUtils;

public class JavaFile {
	
	private JavaClass mClass;
	private List<String> mImports;
	
	public JavaFile(String pPackageName, String pClassName) throws JavaInfoException {
		mImports = new ArrayList<String>();
		mClass = new JavaClass(this, JavaTypeManager.getManager().createType(JavaTypeUtils.composeType(pPackageName, pClassName)));
	}
	
	public void addImport(String pImport) {
		mImports.add(pImport);
	}
	
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		String classPackage = mClass.getInfo().getPackage();
		if (!classPackage.isEmpty()) {
			srcBuilder.append("package ");
			srcBuilder.append(classPackage);
			srcBuilder.append(";\n");
			srcBuilder.append("\n");
		}
		
		if (!mImports.isEmpty()) {
			for (String importName : mImports) {
				srcBuilder.append("import ");
				srcBuilder.append(importName);
				srcBuilder.append(";\n");
			}
			srcBuilder.append("\n");
		}
		
		String classText = mClass.getSourceCode();
		srcBuilder.append(classText);
		return srcBuilder.toString();
	}

	public JavaClass getJavaClass() {
		return mClass;
	}
}
