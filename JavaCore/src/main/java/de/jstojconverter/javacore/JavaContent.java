package de.jstojconverter.javacore;

public abstract class JavaContent {
	
	private JavaContent mParent;
	
	protected void attatchChild(JavaContent pContent) {
		pContent.mParent = this;
	}

	public boolean hasParent() {
		return mParent != null;
	}

	public JavaContent getParent() {
		return mParent;
	}
	
	public boolean isContainer() {
		return true;
	}
	
	/**
	 * Returns the parent class of this {@code JavaContent} or null
	 * if there is no parent class.
	 * 
	 * @return
	 */
	public JavaClass getParentClass() {
		if (mParent != null) {
			if (mParent instanceof JavaClass) {
				return (JavaClass) mParent;
			} else {
				return mParent.getParentClass();
			}
		}
		if (this instanceof JavaClass) {
			return (JavaClass) this;
		}
		return null;
	}
	
	public abstract String getSourceCode();
}
