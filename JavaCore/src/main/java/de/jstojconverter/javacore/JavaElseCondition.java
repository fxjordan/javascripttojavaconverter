package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;

public class JavaElseCondition extends JavaCodeBlock {
	
	protected JavaElseCondition(JavaCodeBlockInfo pInfo) {
		super(pInfo);
	}

	@Override
	protected String getBlockBegin() {
		return "else";
	}
}
