package de.jstojconverter.javacore;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;

public class JavaForLoop extends JavaCodeBlock {
	
	private String mPreStatement;
	private String mCondition;
	private String mPostStatement;

	/**
	 * Creates a new java for loop.
	 * <br>
	 * The arguments must not contain the semicolons at the end of the statements.
	 * 
	 * @param pPreStatement
	 * @param pCondition
	 * @param pPostStatement
	 */
	protected JavaForLoop(JavaCodeBlockInfo pInfo, String pPreStatement, String pCondition, String pPostStatement) {
		super(pInfo);
		mPreStatement = pPreStatement;
		mCondition = pCondition;
		mPostStatement = pPostStatement;
	}

	@Override
	protected String getBlockBegin() {
		return "for (" + mPreStatement + "; " + mCondition +"; " + mPostStatement + ")";
	}

}
