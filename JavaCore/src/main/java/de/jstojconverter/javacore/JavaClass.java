package de.jstojconverter.javacore;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.javacore.info.JavaInfoException;
import de.jstojconverter.javacore.info.JavaTypeInfo;
import de.jstojconverter.javacore.util.JavaParameter;

public class JavaClass extends JavaContent {
	
	private JavaTypeInfo mTypeInfo;
	private JavaFile mFile;
	private List<JavaField> mFields;
	private List<JavaConstructor> mConstructors;
	private List<JavaMethod> mMethods;
	
	public JavaClass(JavaFile pFile, JavaTypeInfo ptTypeInfo) {
		mFile = pFile;
		mTypeInfo = ptTypeInfo;
		mFields = new ArrayList<JavaField>();
		mConstructors = new ArrayList<JavaConstructor>();
		mMethods = new ArrayList<JavaMethod>();
		mFile.addImport("java.util.ArrayList");
		mFile.addImport("java.util.List");
	}
	
	public JavaFile getFile() {
		return mFile;
	}
	
	public JavaField createField(String pName, String pType) throws JavaInfoException {
		JavaField field =  new JavaField(mTypeInfo.createField(pName, pType));
		super.attatchChild(field);
		mFields.add(field);
		return field;
	}
	
	public JavaField createField(String pName, String pType, String pValue) throws JavaInfoException {
		JavaField field = new JavaField(mTypeInfo.createField(pName, pType), pValue);
		mFields.add(field);
		return field;
	}
	
	public JavaConstructor createConstructor(JavaParameter... pParameters) throws JavaInfoException {
		JavaConstructor constructor = new JavaConstructor(mTypeInfo.createConstructor(pParameters));
		super.attatchChild(constructor);
		mConstructors.add(constructor);
		return constructor;
	}
	
	public JavaMethod createMethod(String pVisibility, String pModifier, String pName,
			String pReturnType, JavaParameter... pParameters) throws JavaInfoException {
		JavaMethod method = new JavaMethod(pVisibility, pModifier, mTypeInfo.createMethod(pName, pReturnType, pParameters));
		super.attatchChild(method);
		mMethods.add(method);
		return method;
	}
	
	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append("public class ");
		srcBuilder.append(mTypeInfo.getName());
		srcBuilder.append(" {\n    ");
		
		
		if (mFields.size() > 0) {
			srcBuilder.append("\n    ");
		}
		for (int i=0; i<mFields.size(); i++) {
			JavaField field = mFields.get(i);
			String fieldText = field.getSourceCode().replace("\n", "\n    ");
			srcBuilder.append(fieldText);
		}
		
		for (int i=0; i<mConstructors.size(); i++) {
			srcBuilder.append("\n    ");
			JavaConstructor constructor = mConstructors.get(i);
			String constructorText = constructor.getSourceCode().replace("\n", "\n    ");
			srcBuilder.append(constructorText);
		}
		
		for (int i=0; i<mMethods.size(); i++) {
			srcBuilder.append("\n    ");
			JavaMethod method = mMethods.get(i);
			String methodText = method.getSourceCode().replace("\n", "\n    ");
			srcBuilder.append(methodText);
		}
		
		srcBuilder.delete(srcBuilder.length()-5, srcBuilder.length());
		srcBuilder.append("\n}\n");
		return srcBuilder.toString();
	}
	
	public JavaTypeInfo getInfo() {
		return mTypeInfo;
	}
}
