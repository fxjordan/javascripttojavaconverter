package de.jstojconverter.javacore;

import de.jstojconverter.javacore.value.JavaMethodInvocation;

public class JavaMethodInvocationStatement extends JavaContent {
	
	private JavaMethodInvocation mMethodInvocation;
	
	protected JavaMethodInvocationStatement(JavaMethodInvocation pMethodInvocation) {
		mMethodInvocation = pMethodInvocation;
	}
	
	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(mMethodInvocation.getValue());
		srcBuilder.append(";\n");
		return srcBuilder.toString();
	}
}
