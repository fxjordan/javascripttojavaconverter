package de.jstojconverter.javacore.info;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jstojconverter.javacore.util.JavaNameValidation;

public class JavaCodeBlockInfo implements IJavaVariableContainer {
	
	private Map<String, JavaFieldInfo> mGlobalVariables;
	private Map<String, JavaVariableInfo> mVariables;
	private List<JavaCodeBlockInfo> mCodeBocks;
	protected JavaTypeManager mTypeManager;
	private boolean mEditable;
	private JavaTypeInfo mParentType;
	
	/**
	 * Global variables map is just stored, so if a class adds a global field it would be appear in this map.
	 * 
	 * Parent variables are copied to a new map, so that variables created after this code block will
	 * not appear in this list.
	 * 
	 * When the code block is created only the variables that re defined before
	 * the code block is defined.
	 * 
	 * @param pParentVariables
	 */
	protected JavaCodeBlockInfo(JavaTypeInfo pParentType, Map<String, JavaFieldInfo> pGlobalVariables, Map<String, JavaVariableInfo> pParentVariables) {
		mParentType = pParentType;
		mGlobalVariables = pGlobalVariables;
		mVariables = new HashMap<String, JavaVariableInfo>();
		if (pParentVariables != null) {
			mVariables.putAll(pParentVariables);	
		}
		mCodeBocks = new ArrayList<JavaCodeBlockInfo>();
		mTypeManager = JavaTypeManager.getManager();
		mEditable = true;
	}
	
	public JavaCodeBlockInfo createCodeBlock() {
		checkEditable();
		JavaCodeBlockInfo codeBlockInfo = new JavaCodeBlockInfo(mParentType, mGlobalVariables, mVariables);
		mCodeBocks.add(codeBlockInfo);
		return codeBlockInfo;
	}
	
	public JavaVariableInfo createVariable(String pName, String pType) throws JavaInfoException {
		checkEditable();
		if (!mTypeManager.existType(pType)) {
			throw new JavaInfoException("Type '" + pType + "' does not exist!");
		}
		if (!JavaNameValidation.isVariableNameValid(pName)) {
			throw new JavaInfoException("Variable name '" + pName + "' is not valid!");
		}
		// TODO Check global variables
		if (mVariables.containsKey(JavaVariableInfo.createSignature(pName))) {
			throw new JavaInfoException("Variable '" + pName + "' already exists!");
		}
		JavaVariableInfo variableInfo = new JavaVariableInfo(mParentType, pName, pType);
		mVariables.put(variableInfo.getSignature(), variableInfo);
		return variableInfo;
	}
	
	public boolean existVariable(String pName) {
		String signature = JavaVariableInfo.createSignature(pName);
		if (mVariables.containsKey(signature)) {
			return true;
		}
		return mGlobalVariables.containsKey(signature);
	}
	
	public JavaVariableInfo getVariable(String pName) throws JavaInfoException {
		String signature = JavaVariableInfo.createSignature(pName);
		if (mVariables.containsKey(signature)) {
			return mVariables.get(signature);
		} else if (mGlobalVariables.containsKey(signature)) {
			return mGlobalVariables.get(signature);
		}
		throw new JavaInfoException("Variable '" + signature + "' does not exist!");
	}
	
	public JavaTypeInfo getParentType() {
		return mParentType;
	}
	
	protected void setEditable(boolean pEditable) {
		mEditable = pEditable;
	}
	
	private void checkEditable() {
		if (!mEditable) {
			throw new IllegalStateException("CodeBlock is not editable!");
		}
	}
}
