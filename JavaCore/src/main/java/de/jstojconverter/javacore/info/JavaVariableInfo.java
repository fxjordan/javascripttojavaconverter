package de.jstojconverter.javacore.info;

public class JavaVariableInfo {
	
	private String mName;
	private String mType;
	private JavaTypeInfo mParentType;
	private JavaTypeManager mTypeManager;
	
	protected JavaVariableInfo(JavaTypeInfo pParentType, String pName, String pType) {
		mParentType = pParentType;
		mName = pName;
		mType = pType;
		mTypeManager = JavaTypeManager.getManager();
	}
	
	public String getName() {
		return mName;
	}
	
	public String getType() {
		return mType;
	}
	
	public String getSignature() {
		return createSignature(mName);
	}

	public static String createSignature(String pName) {
		return pName;
	}
	
	public void changeType(String pType) throws JavaInfoException {
		if (!mType.equals("undefined")) {
			throw new JavaInfoException("You can only change type if type is 'undefined'");
		}
		if (!mTypeManager.existType(pType)) {
			throw new JavaInfoException("Type '" + pType + "' does not exist!");
		}
		mType = pType;
	}
	
	public JavaTypeInfo getParentType() {
		return mParentType;
	}
}
