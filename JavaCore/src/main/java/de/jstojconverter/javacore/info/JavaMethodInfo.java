package de.jstojconverter.javacore.info;

import java.util.Map;

import de.jstojconverter.javacore.util.JavaParameter;

public class JavaMethodInfo extends JavaCodeBlockInfo {
	
	private String mName;
	private String mReturnType;
	private JavaParameter[] mParameters;
	private boolean mLoaded;
	
	protected JavaMethodInfo(JavaTypeInfo pParentType, Map<String, JavaFieldInfo> pGlobalVariables, Map<String,
			JavaVariableInfo> pParentVariables, String pName, String pReturnType, JavaParameter... pParameters) {
		super(pParentType, pGlobalVariables, pParentVariables);
		mName = pName;
		mReturnType = pReturnType;
		mParameters = pParameters;
		try {
			for (int i=0; i<mParameters.length; i++) {
				JavaParameter parameter = mParameters[i];
				createVariable(parameter.getName(), parameter.getType());
			}
		} catch (JavaInfoException e) {
			throw new RuntimeException("Internal error. Failed to create method parameters!");
		}
	}
	
	public String getName() {
		return mName;
	}
	
	public String getReturnType() {
		return mReturnType;
	}
	
	public JavaParameter[] getParameters() {
		return mParameters;
	}
	
	@Deprecated // TODO Check if method is used
	public boolean isLoaded() {
		return mLoaded;
	}
	
	@Deprecated // TODO Check if method is used
	public void load() {
		mLoaded = true;
	}
	
	public String getSignature() {
		return createSignature(mName, JavaParameter.getTypeArray(mParameters));
	}
	
	public static String createSignature(String pName, String... pParameters) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(pName);
		signatureBuilder.append("(");
		for (int i=0; i<pParameters.length; i++) {
			signatureBuilder.append(pParameters[i]);
			if (i < pParameters.length - 1) {
				signatureBuilder.append(",");
			}
		}
		signatureBuilder.append(")");
		return signatureBuilder .toString();
	}
}