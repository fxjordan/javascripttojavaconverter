package de.jstojconverter.javacore.info;

public class JavaFieldInfo extends JavaVariableInfo {
	
	/*
	 * TODO Later implement visibility and other modifiers
	 * for fields
	 */
	
	protected JavaFieldInfo(JavaTypeInfo pParentType, String pName, String pType) {
		super(pParentType, pName, pType);
	}
}