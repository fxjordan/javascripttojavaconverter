package de.jstojconverter.javacore.info;

public interface IJavaVariableContainer {
	
	public boolean existVariable(String pSignature);
	
	public JavaVariableInfo getVariable(String pSignature) throws JavaInfoException;
	
	public JavaTypeInfo getParentType();
}
