package de.jstojconverter.javacore.info;

import de.jstojconverter.javacore.JavaCoreException;

/**
 * @author Felix Jordan
 * @since 24.07.2015 - 22:05:45
 * @version 1.0
 */
public class JavaInfoException extends JavaCoreException {
	
	private static final long serialVersionUID = -8386338507720090223L;
	
	public JavaInfoException() {
	}

	public JavaInfoException(String pMessage) {
		super(pMessage);
	}

	public JavaInfoException(Throwable pThrowable) {
		super(pThrowable);
	}

	public JavaInfoException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
