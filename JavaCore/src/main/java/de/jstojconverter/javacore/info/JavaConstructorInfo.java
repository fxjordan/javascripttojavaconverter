package de.jstojconverter.javacore.info;

import java.util.Map;

import de.jstojconverter.javacore.util.JavaParameter;

public class JavaConstructorInfo extends JavaCodeBlockInfo {
	
	private String mType;
	private JavaParameter[] mParameters;

	protected JavaConstructorInfo(JavaTypeInfo pParentType, Map<String, JavaFieldInfo> pGlobalVariables,
			Map<String, JavaVariableInfo> pParentVariables, String pType, JavaParameter... pParameters) {
		super(pParentType, pGlobalVariables, pParentVariables);
		mType = pType;
		mParameters = pParameters;
		try {
			for (int i=0; i<mParameters.length; i++) {
				JavaParameter parameter = mParameters[i];
				createVariable(parameter.getName(), parameter.getType());
			}
		} catch (JavaInfoException e) {
			throw new RuntimeException("Internal error. Failed to create method parameters!", e);
		}
	}
	
	public String getType() {
		return mType;
	}
	
	public JavaParameter[] getParameters() {
		return mParameters;
	}
	
	public String getSignature() {
		return createSignature(mType, JavaParameter.getTypeArray(mParameters));
	}

	public static String createSignature(String pType, String[] pParameters) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(pType);
		signatureBuilder.append("(");
		for (int i=0; i<pParameters.length; i++) {
			signatureBuilder.append(pParameters[i]);
			if (i < pParameters.length - 1) {
				signatureBuilder.append(",");
			}
		}
		signatureBuilder.append(")");
		return signatureBuilder .toString();
	}
}