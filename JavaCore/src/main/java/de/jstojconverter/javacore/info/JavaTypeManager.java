package de.jstojconverter.javacore.info;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jstojconverter.ctdloader.CTDManager;
import de.jstojconverter.ctdloader.Class;
import de.jstojconverter.ctdloader.Constructor;
import de.jstojconverter.ctdloader.Field;
import de.jstojconverter.ctdloader.Method;
import de.jstojconverter.javacore.util.JavaNameValidation;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.util.JavaTypeUtils;

public class JavaTypeManager {
	
	public static final String LOG_TAG = "TypeManager";
	
	private static final String EXTERNAL_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "JavaScriptToJavaConverter/src/main/resources/ctd_files";
	private static final String INTERNAL_RESOURCE_PATH = "ctd_files";

	private static JavaTypeManager sInstance;
	
	/** Types mapped to their signatures */
	private Map<String, JavaTypeInfo> mTypeMap;
	private CTDManager mCTDManager;
	
	private JavaTypeManager() {
		mTypeMap = new HashMap<String, JavaTypeInfo>();
		mCTDManager = new CTDManager();
		mCTDManager.load(EXTERNAL_FOLDER, INTERNAL_RESOURCE_PATH);
	}
	
	public static JavaTypeManager getManager() {
		if (sInstance == null) {
			sInstance = new JavaTypeManager();
			// Don't invoke inside the constructor (Loop).
			sInstance.createPrimitiveTypes();
			sInstance.createLoadedClassTypes();
			return sInstance;
		}
		return sInstance;
	}

	private void createPrimitiveTypes() {
		try {
			createType("char").setEditable(false);
			createType("byte").setEditable(false);
			createType("boolean").setEditable(false);
			createType("short").setEditable(false);
			createType("int").setEditable(false);
			createType("long").setEditable(false);
			createType("float").setEditable(false);
			createType("double").setEditable(false);
			createType("void").setEditable(false);
			
			// TODO Remove! This is not a real Java type
			createType("undefined").setEditable(false);
		} catch (JavaInfoException e) {
			throw new RuntimeException("Internal error. Failed to create primitive types!", e);
		}
	}
	
	protected JavaTypeInfo loadCTDFile(String pName) throws JavaInfoException {
		Class clazz = mCTDManager.getForName(pName);
		if (clazz == null) {
			return null;
		}
		JavaTypeInfo typeInfo = createType(JavaTypeUtils.composeType(clazz.getPackage(), clazz.getName()));
		createLoadedFields(typeInfo, clazz.getFields());
		createLoadedConstructors(typeInfo, clazz.getConstructors());
		createLoadedMethods(typeInfo, clazz.getMethods());
		typeInfo.setEditable(false);
		return typeInfo;
	}
	
	private void createLoadedClassTypes() {
		try {
			List<Class> classes = mCTDManager.load(EXTERNAL_FOLDER, INTERNAL_RESOURCE_PATH);
			for (Class clazz : classes) {
				JavaTypeInfo typeInfo;
				if (!existType(JavaTypeUtils.composeType(clazz.getPackage(), clazz.getName()))) {
					// TODO This code is probably dead, because the types are created insode the 'existType' method.
					typeInfo = createType(JavaTypeUtils.composeType(clazz.getPackage(), clazz.getName()));
					createLoadedFields(typeInfo, clazz.getFields());
					createLoadedConstructors(typeInfo, clazz.getConstructors());
					createLoadedMethods(typeInfo, clazz.getMethods());
				}
			}
		} catch (JavaInfoException e) {
			throw new RuntimeException("Internal error. Failed to create loaded class types!", e);
		}
	}

	private void createLoadedMethods(JavaTypeInfo pTypeInfo, List<Method> pMethods) throws JavaInfoException {
		for (Method method : pMethods) {
			String[] parameterTypes = method.getParameters();
			JavaParameter[] parameters = new JavaParameter[parameterTypes.length];
			for (int i=0; i<parameters.length; i++) {
				parameters[i] = createParameter(parameterTypes[i]);
			}
			pTypeInfo.createMethod(method.getName(), method.getReturnType(), parameters).setEditable(false);
		}
	}

	private void createLoadedConstructors(JavaTypeInfo pTypeInfo, List<Constructor> pConstructors) throws JavaInfoException {
		for (Constructor constructor : pConstructors) {
			String[] parameterTypes = constructor.getParameters();
			JavaParameter[] parameters = new JavaParameter[parameterTypes.length];
			for (int i=0; i<parameters.length; i++) {
				parameters[i] = createParameter(parameterTypes[i]);
			}
			pTypeInfo.createConstructor(parameters).setEditable(false);
		}
	}
	
	private JavaParameter createParameter(String pType) {
		int r = (int) (Math.random() * 10);
		String name = pType + Integer.toString(r);
		return new JavaParameter(name, pType);
	}

	private void createLoadedFields(JavaTypeInfo pTypeInfo, List<Field> pFields) throws JavaInfoException {
		for (Field field : pFields) {
			pTypeInfo.createField(field.getName(), field.getType());
		}
	}
	
	/**
	 * 
	 * <b>Important:</b> Do not use to create array types. Array
	 *  	Types are created automatically.
	 * 
	 * @param pType
	 * @return
	 * @throws JavaInfoException
	 */
	public JavaTypeInfo createType(String pType) throws JavaInfoException {
		String[] typeData = JavaTypeUtils.splitType(pType);
		String signature = JavaTypeInfo.createSignature(typeData[0], typeData[1]);
		if (!JavaNameValidation.isPackageNameValid(typeData[0])) {
			throw new JavaInfoException("Package name '" + typeData[0] + "' is not valid!");
		}
		if (!JavaNameValidation.isTypeNameValid(typeData[1])) {
			throw new JavaInfoException("Type name '" + typeData[1] + "' is not valid!");
		}
		if (mTypeMap.containsKey(signature)) {
			throw new JavaInfoException("Type '" + signature + "' already exists!");
		}
		JavaTypeInfo typeInfo = new JavaTypeInfo(typeData[0], typeData[1]);
		mTypeMap.put(typeInfo.getSignature(), typeInfo);
		return typeInfo;
	}
	
	public boolean existType(String pType) {
		String[] typeData = JavaTypeUtils.splitType(pType);
		String signature = JavaTypeInfo.createSignature(typeData[0], typeData[1]);
		if (mTypeMap.containsKey(signature)) {
			return true;
		}
		if (pType.contains("[]")) {
			// If element type exists the array type can be created.
			if (existType(pType.replace("[]", ""))) {
				mTypeMap.put(signature, new JavaTypeInfo(typeData[0], typeData[1]));
				return true;
			}
			return false;
		} else {
			JavaTypeInfo typeInfo;
			try {
				typeInfo = loadCTDFile(pType);
			} catch (JavaInfoException e) {
				throw new RuntimeException("Internal error. Failed to load ctd file!", e);
			}
			if (typeInfo == null) {
				return false;
			}
			return true;
		}
	}
	
	public JavaTypeInfo getType(String pType) throws JavaInfoException {
		if (existType(pType)) {
			String[] typeData = JavaTypeUtils.splitType(pType);
			String signature = JavaTypeInfo.createSignature(typeData[0], typeData[1]);
			return mTypeMap.get(signature);
		}
		throw new JavaInfoException("Type '" + pType + "' does not exist!");
	}
}
