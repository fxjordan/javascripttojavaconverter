package de.jstojconverter.javacore.info;

import java.util.HashMap;
import java.util.Map;

import de.jstojconverter.javacore.util.JavaNameValidation;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.util.JavaTypeUtils;

public class JavaTypeInfo implements IJavaVariableContainer {
	
	private String mPackage;
	private String mName;
	
	/** Fields mapped to their signature */
	private Map<String, JavaFieldInfo> mFields;
	private Map<String, JavaConstructorInfo> mConstructors;
	
	/** First key: method name ; Second key: full signature */
	private Map<String, Map<String, JavaMethodInfo>> mMethods;
	
	private JavaTypeManager mTypeManager;
	private boolean mEditable;
	
	protected JavaTypeInfo(String pPackage, String pName) {
		mPackage = pPackage;
		mName = pName;
		mFields = new HashMap<String, JavaFieldInfo>();
		mConstructors = new HashMap<String, JavaConstructorInfo>();
		mMethods = new HashMap<String, Map<String, JavaMethodInfo>>();
		mTypeManager = JavaTypeManager.getManager();
		mEditable = true;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getPackage() {
		return mPackage;
	}
	
	public String getSignature() {
		return createSignature(mPackage, mName);
	}
	
	public static String createSignature(String pPackage, String pName) {
		return JavaTypeUtils.composeType(pPackage, pName);
	}
	
	public JavaFieldInfo createField(String pName, String pType) throws JavaInfoException {
		checkEditable();
		if (!mTypeManager.existType(pType)) {
			throw new JavaInfoException("Type '" + pType + "' does not exist!");
		}
		if (!JavaNameValidation.isVariableNameValid(pName)) {
			throw new JavaInfoException("Field name '" + pName + "' is not valid!");
		}
		if (mFields.containsKey(JavaFieldInfo.createSignature(pName))) {
			throw new JavaInfoException("Field '" + pName + "' already exists!");
		}
		JavaFieldInfo fieldInfo = new JavaFieldInfo(this, pName, pType);
		mFields.put(fieldInfo.getSignature(), fieldInfo);
		return fieldInfo;
	}
	
	public JavaConstructorInfo createConstructor(JavaParameter... pParameters) throws JavaInfoException {
		checkEditable();
		String signature = JavaConstructorInfo.createSignature(mName, JavaParameter.getTypeArray(pParameters));
		if (mConstructors.containsKey(signature)) {
			throw new JavaInfoException("Constructor '" + signature + "' already exists!");
		}
		JavaConstructorInfo constructorInfo = new JavaConstructorInfo(this, mFields, null, mName, pParameters);
		mConstructors.put(constructorInfo.getSignature(), constructorInfo);
		return constructorInfo;
	}
	
	public JavaMethodInfo createMethod(String pName, String pReturnType, JavaParameter... pParameters) throws JavaInfoException {
		checkEditable();
		if (!mTypeManager.existType(pReturnType)) {
			throw new JavaInfoException("Type '" + pReturnType + "' does not exist!");
		}
		for (int i=0; i<pParameters.length; i++) {
			if (!mTypeManager.existType(pParameters[i].getType())) {
				throw new JavaInfoException("Type '" + pParameters[i].getType() + "' does not exist!");
			}
		}
		if (!JavaNameValidation.isMethodNameValid(pName)) {
			throw new JavaInfoException("Method name '" + pName + "' is not valid!");
		}
		String signature = JavaMethodInfo.createSignature(pName, JavaParameter.getTypeArray(pParameters));
		if (mMethods.containsKey(pName)) {
			Map<String, JavaMethodInfo> methods = mMethods.get(pName);
			if (methods.containsKey(signature)) {
				throw new JavaInfoException("Method '" + signature + "' already exists!");
			}
			JavaMethodInfo methodInfo = new JavaMethodInfo(this, mFields, null, pName, pReturnType, pParameters);
			methods.put(signature, methodInfo);
			return methodInfo;
		}
		JavaMethodInfo methodInfo = new JavaMethodInfo(this, mFields, null, pName, pReturnType, pParameters);
		Map<String, JavaMethodInfo> methods = new HashMap<String, JavaMethodInfo>();
		methods.put(signature, methodInfo);
		mMethods.put(pName, methods);
		return methodInfo;
	}
	
	public boolean existField(String pName) {
		String signature = JavaFieldInfo.createSignature(pName);
		return mFields.containsKey(signature);
	}
	
	public boolean existConstructor(String... pParameters) {
		String signature = JavaConstructorInfo.createSignature(mName, pParameters);
		return mConstructors.containsKey(signature);
	}
	
	public boolean existMethod(String pName, String... pParameters) {
		String signature = JavaMethodInfo.createSignature(pName, pParameters);
		if (mMethods.containsKey(pName)) {
			Map<String, JavaMethodInfo> methods = mMethods.get(pName);
			if (methods.containsKey(signature)) {
				return true;
			}
			return false;
		}
		return false;
	}
	
	public JavaFieldInfo getField(String pName) throws JavaInfoException {
		String signature = JavaFieldInfo.createSignature(pName);
		if (!mFields.containsKey(signature)) {
			throw new JavaInfoException("Field '" + signature + "' does not exist!");
		}
		return mFields.get(signature);
	}
	
	public JavaConstructorInfo getConstructor(String... pParameters) throws JavaInfoException {
		String signature = JavaConstructorInfo.createSignature(mName, pParameters);
		if (!mConstructors.containsKey(signature)) {
			throw new JavaInfoException("Constructor '" + signature + "' does not exist!");
		}
		return mConstructors.get(signature);
	}
	
	public JavaMethodInfo getMethod(String pName, String... pParameters) throws JavaInfoException {
		String signature = JavaMethodInfo.createSignature(pName, pParameters);
		if (!mMethods.containsKey(pName)) {
			throw new JavaInfoException("Method '" + signature + "' does not exist for type '" + mName +"'!");
		}
		Map<String, JavaMethodInfo> methods = mMethods.get(pName);
		if (methods.size() == 1) {
			String key = methods.keySet().iterator().next();
			return methods.get(key);
		}
		if (!methods.containsKey(signature)) {
			throw new JavaInfoException("Method '" + signature + "' does not exist for type '" + mName +"'!");
		}
		return methods.get(signature);
	}
	
	protected void setEditable(boolean pEditable) {
		mEditable = pEditable;
	}
	
	private void checkEditable() {
		if (!mEditable) {
			throw new IllegalStateException("Type " + getSignature() + " is not editable!");
		}
	}

	@Override
	public boolean existVariable(String pSignature) {
		return mFields.containsKey(pSignature);
	}

	@Override
	public JavaVariableInfo getVariable(String pSignature) throws JavaInfoException {
		if (!mFields.containsKey(pSignature)) {
			throw new JavaInfoException("Variable '" + pSignature + "' does not exist!");
		}
		return mFields.get(pSignature);
	}

	@Override
	public JavaTypeInfo getParentType() {
		return this;
	}
}