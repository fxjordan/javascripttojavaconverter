package de.jstojconverter.jstdloader;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.jstojconverter.util.ThreeMethodLoader;

/**
 * @author Felix Jordan
 * @since 23.07.2015 - 22:33:39
 * @version 2.0
 */
public class JsTDManager extends ThreeMethodLoader<JsType> {
	
	private static final String DATA_FILE_ENDING = ".jstd";
	private static final String LIST_FILE_NAME = "types.list";
	private static final String DATA_CLASS_NAME_ENDINGS = "JSTD";
	private static final String LIST_CLASS_NAME = "TypesLIST";
	
	private Map<String, List<JsType>> mTypeDataMap;
	
	public JsTDManager() {
		super(DATA_FILE_ENDING, LIST_FILE_NAME, DATA_CLASS_NAME_ENDINGS, LIST_CLASS_NAME);
		mTypeDataMap = new HashMap<String, List<JsType>>();
	}
	
	@Override
	public List<JsType> onPreLoadFolder(String pExternalFolder, String pResourcePath) {
		String key = getKey(pExternalFolder, pResourcePath);
		if (mTypeDataMap.containsKey(key)) {
			return mTypeDataMap.get(key);
		}
		return null;
	}
	
	@Override
	public JsType onProcessLoadedItem(InputStream pInputStream) {
		return new JsTypeDataLoader(pInputStream).parseType();
	}
	
	@Override
	public void onPostLoadFolder(List<JsType> pResult, String pExternalFolder, String pResourcePath) {
		mTypeDataMap.put(getKey(pExternalFolder, pResourcePath), pResult);
	}
	
	private String getKey(String pExternalFolder, String pResourcePath) {
		return pExternalFolder + "&&&" + pResourcePath;
	}

	public JsType getForName(String pName) {
		Iterator<List<JsType>> iterator = mTypeDataMap.values().iterator();
		while (iterator.hasNext()) {
			List<JsType> types = iterator.next();
			for (JsType type : types) {
				if (type.getName().equals(pName)) {
					return type;
				}
			}
		}
		return null;
	}
}
