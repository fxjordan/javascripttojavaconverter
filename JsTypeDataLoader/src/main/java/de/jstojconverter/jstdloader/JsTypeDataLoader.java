package de.jstojconverter.jstdloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.jstojconverter.util.Log;
import de.jstojconverter.util.StringUtils;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 18:45:04
 * @version 1.0
 */
public class JsTypeDataLoader {
	
	public static final String LOG_TAG = "JsTypeDataLoader";
	
	private static final String PROPERTIES_TAG = "PROPERTIES:";
	private static final String METHODS_TAG = "METHODS:";
	
	private String mContent;
	
	public JsTypeDataLoader(InputStream pInputStream) {
		StringBuilder contentBuilder = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(pInputStream));
			String line = null;
			while ((line = reader.readLine()) != null) {
				contentBuilder.append(line);
			}
			mContent = contentBuilder.toString();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Cannot load jstd file", e);
		} catch (IOException e) {
			throw new RuntimeException("Cannot load jstd file", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Log.e(LOG_TAG, "Cannot close InputStream");
					e.printStackTrace();
				}
			}
		}
	}
	
	public JsTypeDataLoader(File pFile) {
		StringBuilder contentBuilder = new StringBuilder();
		BufferedReader reader = null;
		try {
			InputStream in = new FileInputStream(pFile);
			reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				contentBuilder.append(line);
			}
			mContent = contentBuilder.toString();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Cannot load jstd file", e);
		} catch (IOException e) {
			throw new RuntimeException("Cannot load jstd file", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Log.e(LOG_TAG, "Cannot close InputStream");
					e.printStackTrace();
				}
			}
		}
	}
	
	public JsType parseType() {
		Log.d(LOG_TAG, "====================");
		int firstSemicolon = mContent.indexOf(';');
		String typeInfo = StringUtils.removeSpaceAndTabOutside(mContent.substring(5, firstSemicolon));
		JsType type = createTypeObject(typeInfo);
		
		//----Properties:
		int propertiesStartIndex = mContent.indexOf(PROPERTIES_TAG);
		if (propertiesStartIndex == -1) {
			throw new RuntimeException("Missing 'PROPERTIES:' tag in jstd file!");
		}
		propertiesStartIndex += PROPERTIES_TAG.length();
		
		int propertiesEndIndex = mContent.indexOf(METHODS_TAG);
		if (propertiesEndIndex == -1) {
			throw new RuntimeException("Missing 'METHODS:' tag in jstd file!");
		}
		propertiesEndIndex -= 1;
		
		String propertiesData = StringUtils.removeSpaceAndTabOutside(mContent.substring(propertiesStartIndex,
				propertiesEndIndex + 1));
		parseProperties(type, propertiesData);
		
		propertiesEndIndex += METHODS_TAG.length() + 1;
		
		int methodsEndIndex = mContent.length();
		
		String methodsData = StringUtils.removeSpaceAndTabOutside(mContent.substring(propertiesEndIndex, methodsEndIndex));
		parseMethods(type, methodsData);
		
		return type;
	}
	
	private void parseMethods(JsType pType, String pMethodData) {
		Log.d(LOG_TAG, "Parsing methods...");
		String[] methods = pMethodData.split(";");
		for (int i=0; i<methods.length; i++) {
			methods[i] = StringUtils.removeSpaceAndTabOutside(methods[i]);
			JsMethod method = createMethod(methods[i]);
			pType.addMethod(method);
		}
	}
	
	private JsMethod createMethod(String pMethodString) {
		Log.d(LOG_TAG, "==");
		Log.d(LOG_TAG, "Method string: " + pMethodString);
		int parametersStartIndex = pMethodString.indexOf('(');
		int returnTypeEndIndex = pMethodString.indexOf(']');
		String name = StringUtils.removeSpaceAndTabOutside(pMethodString.substring(returnTypeEndIndex+1,
				parametersStartIndex));
		Log.d(LOG_TAG, "Method name: " + name);
		
		String returnTypesData = pMethodString.substring(1, returnTypeEndIndex);
		String[] returnTypes = returnTypesData.split(",");
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Return types:");
		for (int i=0; i<returnTypes.length; i++) {
			returnTypes[i] = StringUtils.removeSpaceAndTabOutside(returnTypes[i]);
			messageBuilder.append(" " + returnTypes[i]);
		}
		Log.d(LOG_TAG, messageBuilder.toString());
		
		String parameterData = pMethodString.substring(parametersStartIndex+1, pMethodString.length()-1);
		String[] parameters = parameterData.split(",");
		messageBuilder.setLength(0);
		messageBuilder.append("Parameters:");
		for (int i=0; i<parameters.length; i++) {
			parameters[i] = StringUtils.removeSpaceAndTabOutside(parameters[i]);
			messageBuilder.append(" " + parameters[i]);
		}
		Log.d(LOG_TAG, messageBuilder.toString());
		
		return new JsMethod(name, returnTypes, parameters);
	}
	
	private void parseProperties(JsType pType, String pPropertyData) {
		Log.d(LOG_TAG, "Parsing properties...");
		String[] properties = pPropertyData.split(";");
		for (int i=0; i<properties.length; i++) {
			properties[i] = StringUtils.removeSpaceAndTabOutside(properties[i]);
			if (properties[i].isEmpty()) {
				continue;
			}
			JsProperty property = createProperty(properties[i]);
			pType.addProperty(property);
		}
	}
	
	private JsProperty createProperty(String pPropertyString) {
		Log.d(LOG_TAG, "==");
		String[] parts = pPropertyString.substring(0, pPropertyString.length()).split(" ");
		String name = parts[0];
		Log.d(LOG_TAG, "Property name: " + name);
		String type = parts[1];
		Log.d(LOG_TAG, "Property type: " + type);
		
		return new JsProperty(name, type);
	}
	
	private JsType createTypeObject(String pTypeInfo) {
		Log.d(LOG_TAG, "Parsing new js type data...");
		String typeName = StringUtils.removeSpaceAndTabOutside(pTypeInfo);
		Log.d(LOG_TAG, "Js type name: " + typeName);
		
		return new JsType(typeName);
	}
}
