package de.jstojconverter.jstdloader;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 15:43:23
 * @version 1.0
 */
public class JsType {
	
	private String mName;
	private List<JsProperty> mProperties;
	private List<JsMethod> mMethods;
	
	protected JsType(String pName) {
		mName = pName;
		mProperties = new ArrayList<JsProperty>();
		mMethods = new ArrayList<JsMethod>();
	}
	
	public String getName() {
		return mName;
	}
	
	public List<JsProperty> getProperties() {
		return mProperties;
	}
	
	public List<JsMethod> getMethods() {
		return mMethods;
	}
	
	protected void addProperty(JsProperty pProperty) {
		mProperties.add(pProperty);
	}
	
	protected void addMethod(JsMethod pMethod) {
		mMethods.add(pMethod);
	}
}