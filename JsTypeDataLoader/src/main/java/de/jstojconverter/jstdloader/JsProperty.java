package de.jstojconverter.jstdloader;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 15:45:47
 * @version 1.0
 */
public class JsProperty {
	
	private String mName;
	private String mType;
	
	protected JsProperty(String pName, String pType) {
		mName = pName;
		mType = pType;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getType() {
		return mType;
	}
}