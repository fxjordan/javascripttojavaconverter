package de.jstojconverter.jstdloader;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 15:45:52
 * @version 1.0
 */
public class JsMethod {
	
	private String mName;
	private String[] mReturnTypes;
	private String[] mParameters;
	
	protected JsMethod(String pName, String[] pReturnTypes, String[] pParameters) {
		mName = pName;
		mReturnTypes = pReturnTypes;
		mParameters = pParameters;
	}
	
	public String getName() {
		return mName;
	}
	
	public String[] getReturnTypes() {
		return mReturnTypes;
	}
	
	public String[] getParameters() {
		return mParameters;
	}
}
