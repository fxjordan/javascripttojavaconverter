package de.jstojconverter.jstdloader;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 20:41:15
 * @version 1.0
 */
public class JsTypeDataLoaderTest {

	public static final String LOG_TAG = "JsTypeDataLoaderTest";
	
	private static final String EXTERNAL_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "JsTypeDataLoader/src/main/resources/jstd_test_files";
	private static final String INTERNAL_RESOURCE_PATH = "jstd_test_files";

	public static void main(String[] pArgs) {
		new JsTypeDataLoaderTest();
	}
	
	public JsTypeDataLoaderTest() {
		JsTDManager jstdManager = new JsTDManager();
		
		jstdManager.load(EXTERNAL_FOLDER, INTERNAL_RESOURCE_PATH);
	}
}
