function List<JsContent> getContent(String pContent, boolean pLog) throws JsReaderException {
		List<JsContent> contentList = new ArrayList<JsContent>();
		String currentKeyword = null;
		boolean waitForClose = false;
		boolean waitForSemicolon = false;
		int openedCodeBlocks = 0;
		int begin = 0;
		for (int i=0; i<pContent.length(); i++) {
			if (currentKeyword == null) {
				currentKeyword = StringUtils.beginsWithOneOf(pContent, i, JS_CONTENT_KEYWORDS);
				if (currentKeyword == null) {
					char character = pContent.charAt(i);
					if (character == ';' && CodeValidation.isCodeValid(pContent, i)) {
						int type = JsContent.TYPE_STATEMENT;
						String text = StringUtils.removeSpaceAndTabOutside(pContent.substring(begin, i+1));
						JsContent content = new JsContent(text, type);
						contentList.add(content);
						begin = i+1;
						if (pLog) {
							printElement(content);
						}
					}
				} else {
					waitForClose = true;
					if (currentKeyword.equals("do")) {
						waitForSemicolon = true;
					}
				}
			} else {
				char character = pContent.charAt(i);
				if (character == '}' && CodeValidation.isCodeValid(pContent, i)) {
					openedCodeBlocks--;
					waitForClose = false;
				} else {
					if (character == '{' && CodeValidation.isCodeValid(pContent, i)) {
						openedCodeBlocks++;
					}
				}
				if (openedCodeBlocks == 0 && !waitForClose && waitForSemicolon) {
					if (character == ';' && CodeValidation.isCodeValid(pContent, i)) {
						waitForSemicolon = false;
					}
				}
				if (openedCodeBlocks == 0 && !waitForClose && !waitForSemicolon) {
					int type = getElementType(currentKeyword);
					
					String contentText = pContent.substring(begin, i+1);
					
					int bracketIndex = firstValidIndexOf(contentText, '{');
					if (bracketIndex == -1) {
						throw new JsReaderException("Cannot find valid bracket");
					}
					
					String text;
					if (type == JsContent.TYPE_DO_WHILE_LOOP) {
						int lastBracketIndex = lastValidIndexOf(contentText, '}');
						if (lastBracketIndex == -1) {
							throw new JsReaderException("Cannot find valid bracket");
						}
						text = StringUtils.removeSpaceAndTabOutside(contentText.substring(lastBracketIndex+1, contentText.length()-1));
						contentText = contentText.substring(bracketIndex+1, lastBracketIndex);
					} else {
						text = StringUtils.removeSpaceAndTabOutside(contentText.substring(0, bracketIndex));
						contentText = contentText.substring(bracketIndex+1, contentText.length()-1);
					}
					contentText = StringUtils.removeSpaceAndTabOutside(contentText);
					
					JsContent content = new JsContent(text, type);
					content.addContents(getContent(contentText, false));
					if (pLog) {
						printElement(content);
					}
					contentList.add(content);
					begin = i+1;
					currentKeyword = null;
				}
			}
		}
		return contentList;
	}