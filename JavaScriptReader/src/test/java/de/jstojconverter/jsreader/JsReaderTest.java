package de.jstojconverter.jsreader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

/**
 * @author Felix Jordan
 * @since 03.08.2015 - 19:26:14
 * @version 1.0
 */
public class JsReaderTest extends TestCase {
	
	private static final String TEST_FILE_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "JavaScriptReader/test-files/";
	
	public void testAnonymusFunction() {
		File file = new File(TEST_FILE_FOLDER, "anonymus-function-test.js");
		JsReader reader = new JsReader(file);
		List<JsReaderContent> content = new ArrayList<JsReaderContent>();
		try {
			content = reader.readContent();
		} catch (JsReaderException e) {
			throw new RuntimeException("Cannot read JavaScript", e);
		}
		assertEquals("function boo()", content.get(0).getText());
		assertEquals("var x = function(a, b) {			return a * b;		};", content.get(1).getText());
		assertEquals("console.log(x(42, 73));", content.get(2).getText());
		assertEquals("function foo()", content.get(3).getText());
		assertEquals("var y = function(a, b) {			return a / b;		};", content.get(4).getText());
		assertEquals("console.log(y(21, 37));", content.get(5).getText());
	}
}
