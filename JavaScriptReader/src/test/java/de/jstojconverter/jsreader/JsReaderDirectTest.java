package de.jstojconverter.jsreader;

import java.io.File;

public class JsReaderDirectTest {
	
	private static final boolean RUNNING_ON_ANDROID = true;
	
	public static void main(String[] args) {
		new JsReaderDirectTest();
	}
	
	public JsReaderDirectTest() {
		File file;
		if (RUNNING_ON_ANDROID) {
			file = new File("/storage/emulated/0/AppProjects/JavaScriptToJavaConverter/JavaScriptReader/js-input/annonymus-function.js");
		} else {
			file = new File("E:/EclipseJavaEE/workspace/JavaScriptReader/js-input/annonymous-function.js");
		}
		JsReader reader = new JsReader(file);
		
		try {
			reader.readContent();
		} catch (JsReaderException e) {
			throw new RuntimeException("Cannot read JavaScript", e);
		}
	}
}
