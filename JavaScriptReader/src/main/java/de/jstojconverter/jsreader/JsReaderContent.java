package de.jstojconverter.jsreader;

import java.util.ArrayList;
import java.util.List;

public class JsReaderContent {
	
	public static final int TYPE_STATEMENT = 0;
	public static final int TYPE_IF_CONDITION = 1;
	public static final int TYPE_ELSE_CONDITION = 2;
	public static final int TYPE_ELE_IF_CONDITION = 3;
	public static final int TYPE_SWITCH_CONDITION = 4;
	public static final int TYPE_WHILE_LOOP = 5;
	public static final int TYPE_DO_WHILE_LOOP = 6;
	public static final int TYPE_FOR_LOOP = 7;
	public static final int TYPE_FUNCTION_DECLARATION = 8;
	
	private final String mText;
	private final int mType;
	private final List<JsReaderContent> mContent;

	public JsReaderContent(String pContent, int pType) {
		mText = pContent;
		mType = pType;
		if (mType != TYPE_STATEMENT) {
			mContent = new ArrayList<JsReaderContent>();
		} else {
			mContent = null;
		}
	}
	
	@Override
	public String toString() {
		return mText;
	}
	
	public String getText() {
		return mText;
	}
	
	public int getType() {
		return mType;
	}
	
	public boolean isContainer() {
		return mType != TYPE_STATEMENT;
	}
	
	public List<JsReaderContent> getContent() {
		if (!isContainer()) {
			throw new IllegalStateException("JsContent is no container");
		}
		return mContent;
	}
	
	public void addContent(int pPosition, JsReaderContent pContent) {
		if (!isContainer()) {
			throw new IllegalStateException("JsContent is no container");
		}
		mContent.add(pPosition, pContent);
	}
	
	public void addContent(JsReaderContent pContent) {
		if (!isContainer()) {
			throw new IllegalStateException("JsContent is no container");
		}
		mContent.add(pContent);
	}
	
	protected void addContents(List<JsReaderContent> pContents) {
		if (!isContainer()) {
			throw new IllegalStateException("JsContent is no container");
		}
		mContent.addAll(pContents);
	}
}
