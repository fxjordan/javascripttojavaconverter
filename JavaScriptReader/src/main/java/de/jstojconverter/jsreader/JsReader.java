package de.jstojconverter.jsreader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.util.CodeValidation;
import de.jstojconverter.util.Log;
import de.jstojconverter.util.StringUtils;

public class JsReader {
	
	private static final String LOG_TAG = "JsReader";
	
	private final String mContent;
	
	public JsReader(String pContent) {
		mContent = pContent;
	}
	
	public JsReader(File pFile) {
		StringBuilder contentBuilder = new StringBuilder();
		BufferedReader reader = null;
		try {
			InputStream in = new FileInputStream(pFile);
			reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				contentBuilder.append(line);
			}
			mContent = contentBuilder.toString();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Cannot load script file", e);
		} catch (IOException e) {
			throw new RuntimeException("Cannot load script file", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Log.e(LOG_TAG, "Cannot close InputStream");
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<JsReaderContent> readContent() throws JsReaderException {
		return getContent(mContent, true);
	}
	
	private List<JsReaderContent> getContent(String pContent, boolean pLog) throws JsReaderException {
		if (pLog) {
			Log.d(LOG_TAG, "Reding JavaScript code from file...");
		}
		List<JsReaderContent> contentList = new ArrayList<JsReaderContent>();
		String currentKeyword = null;
		boolean waitForClose = false;
		boolean waitForSemicolon = false;
		boolean waitForSemicolonB = true;
		boolean waitForCloseB = false;
		int openedCodeBlocks = 0;
		int openedCodeBlocksB = 0;
		int begin = 0;
		for (int i=0; i<pContent.length(); i++) {
			if (currentKeyword == null) {
				currentKeyword = StringUtils.beginsWithOneOf(pContent, i, JS_CONTENT_KEYWORDS);
				if (!isAllowedBeforeKeyword(pContent, i-1)) {
					currentKeyword = null;
				}
				if (currentKeyword == null) {
					char character = pContent.charAt(i);
					if (character == '}' && CodeValidation.isCodeValid(pContent, i)) {
						openedCodeBlocksB--;
						waitForCloseB = false;
					} else {
						if (character == '{' && CodeValidation.isCodeValid(pContent, i)) {
							openedCodeBlocksB++;
						}
					}
					if (openedCodeBlocksB == 0 && !waitForCloseB && waitForSemicolonB) {
						if (character == ';' && CodeValidation.isCodeValid(pContent, i)) {
							waitForSemicolonB = false;
						}
					}
					if (openedCodeBlocksB == 0 && !waitForCloseB && !waitForSemicolonB) {
						int type = JsReaderContent.TYPE_STATEMENT;
						String text = StringUtils.removeSpaceAndTabOutside(pContent.substring(begin, i+1));
						JsReaderContent content = new JsReaderContent(text, type);
						contentList.add(content);
						begin = i+1;
						if (pLog) {
							printElement(content);
						}
						waitForSemicolonB = true;
					}
//					if (character == ';' && CodeValidation.isCodeValid(pContent, i)) {
//						int type = JsReaderContent.TYPE_STATEMENT;
//						String text = StringUtils.removeSpaceAndTabOutside(pContent.substring(begin, i+1));
//						JsReaderContent content = new JsReaderContent(text, type);
//						contentList.add(content);
//						begin = i+1;
//						if (pLog) {
//							printElement(content);
//						}
//					}
				} else {
					waitForClose = true;
					if (currentKeyword.equals("do ")) {
						waitForSemicolon = true;
					}
				}
			} else {
				char character = pContent.charAt(i);
				if (character == '}' && CodeValidation.isCodeValid(pContent, i)) {
					openedCodeBlocks--;
					waitForClose = false;
				} else {
					if (character == '{' && CodeValidation.isCodeValid(pContent, i)) {
						openedCodeBlocks++;
					}
				}
				if (openedCodeBlocks == 0 && !waitForClose && waitForSemicolon) {
					if (character == ';' && CodeValidation.isCodeValid(pContent, i)) {
						waitForSemicolon = false;
					}
				}
				if (openedCodeBlocks == 0 && !waitForClose && !waitForSemicolon) {
					int type = getElementType(currentKeyword);
					
					String contentText = pContent.substring(begin, i+1);
					
					int bracketIndex = firstValidIndexOf(contentText, '{');
					if (bracketIndex == -1) {
						throw new JsReaderException("Cannot find valid bracket");
					}
					
					String text;
					if (type == JsReaderContent.TYPE_DO_WHILE_LOOP) {
						int lastBracketIndex = lastValidIndexOf(contentText, '}');
						if (lastBracketIndex == -1) {
							throw new JsReaderException("Cannot find valid bracket");
						}
						text = StringUtils.removeSpaceAndTabOutside(contentText.substring(lastBracketIndex+1, contentText.length()-1));
						contentText = contentText.substring(bracketIndex+1, lastBracketIndex);
					} else {
						text = StringUtils.removeSpaceAndTabOutside(contentText.substring(0, bracketIndex));
						contentText = contentText.substring(bracketIndex+1, contentText.length()-1);
					}
					contentText = StringUtils.removeSpaceAndTabOutside(contentText);
					
					JsReaderContent content = new JsReaderContent(text, type);
					content.addContents(getContent(contentText, false));
					if (pLog) {
						printElement(content);
					}
					contentList.add(content);
					begin = i+1;
					currentKeyword = null;
				}
			}
		}
		return contentList;
	}
	
	private boolean isAllowedBeforeKeyword(String pString, int pIndex) {
		if (pIndex < 0) {
			return true;
		}
		char character = pString.charAt(pIndex);
		for (char allowed : ALLOWED_BEFORE_KEYWORD) {
			if(character == allowed) {
				return true;
			}
		}
		return false;
	}

	private int firstValidIndexOf(String pString, char pCharacter) {
		int bracketIndex = -1;
		while ((bracketIndex = pString.indexOf(pCharacter, bracketIndex+1)) != -1) {
			if (CodeValidation.isCodeValid(pString, bracketIndex)) {
				break;
			}
		}
		return bracketIndex;
	}
	
	private int lastValidIndexOf(String pString, char pCharacter) {
		int bracketIndex = pString.length();
		while ((bracketIndex = pString.lastIndexOf(pCharacter, bracketIndex-1)) != -1) {
			if (CodeValidation.isCodeValid(pString, bracketIndex)) {
				break;
			}
		}
		if (bracketIndex == pString.length()) {
			return -1;
		}
		return bracketIndex;
	}
	
	private void printElement(JsReaderContent pElement) {
		Log.d("JsReader", "Parsed new element: (type=" + pElement.getType() + "; container=" + pElement.isContainer() + ")");
		Log.d("JsReader", "Element text: " + pElement.getText());
		if (pElement.isContainer()) {
			List<JsReaderContent> contentList = pElement.getContent();
			Log.d("JsReader", "Content: (size=" + contentList.size() + ")");
			for (JsReaderContent content : contentList) {
				printElement(content);
			}
		}
	}
	
	private int getElementType(String pKeyword) {
		if (pKeyword == null) {
			return JsReaderContent.TYPE_STATEMENT;
		}
		switch (pKeyword) {
		case "if ":
			return JsReaderContent.TYPE_IF_CONDITION;
		case "else ":
			return JsReaderContent.TYPE_ELSE_CONDITION;
		case "else if ":
			return JsReaderContent.TYPE_ELE_IF_CONDITION;
		case "switch ":
			return JsReaderContent.TYPE_SWITCH_CONDITION;
		case "while ":
			return JsReaderContent.TYPE_WHILE_LOOP;
		case "for ":
			return JsReaderContent.TYPE_FOR_LOOP;
		case "function ":
			return JsReaderContent.TYPE_FUNCTION_DECLARATION;
		case "do ":
			return JsReaderContent.TYPE_DO_WHILE_LOOP;
		}
		throw new RuntimeException("Unknown keyword '" + pKeyword + "'");
	}
	
	private static final char[] ALLOWED_BEFORE_KEYWORD = new char[] {
		' ', '}', '\n', '\t', ';'
	};
	
	// Longest value must be at beginning
	private static final String[] JS_CONTENT_KEYWORDS = new String[] {
		"function ", "if ", "else if ", "else ", "switch ", "while ", "for ", "do "
	};
}
