package de.jstojconverter.jsreader;

public class JsReaderException extends Exception {
	
	private static final long serialVersionUID = 82258393006755812L;

	public JsReaderException() {
	}

	public JsReaderException(String pMessage) {
		super(pMessage);
	}

	public JsReaderException(Throwable pThrowable) {
		super(pThrowable);
	}

	public JsReaderException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}

