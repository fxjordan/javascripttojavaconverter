package de.jstojconverter.util;

public class LogTest {
	
	public static final String LOG_TAG = "LogTest";
	
	public static void main(String[] pArgs) {
		new LogTest();
	}
	
	public LogTest() {
		Log.i(LOG_TAG, "Normal info log message");
		System.out.println("Message print to System.out");
		Log.e(LOG_TAG, "Normal error log message");
		System.err.println("Message print to System.err");
		Exception e = new Exception();
		Log.e(LOG_TAG, "Error message with exception", e);
		System.err.println("Exception print to System.err:");
		e.printStackTrace();
	}
}
