package de.jstojconverter.util;

import junit.framework.TestCase;

import de.jstojconverter.util.CodeValidation;

public class CodeValidationTest extends TestCase {
	
	public void testIsInsideString() {
		String string = "variable + \"foo\"";
		assertTrue(CodeValidation.isInsideStringDeclaration(string, 13));
		assertFalse(CodeValidation.isInsideStringDeclaration(string, 0));
		assertFalse(CodeValidation.isInsideStringDeclaration(string, 15));
		
		string = " \"boo\"  // \"foo\"";
		assertTrue(CodeValidation.isInsideStringDeclaration(string, 2));
		assertFalse(CodeValidation.isInsideStringDeclaration(string, 5));
	}
	
	public void testIsInsideChar() {
		String string = "variable + 'a'";
		assertTrue(CodeValidation.isInsideCharDeclaration(string, 12));
		assertFalse(CodeValidation.isInsideCharDeclaration(string, 0));
		assertFalse(CodeValidation.isInsideCharDeclaration(string, 11));
		
		string = " 'boo'  // 'foo'";
		assertTrue(CodeValidation.isInsideCharDeclaration(string, 2));
		assertFalse(CodeValidation.isInsideStringDeclaration(string, 5));
	}
	
	public void testIsInsideLineComment() {
		String string = "//a = 5;\n variable";
		assertFalse(CodeValidation.isInsideLineComment(string, 0));
		assertTrue(CodeValidation.isInsideLineComment(string, 2));
		assertFalse(CodeValidation.isInsideLineComment(string, 13));
		assertFalse(CodeValidation.isInsideLineComment(string, 8));
		
		string = "foo // boo";
		assertFalse(CodeValidation.isInsideLineComment(string, 4));
		assertFalse(CodeValidation.isInsideLineComment(string, 5));
		
		string = "foo // foo // boo";
		assertFalse(CodeValidation.isInsideLineComment(string, 4));
		assertFalse(CodeValidation.isInsideLineComment(string, 5));
		assertTrue(CodeValidation.isInsideLineComment(string, 6));
		assertTrue(CodeValidation.isInsideLineComment(string, 11));
		assertTrue(CodeValidation.isInsideLineComment(string, 12));
		
		// Line comment is avoided by string definition
		string = "foo \"//\" boo";
		assertFalse(CodeValidation.isInsideLineComment(string, 11));
	}
	
	public void testIsInsideBlockComment() {
		String sting = "foo /* boo */ foo";
		assertFalse(CodeValidation.isInsideBlockComment(sting, 2));
		assertFalse(CodeValidation.isInsideBlockComment(sting, 4));
		assertFalse(CodeValidation.isInsideBlockComment(sting, 5));
		assertTrue(CodeValidation.isInsideBlockComment(sting, 6));
		assertTrue(CodeValidation.isInsideBlockComment(sting, 10));
		assertFalse(CodeValidation.isInsideBlockComment(sting, 11));
		assertFalse(CodeValidation.isInsideBlockComment(sting, 12));
		assertFalse(CodeValidation.isInsideBlockComment(sting, 15));
	}
	
	public void testAllCodeValid() {
		String code = "foo \"/* boo 42  \"*/ foo // a '42'";
		assertTrue(CodeValidation.isCodeValid(code, 2));
		assertTrue(CodeValidation.isCodeValid(code, 4));
		assertFalse(CodeValidation.isCodeValid(code, 5));
		assertFalse(CodeValidation.isCodeValid(code, 10));
		assertTrue(CodeValidation.isCodeValid(code, 17));
		assertTrue(CodeValidation.isCodeValid(code, 24));
		assertFalse(CodeValidation.isCodeValid(code, 27));
	}
}
