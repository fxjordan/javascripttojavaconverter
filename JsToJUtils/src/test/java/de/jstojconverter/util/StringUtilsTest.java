package de.jstojconverter.util;

import de.jstojconverter.util.StringUtils;
import junit.framework.TestCase;

public class StringUtilsTest extends TestCase {
	
	public void testContainsOneOf() {
		String string = "10 - 5 / 3";
		assertEquals(3, StringUtils.containsOneOf(string, 0, '-', '/'));
		assertEquals(7, StringUtils.containsOneOf(string, 4, '-', '/'));
		assertEquals(3, StringUtils.containsOneOf(string, 3, '-', '/'));
	}
	
	private static final String[] KEYWORDS = new String[] {
		"if", "else", "else if", "switch", "while"
	};

	public void testBeginsWithKeywords() {
		String string = "if (fooCondition) {} else {}";
		assertEquals("if", StringUtils.beginsWithOneOf(string, 0, KEYWORDS));
		assertNull(StringUtils.beginsWithOneOf(string, 1, KEYWORDS));
		assertEquals("else", StringUtils.beginsWithOneOf(string, 21, KEYWORDS));
		assertNull(StringUtils.beginsWithOneOf(string, 20, KEYWORDS));
	}
	
	public void testLastValidIndexOf() {
		String string = "getPerson().setName(randomName());";
		assertEquals(32, StringUtils.lastValidIndexOf(string, ')'));
		assertEquals(30, StringUtils.lastValidIndexOf(string, '('));
	}
	
	public void testCountSequence() {
		assertEquals(3, StringUtils.countSequence("foo[]boo[][]", "[]"));
	}
}
