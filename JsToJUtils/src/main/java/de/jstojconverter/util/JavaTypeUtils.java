package de.jstojconverter.util;

public class JavaTypeUtils {
	
	private JavaTypeUtils() {
	}
	
	public static String[] splitType(String pType) {
		String[] data = new String[2];
		int lastDotIndex = pType.lastIndexOf('.');
		if (lastDotIndex == -1) {
			data[0] = "";
			data[1] = pType;
			return data;
		}
		data[0] = pType.substring(0, lastDotIndex);
		data[1] = pType.substring(lastDotIndex+1);
		return data;
	}
	
	public static String composeType(String pPackage, String pName) {
		if (pPackage.isEmpty()) {
			return pName;
		} else {
			return pPackage + "." + pName;
		}
	}
}
