package de.jstojconverter.util;

import java.util.Calendar;
import java.io.*;
import java.nio.charset.*;

/**
 * Helper class to easily print messages to the default output stream.
 * <br><br>
 * You can specify if the message should only be printed if the 'debug-mode' is enabled
 * in the {@link BuildConfig} class.
 * 
 * @author Felix Jordan
 * @since 22.01.2015 - 22:50:26
 * @version 1.0
 */
public class Log {
	
	private static final int MAX_TAG_LENGTH = 20;
	private static final String SEPERATOR = "    ";
	
	private static PrintStream mSystemOut;
	private static PrintStream mSystemErr;
	
	static {
		mSystemOut = System.out;
		mSystemErr = System.err;
		System.setOut(new PrintStream(new AutoLogOutputStream(Level.INFO, "System.out")));
		System.setErr(new PrintStream(new AutoLogOutputStream(Level.ERROR, "System.err")));
	}
	
	private Log() {
	}
	
	public static void d(String pTag, String pMessage) {
		log(Level.DEBUG, pTag, pMessage);
	}
	
	public static void d(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.DEBUG, pTag, pMessage, pThrowable);
	}
	
	public static void i(String pTag, String pMessage) {
		log(Level.INFO, pTag, pMessage);
	}
	
	public static void i(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.INFO, pTag, pMessage, pThrowable);
	}
	
	public static void w(String pTag, String pMessage) {
		log(Level.WARN, pTag, pMessage);
	}
	
	public static void w(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.WARN, pTag, pMessage, pThrowable);
	}
	
	public static void e(String pTag, String pMessage) {
		log(Level.ERROR, pTag, pMessage);
	}
	
	public static void e(String pTag, String pMessage, Throwable pThrowable) {
		log(Level.ERROR, pTag, pMessage, pThrowable);
	}
	
	public static void log(Level pLevel, String pTag, String pMessage) {
		String timeString = getTimeString();
		String tagString = append(pTag, MAX_TAG_LENGTH, " ", true);
		if (pTag.length() > MAX_TAG_LENGTH) {
			tagString = pTag.substring(0, MAX_TAG_LENGTH - 3) + "...";
		}
		String transformedMessage = pMessage.replace("\n", "\n" + charString(' ', 53));
		String text = "[" + pLevel.mShort + "]" + SEPERATOR  + timeString + SEPERATOR
				+ tagString + SEPERATOR + transformedMessage;
		if (pLevel.equals(Level.ERROR) || pLevel.equals(Level.WARN)) {
			mSystemErr.println(text);
		} else {
			mSystemOut.println(text);
		}
	}
	
	public static void log(Level pLevel, String pTag, String pMessage, Throwable pThrowable) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append(pMessage);
		messageBuilder.append('\n');
		messageBuilder.append(getStackTrace(pThrowable));
		log(pLevel, pTag, messageBuilder.toString());
	}

	private static String getStackTrace(Throwable pThrowable) {
		ByteArrayOutputStream out = null;
		try {
			out = new ByteArrayOutputStream();
			pThrowable.printStackTrace(new PrintStream(out));
			return new String(out.toByteArray(), Charset.forName("UTF-8"));
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private static String getTimeString() {
		Calendar calendar = Calendar.getInstance();
		// Calendar.JANUARY has value 0
		int month = calendar.get(Calendar.MONTH) + 1;
		int day = calendar.get(Calendar.DAY_OF_MONTH);
		int hour = calendar.get(Calendar.HOUR_OF_DAY);
		int minute = calendar.get(Calendar.MINUTE);
		int second = calendar.get(Calendar.SECOND);
		int millisecond = calendar.get(Calendar.MILLISECOND);
		
		String monthString = appendZeros(month, 2);
		String dayString = appendZeros(day, 2);
		String hourString = appendZeros(hour, 2);
		String minuteString = appendZeros(minute, 2);
		String secondString = appendZeros(second, 2);
		String millisecondString = appendZeros(millisecond, 3);
		
		return monthString + "-" + dayString + " " + hourString + ":" + minuteString + ":" + secondString + "." + millisecondString;
	}
	
	private static String appendZeros(int pInteger, int pLength) {
		return append(Integer.toString(pInteger), pLength, "0", false);
	}
	
	private static String charString(char pChar, int pCount) {
		StringBuilder builder = new StringBuilder();
		for (int i=0; i<pCount; i++) {
			builder.append(pChar);
		}
		return builder.toString();
	}
	
	private static String append(String pString, int pLength, String pExtra, boolean pSuffix) {
		String string = pString;
		while (string.length() < pLength) {
			if (pSuffix) {
				string = string + pExtra;
			} else {
				string = pExtra + string;
			}
		}
		return string;
	}
	
	public static enum Level {
		
		DEBUG("D"),
		INFO("I"),
		WARN("W"),
		ERROR("E");
		
		private String mShort;
		
		private Level(String pShort) {
			mShort = pShort;
		}
	}
	
	private static class AutoLogOutputStream extends OutputStream {
		
		private final Level mLevel;
		private final String mLogTag;
		private final StringBuilder mMessageBuilder;
		
		public AutoLogOutputStream(Level pLevel, String pLogTag) {
			mLevel = pLevel;
			mLogTag = pLogTag;
			mMessageBuilder = new StringBuilder();
		}
		
		@Override
		public void write(int pChar) throws IOException {
			char character = (char) pChar;
			if (character == '\n') {
				Log.log(mLevel, mLogTag, mMessageBuilder.toString());
				mMessageBuilder.setLength(0);
			} else {
				mMessageBuilder.append(character);
			}
		}
	}
}
