package de.jstojconverter.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
/**
 * @author Felix Jordan
 * @since 23.07.2015 - 21:50:37
 * @version 1.0
 * @param <T>
 */
public abstract class ThreeMethodLoader<T> {
	
	public static final String LOG_TAG = "ThreeLayerLoader";
	
	private final String mDataFileEnding;
	private final String mListFileName;
	private final String mDataClassEnding;
	private final String mListClassName;
	
	public ThreeMethodLoader(String pDataFileEnding, String pListFileName, String pDataClassNameEnding,
			String pListClassName) {
		mDataFileEnding = pDataFileEnding;
		mListFileName = pListFileName;
		mDataClassEnding = pDataClassNameEnding;
		mListClassName = pListClassName;
	}
	
	public List<T> load(String pExternalFolder, String pResourcePath) {
		File externalFolder = new File(pExternalFolder);
		if (externalFolder.exists()) {
			Log.i(LOG_TAG, "Loading data files from external folder...");
			return loadFromExternalFolder(externalFolder, pResourcePath);
		} else {
			Log.w(LOG_TAG, "External folder does not exist!");
			Log.i(LOG_TAG, "Loading data files from internal resource path...");
			try {
				return loadFromInternalResourcePath(pExternalFolder, pResourcePath);
			} catch (Exception e) {
				Log.e(LOG_TAG, "Exception while loading from internal resources. Using AIDE?");
				e.printStackTrace();
				Log.w(LOG_TAG, "Falling back to data classes (workaround)!");
				return loadFromDataClasses(pExternalFolder, pResourcePath);
			}
		}
	}
	
	/**
	 * 
	 * @return {@code null} if the loading should continue or return an alternative result, which
	 * 		will be returned directly.
	 */
	public abstract List<T> onPreLoadFolder(String pExternalFolder, String pResourcePath);
	
	public abstract T onProcessLoadedItem(InputStream pInputStream);
	
	public abstract void onPostLoadFolder(List<T> pResult, String pExternalFolder, String pResourcePath);
	
	private List<T> loadFromExternalFolder(File pExternalFolder, String pResourcePath) {
		if (!pExternalFolder.isDirectory()) {
			throw new IllegalArgumentException("'" + pExternalFolder.getAbsolutePath() + "' is not a directory");
		}
		
		List<T> alternative = onPreLoadFolder(pExternalFolder.getAbsolutePath(), pResourcePath);
		if (alternative != null) {
			return alternative;
		}
		
		List<File> files = getFiles(pExternalFolder);
		List<T> dataList = new ArrayList<T>(files.size());
		for (File dataFile : files) {
			InputStream inputStream = null;
			try {
				inputStream = new FileInputStream(dataFile);
				dataList.add(onProcessLoadedItem(inputStream));
			} catch(FileNotFoundException e) {
				throw new IllegalStateException("Could not find data file '" + dataFile.getAbsolutePath() + "'", e);
			} finally {
				if (inputStream != null) {
					try {
						inputStream.close();
					} catch (IOException e) {
						Log.w(LOG_TAG, "Failed to close InputStream!");
						e.printStackTrace();
					}
				}
			}
		}
		onPostLoadFolder(dataList, pExternalFolder.getAbsolutePath(), pResourcePath);
		return dataList;
	}

	private List<T> loadFromInternalResourcePath(String pExternalFolder, String pResourcePath) throws Exception {
		List<T> alternative = onPreLoadFolder(pExternalFolder, pResourcePath);
		if (alternative != null) {
			return alternative;
		}
		
		ClassLoader classloader = Thread.currentThread().getContextClassLoader();
		InputStream typesListInputStream = null;
		try {
			typesListInputStream = classloader.getResource(pResourcePath + "/" + mListFileName).openStream();
			BufferedReader listReader = new BufferedReader(new InputStreamReader(typesListInputStream));
			
			List<T> dataList = new ArrayList<T>();
			
			String dataName;
			while ((dataName = listReader.readLine()) != null) {
				String path = pResourcePath + "/" + dataName + mDataFileEnding;
				InputStream dataInputStream = null;
				try {
					dataInputStream = classloader.getResource(path).openStream();
					dataList.add(onProcessLoadedItem(dataInputStream));
				} catch (Exception e) {
					throw new Exception("Exception while loading data file (" + path +")!", e);
				} finally {
					if (dataInputStream != null) {
						dataInputStream.close();
					}
				}
			}
			
			onPostLoadFolder(dataList, pExternalFolder, pResourcePath);
			return dataList;
		} catch (Exception e) {
			throw new Exception("Exception while loading data list in folder (" + pResourcePath +")!", e);
		} finally {
			if (typesListInputStream != null) {
				typesListInputStream.close();
			}
		}
	}

	private List<T> loadFromDataClasses(String pExternalFolder, String pResourcePath) {
		onPreLoadFolder(pExternalFolder, pResourcePath);
		
		Class<?> dataListDataClass;
		try {
			dataListDataClass = Class.forName(pResourcePath + "." + mListClassName);
		} catch (ClassNotFoundException e) {
			throw new IllegalArgumentException("No '" + mListClassName + "' class exists in resource path '"
					+ pResourcePath + "'!", e);
		}
		InputStream dataListInputStream = null;
		try {
			dataListInputStream = (InputStream) dataListDataClass.getMethod("getData").invoke(null);
			BufferedReader listReader = new BufferedReader(new InputStreamReader(dataListInputStream));
			
			List<T> dataList = new ArrayList<T>();
			
			String dataName;
			while ((dataName = listReader.readLine()) != null) {
				String className = pResourcePath + "." + dataName.replace('/', '.') + mDataClassEnding;
				Class<?> dataClass;
				try {
					dataClass = Class.forName(className);
				} catch (ClassNotFoundException e) {
					throw new IllegalArgumentException("Data class '" + className
							+ "' does not exists on resource path '" + pResourcePath + "'!", e);
				} finally {
					if (dataListInputStream != null) {
						dataListInputStream.close();
					}
				}
				InputStream dataInputStream = null;
				try {
					dataInputStream = (InputStream) dataClass.getMethod("getData").invoke(null);
					dataList.add(onProcessLoadedItem(dataInputStream));
				} catch (Exception e) {
					throw new Exception("Exception while loading data class (" + className +")!", e);
				} finally {
					if (dataInputStream != null) {
						dataInputStream.close();
					}
				}
			}
			
			onPostLoadFolder(dataList, pExternalFolder, pResourcePath);
			return dataList;
		} catch (Exception e) {
			throw new RuntimeException("Exception while loading data from data classes on resource path ("
					+ pResourcePath +")!", e);
		} finally {
			if (dataListInputStream != null) {
				try {
					dataListInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private List<File> getFiles(File pFolder) {
		ArrayList<File> fileList = new ArrayList<File>();
		File[] files = pFolder.listFiles();
		for (int i=0; i<files.length; i++) {
			File file = files[i];
			if (file.isDirectory()) {
				fileList.addAll(getFiles(file));
			} else if (file.getName().endsWith(mDataFileEnding)) {
				fileList.add(file);
			}
		}
		return fileList;
	}
}
