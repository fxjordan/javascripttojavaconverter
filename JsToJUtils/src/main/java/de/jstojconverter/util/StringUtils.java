package de.jstojconverter.util;

public class StringUtils {

	private StringUtils() {
	}
	
	/**
	 * Removes all whitespace characters and tabulators at the beginning up to the first non-whitespace character
	 * and all from the end up to the last non-whitespace character.
	 * 
	 * @param pString The string to edit.
	 * @return A new string without whitespace characters and tabulators at beginning or end.
	 */
	public static String removeSpaceAndTabOutside(String pString) {
		try {
			if (pString.length() == 0) {
				return pString;
			}
			int begin = 0;
			while (pString.charAt(begin) == ' ' || pString.charAt(begin) == '\t') {
				begin++;
				if (begin == pString.length()) {
					return "";
				}
			}
			int end = pString.length() - 1;
			while (pString.charAt(end) == ' ' || pString.charAt(begin) == '\t') {
				end--;
				if (end == -1) {
					return "";
				}
			}
			return pString.substring(begin, end+1);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Counts how often a character exists inside a string.
	 * 
	 * @param pString The string to check.
	 * @param pCharacter The character count.
	 * @return The count of the character.
	 */
	public static int countCharacter(String pString, char pCharacter) {
		int count = 0;
		int index = -1;
		while ((index = pString.indexOf(pCharacter, index+1)) != -1) {
			count++;
		}
		return count;
	}
	
	/**
	 * Counts how often a sequence exists inside a string.
	 * 
	 * @param pString The string to check.
	 * @param pCharacter The character count.
	 * @return The count of the character.
	 */
	public static int countSequence(String pString, String pSequence) {
		int count = 0;
		int index = -1;
		while ((index = pString.indexOf(pSequence, index+1)) != -1) {
			count++;
		}
		return count;
	}

	/**
	 * Returns true of the string contains at least one of the given characters
	 * 
	 * @param pString The string to search.
	 * @param pCharacters The characters that must be inside.
	 * @return The index of the first found character, -1 if nothing was found.
	 */
	public static int containsOneOf(String pString, int pFromIndex, char... pCharacters) {
		for (int i=pFromIndex; i<pString.length(); i++) {
			char character = pString.charAt(i);
			for (int j=0; j<pCharacters.length; j++) {
				if (character == pCharacters[j]) {
					return i;
				}
			}
		}
		return -1;
	}
	
	/**
	 * Returns true of the string contains at least one of the given strings
	 * 
	 * @param pString The string to search.
	 * @param pCharacters The characters that must be inside.
	 * @return The index of the first found character, -1 if nothing was found.
	 */
	public static int containsOneOf(String pString, int pFromIndex, String... pValues) {
		for (int i=pFromIndex; i<pString.length(); i++) {
			if (beginsWithOneOf(pString, i, pValues) != null) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * Returns the first value that was found in the string or null;
	 * 
	 * @param pString The string to search.
	 * @param pValues The characters that must be inside.
	 * @return The value that was first found.
	 */
	public static String getFirstValidValue(String pString, int pFromIndex, String... pValues) {
		for (int i=pFromIndex; i<pString.length(); i++) {
			valueLoop:
			for (String value : pValues) {
				for (int j=0; j<value.length(); j++) {
					char character = pString.charAt(i+j);
					if (character != value.charAt(j)) {
						continue valueLoop;
					}
				}
				if (CodeValidation.isCodeValid(pString, i)) {
					return value;
				}
			}
		}
		return null;
	}
	
	/**
	 * Checks if the given string begins at given position with one of the given keywords.
	 * 
	 * @param pString The string to check.
	 * @param pFromIndex The index where the words should start.
	 * @param pBegins The keywords.
	 * @return The keyword, which begins at the position, or {@code null}.
	 */
	public static String beginsWithOneOf(String pString, int pFromIndex, String... pBegins) {
		for (int i=0; i<pBegins.length; i++) {
			String keyword = pBegins[i];
			int keywordIndex = 0;
			while (keyword.charAt(keywordIndex) == pString.charAt(pFromIndex + keywordIndex)) {
				if (keywordIndex == keyword.length() - 1) {
					return keyword;
				}
				keywordIndex++;
			}
		}
		return null;
	}
	
	public static int firstValidIndexOf(String pString, char pCharacter) {
		int index;
		while ((index = pString.indexOf(pCharacter)) != -1) {
			if (CodeValidation.isCodeValid(pString, index)) {
				return index;
			}
		}
		return -1;
	}
	
	public static int firstValidIndexOf(String pString, String pSearch) {
		int index = -1;
		while ((index = pString.indexOf(pSearch, index+1)) != -1) {
			if (CodeValidation.isCodeValid(pString, index)) {
				return index;
			}
		}
		return -1;
	}

	public static int lastValidIndexOf(String pString, char pCharacter) {
		int index = pString.length();
		while ((index = pString.lastIndexOf(pCharacter, index-1)) != -1) {
			if (CodeValidation.isCodeValid(pString, index)) {
				return index;
			}
		}
		return 0;
	}
}
