package de.jstojconverter.util;

public class CodeValidation {
	
	private CodeValidation() {
	}
	
	/**
	 * Returns whether the character at the index inside the given string
	 * is valid. That means the character is not:<br>
	 * ---- NOT inside a string definition<br>
	 * ---- NOT inside a character definition<br>
	 * ---- NOT inside a line comment<br>
	 * ---- NOT inside a block comment<br><br>
	 * 
	 * @param pString The string that contains the code.
	 * @param pIndex The index of the character to check.
	 * @return {@code true} of the character is valid, {@code false if not}.
	 * 
	 * @see #isCodeValid(String, int, boolean, boolean, boolean, boolean)
	 */
	public static boolean isCodeValid(String pString, int pIndex) {
		return isCodeValid(pString, pIndex, true, true, true, true);
	}
	
	/**
	 * Returns whether the character at the index inside the given string
	 * is valid. This method checks only the methods that are marked with {@code true}
	 * in the parameters.
	 * 
	 * @param pString The string that contains the code.
	 * @param pIndex The index of the character to check.
	 * @return {@code true} of the character is valid, {@code false if not}.
	 * 
	 * @see #isCodeValid(String, int)
	 */
	public static boolean isCodeValid(String pString, int pIndex, boolean pCheckString,
			boolean pCheckChar, boolean pCheckLineCommand, boolean pCheckBlockCommand) {
		if (pCheckString && isInsideStringDeclaration(pString, pIndex)) {
			return false;
		}
		if (pCheckChar && isInsideCharDeclaration(pString, pIndex)) {
			return false;
		}
		if (pCheckLineCommand && isInsideLineComment(pString, pIndex)) {
			return false;
		}
		if (pCheckBlockCommand && isInsideBlockComment(pString, pIndex)) {
			return false;
		}
		return true;
	}
	
	/**
	 * Returns whether the given index is inside a string declaration. In fact that means
	 * that the count of quote marks that are before the index is uneven.
	 * <br><br>
	 * The method returns false if the character at the index is a quote mark.
	 * 
	 * @param pString The in which the index is.
	 * @param pIndex The index to check.
	 * @return {@code true} if the index is inside, {@code false} if not.
	 */
	public static boolean isInsideStringDeclaration(String pString, int pIndex) {
		if (pString.charAt(pIndex) == '"') {
			return false;
		}
		int quoteMarksBefore = 0;
		int lastIndex = -1;
		while ((lastIndex = pString.indexOf('"', lastIndex+1)) != -1 && lastIndex < pIndex) {
			if (isCodeValid(pString, lastIndex)) {
				quoteMarksBefore++;
			}
		}
		return quoteMarksBefore % 2 == 1;
	}
	
	/**
	 * Returns whether the given index is inside a char declaration. In fact that means
	 * that the count of single quote marks that are before the index is uneven.
	 * <br><br>
	 * The method returns false if the character at the index is a single quote mark.
	 * 
	 * @param pString The string in which the index is.
	 * @param pIndex The index to check.
	 * @return {@code true} if the index is inside, {@code false} if not.
	 */
	public static boolean isInsideCharDeclaration(String pString, int pIndex) {
		if (pString.charAt(pIndex) == '\'') {
			return false;
		}
		int quoteMarksBefore = 0;
		int lastIndex = -1;
		while ((lastIndex = pString.indexOf('\'', lastIndex+1)) != -1 && lastIndex < pIndex) {
			if (isCodeValid(pString, lastIndex)) {
				quoteMarksBefore++;	
			}
		}
		return quoteMarksBefore % 2 == 1;
	}
	
	/**
	 * Returns whether the given index is inside a line comment.
	 * <br><br>
	 * The method returns false if the character at the index is one of the characters,
	 * which defined the comment.
	 * 
	 * @param pString The string in which the index is.
	 * @param pIndex The index to check.
	 * @return {@code true} if the index is inside, {@code false} if not.
	 */
	public static boolean isInsideLineComment(String pString, int pIndex) {
		if (pIndex < 2) {
			return false;
		}
		if (pString.charAt(pIndex) == '/') {
			if (pIndex - 1 >= 0) {
				if (pString.charAt(pIndex - 1) == '/'
						&& isCodeValid(pString, pIndex-1)) {
					return false;
				}
			}
			if (pIndex + 1 < pString.length()) {
				if (pString.charAt(pIndex+1) == '/'
						&& isCodeValid(pString, pIndex-1)) {
				}
			}
		}
		for (int i=pIndex; i>-1; i--) {
			char character = pString.charAt(i);
			// Line breaks can not be 'out commented'
			if (character == '\n') {
				// Line break so there could be not line comment
				return false;
			}
			
			if (character == '/' && i-1 >= 0 && pString.charAt(i-1) == '/'
					&& isCodeValid(pString, i-1)) {
				// Line command detected
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Returns whether the given index is inside a block comment.
	 * <br><br>
	 * The method returns false if the character is part of the begin or
	 * end of a block comment.
	 * 
	 * @param pString The string in which the index is.
	 * @param pIndex The index to check.
	 * @return {@code true} if the index is inside, {@code false} if not.
	 */
	public static boolean isInsideBlockComment(String pString, int pIndex) {
		if (pIndex < 2) {
			return false;
		}
		if (pString.charAt(pIndex) == '/') {
			if (pIndex + 1 < pString.length()) {
				if (pString.charAt(pIndex+1) == '*'
						&& isCodeValid(pString, pIndex-1, true, true, true, false)) {
					return false;
				}
			}
			if (pIndex - 1 >= 0) {
				if (pString.charAt(pIndex - 1) == '*'
						&& isCodeValid(pString, pIndex-1, true, true, true, false)) {
					return false;
				}
			}
		}
		if (pString.charAt(pIndex) == '*') {
			if (pIndex + 1 < pString.length()) {
				if (pString.charAt(pIndex+1) == '/'
						&& isCodeValid(pString, pIndex-1, true, true, true, false)) {
					return false;
				}
			}
			if (pIndex - 1 >= 0) {
				if (pString.charAt(pIndex - 1) == '/'
						&& isCodeValid(pString, pIndex-1, true, true, true, false)) {
					return false;
				}
			}
		}
		for (int i=pIndex; i>-1; i--) {
			char character = pString.charAt(i);
			if (character == '/' && i-1 >= 0 && pString.charAt(i-1) == '*'
					&& isCodeValid(pString, i-1, true, true, true, false)) {
				return false;
			}
			
			if (character == '*' && i-1 >= 0 && pString.charAt(i-1) == '/'
					&& isCodeValid(pString, i-1, true, true, true, false)) {
				return true;
			}
		}
		return false;
	}
}
