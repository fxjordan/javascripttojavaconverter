package de.jstojconverter.jscore;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * @author Felix Jordan
 * @since 22.07.2015 - 01:00:14
 * @version 1.0
 */
public class JsDoWhileLoop extends JsCodeBlock {
	
	private IJsValue mCondition;
	
	protected JsDoWhileLoop(JsCodeBlockInfo pCodeBlockInfo, IJsValue pCondition) {
		super(pCodeBlockInfo);
		mCondition = pCondition;
	}
	
	public IJsValue getCondition() {
		return mCondition;
	}
	
	@Override
	protected String getBlockBegin() {
		return "do";
	}
	
	@Override
	protected String getBlockEnd() {
		StringBuilder endBuilder = new StringBuilder();
		endBuilder.append("while (");
		endBuilder.append(mCondition.getValue());
		endBuilder.append(");");
		return endBuilder.toString();
	}
}
