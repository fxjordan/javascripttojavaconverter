package de.jstojconverter.jscore.info;

import java.util.HashMap;
import java.util.Map;

import de.jstojconverter.jscore.util.JsNameValidation;
import de.jstojconverter.jscore.util.JsParameter;

/**
 * @author Felix Jordan
 * @since 12.06.2015 - 22:36:11
 * @version 1.0
 */
public class JsCodeBlockInfo {
	
	private JsTypeInfo mParentType;
	private Map<String, JsFunctionInfo> mFunctions;
	private Map<String, JsVariableInfo> mGlobalVariables;
	private Map<String, JsVariableInfo> mVariables;
	private JsTypeManager mTypeManager;
	private boolean mEditable;
	
	/**
	 * Global variables map is just stored, so if a type adds a global variable it would be appear in this map.
	 * 
	 * Parent variables are copied to a new map, so that variables created after this code block will
	 * not appear in this list.
	 * 
	 * When the code block is created only the variables that re defined before
	 * the code block is defined.
	 * 
	 * @param pParentVariables
	 */
	protected JsCodeBlockInfo(JsTypeInfo pParentType, Map<String, JsVariableInfo> pGlobalVariables,
			Map<String, JsVariableInfo> pParentVariables) {
		mParentType = pParentType;
		mFunctions = new HashMap<String, JsFunctionInfo>();
		mGlobalVariables = pGlobalVariables;
		mVariables = new HashMap<String, JsVariableInfo>();
		if (pParentVariables != null) {
			mVariables.putAll(pParentVariables);
		}
		mTypeManager = JsTypeManager.getManager();
		mEditable = true;
	}
	
	public JsFunctionInfo createFunction(String pName, String[] pReturnTypes, JsParameter... pParameters) throws JsInfoException {
		checkEditable();
		for (String returnType : pReturnTypes) {
			if (!mTypeManager.existType(returnType)) {
				throw new JsInfoException("Type '" + returnType + "' does not exist!");
			}
		}
		for (JsParameter parameter : pParameters) {
			if (!mTypeManager.existType(parameter.getType())) {
				throw new JsInfoException("Type '" + parameter.getType() + "' does not exist!");
			}
		}
		if (!JsNameValidation.isMethodNameValid(pName)) {
			throw new JsInfoException("Method name '" + pName + "' is not valid!");
		}
		JsFunctionInfo methodInfo = new JsFunctionInfo(mParentType, mGlobalVariables, mVariables, pName, pReturnTypes, pParameters);
		mFunctions.put(pName, methodInfo);
		return methodInfo;
	}
	
	public JsCodeBlockInfo createCodeBlock() {
		JsCodeBlockInfo codeBlockInfo = new JsCodeBlockInfo(mParentType, mGlobalVariables, mVariables);
		return codeBlockInfo;
	}
	
	public JsVariableInfo createVariable(String pName, String pType) throws JsInfoException {
		checkEditable();
		if (!mTypeManager.existType(pType)) {
			throw new JsInfoException("Type '" + pType + "' does not exist!");
		}
		if (!JsNameValidation.isVariableNameValid(pName)) {
			throw new JsInfoException("Variable name '" + pName + "' is not valid!");
		}
		if (mVariables.containsKey(pName) || mGlobalVariables.containsKey(pName)) {
			throw new JsInfoException("Variable '" + pName + "' already exists!");
		}
		JsVariableInfo variableInfo = new JsVariableInfo(mParentType, pName, pType);
		mVariables.put(pName, variableInfo);
		return variableInfo;
	}
	
	public boolean existFunction(String pName) {
		if (mFunctions.containsKey(pName)) {
			return true;
		}
		return false;
	}
	
	public boolean existVariable(String pName) {
		if (mVariables.containsKey(pName)) {
			return true;
		}
		return mGlobalVariables.containsKey(pName);
	}
	
	public JsVariableInfo getVariable(String pName) throws JsInfoException {
		if (mVariables.containsKey(pName)) {
			return mVariables.get(pName);
		} else if (mGlobalVariables.containsKey(pName)) {
			return mGlobalVariables.get(pName);
		}
		throw new JsInfoException("Variable '" + pName + "' does not exist!");
	}
	
	public JsFunctionInfo getFunction(String pName) throws JsInfoException {
		if (mFunctions.containsKey(pName)) {
			return mFunctions.get(pName);
		}
		throw new JsInfoException("Variable '" + pName + "' does not exist!");
	}
	
	public JsTypeInfo getParentType() {
		return mParentType;
	}
	
	protected void setEditable(boolean pEditable) {
		mEditable = pEditable;
	}
	
	private void checkEditable() throws JsInfoException {
		if (!mEditable) {
			throw new JsInfoException("JsCodeBlock is not editable!");
		}
	}
}
