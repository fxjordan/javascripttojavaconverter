package de.jstojconverter.jscore.info;

/**
 * @author Felix Jordan
 * @since 10.06.2015 - 22:15:50
 * @version 1.0
 */
public class JsVariableInfo {
	
	private String mName;
	private String mType;
	private JsTypeInfo mParentType;
	
	protected JsVariableInfo(JsTypeInfo pParentType, String pName, String pType) {
		mName = pName;
		mType = pType;
		mParentType = pParentType;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getType() {
		return mType;
	}
	
	public JsTypeInfo getParentType() {
		return mParentType;
	}
}
