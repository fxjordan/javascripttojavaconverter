package de.jstojconverter.jscore.info;

import de.jstojconverter.jscore.JsCoreException;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 21:45:27
 * @version 1.0
 */
public class JsInfoException extends JsCoreException {
	
	private static final long serialVersionUID = 5366442792000522491L;
	
	public JsInfoException() {
	}

	public JsInfoException(String pMessage) {
		super(pMessage);
	}

	public JsInfoException(Throwable pThrowable) {
		super(pThrowable);
	}

	public JsInfoException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}