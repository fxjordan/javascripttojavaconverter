package de.jstojconverter.jscore.info;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jstojconverter.jscore.util.JsNameValidation;
import de.jstojconverter.jscore.util.JsParameter;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jstdloader.JsMethod;
import de.jstojconverter.jstdloader.JsProperty;
import de.jstojconverter.jstdloader.JsTDManager;
import de.jstojconverter.jstdloader.JsType;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 18:29:07
 * @version 1.0
 */
public class JsTypeManager {
	
	public static final String LOG_TAG = "JsTypeManager";
	
	private static final String External_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "JsCore/src/main/resources/jstd_files";
	private static final String INTERNAL_RESOURCE_PATH = "jstd_files";
	
	private static JsTypeManager sInstance;
	
	/** Types mapped to their signatures */
	private Map<String, JsTypeInfo> mTypeMap;
	private JsTDManager mJsTDManager;
	
	private JsTypeManager() {
		mTypeMap = new HashMap<String, JsTypeInfo>();
		mJsTDManager = new JsTDManager();
		mJsTDManager.load(External_FOLDER, INTERNAL_RESOURCE_PATH);
	}
	
	public static JsTypeManager getManager() {
		if (sInstance == null) {
			sInstance = new JsTypeManager();
			sInstance.createBuildInTypes();
			sInstance.createLoadedTypes();
			return sInstance;
		}
		return sInstance;
	}
	
	public JsTypeInfo createType(String pType) throws JsInfoException {
		if (!JsNameValidation.isTypeNameValid(pType)) {
			throw new JsInfoException("Type name '" + pType + "' is not valid.");
		}
		if (mTypeMap.containsKey(pType)) {
			throw new JsInfoException("The type '" + pType + "' already exists.");
		}
		JsTypeInfo typeInfo = new JsTypeInfo(pType);
		mTypeMap.put(pType, typeInfo);
		return typeInfo;
	}
	
	public boolean existType(String pType) {
		if (mTypeMap.containsKey(pType)) {
			return true;
		}
		JsTypeInfo typeInfo;
		try {
			typeInfo = loadJsTDFile(pType);
		} catch (JsInfoException e) {
			throw new RuntimeException("Internal error. Failed to load jstd file!", e);
		}
		if (typeInfo == null) {
			return false;
		}
		return true;
	}
	
	public JsTypeInfo getType(String pType) throws JsInfoException {
		if (existType(pType)) {
			return mTypeMap.get(pType);
		}
		throw new JsInfoException("The type '" + pType + "' does not exist!");
	}
	
	private JsTypeInfo loadJsTDFile(String pName) throws JsInfoException {
		JsType type = mJsTDManager.getForName(pName);
		if (type == null) {
			return null;
		}
		JsTypeInfo typeInfo = createType(pName);
		createLoadedProperties(typeInfo, type.getProperties());
		createLoadedMethods(typeInfo, type.getMethods());
		return typeInfo;
	}
	
	private void createBuildInTypes() {
		try {
			createType(IJsValue.TYPE_BOOLEAN);
			createType(IJsValue.TYPE_NUMBER);
			createType(IJsValue.TYPE_OBJECT);
			createType(IJsValue.TYPE_STRING);
			createType(IJsValue.TYPE_FUNCTION);
			createType(IJsValue.TYPE_UNDEFINED);
			
			createType(IJsValue.TYPE_ARRAY);
		} catch (JsInfoException e) {
			throw new RuntimeException("Internal error. Failed to create built-in types!", e);
		}
	}
	
	private void createLoadedTypes() {
		try {
			List<JsType> types = mJsTDManager.load(External_FOLDER, INTERNAL_RESOURCE_PATH);
			for (JsType type : types) {
				if (!existType(type.getName())) {
					// TODO This code is probably dead, because the types are created inside the 'existType' method.
					JsTypeInfo typeInfo = createType(type.getName());
					createLoadedProperties(typeInfo, type.getProperties());
					createLoadedMethods(typeInfo, type.getMethods());
				}
			}
		} catch (JsInfoException e) {
			throw new RuntimeException("Internal error. Failed to create loaded type!");
		}
	}
	
	private void createLoadedProperties(JsTypeInfo pTypeInfo, List<JsProperty> pProperties) throws JsInfoException {
		for (JsProperty property : pProperties) {
			pTypeInfo.createProperty(property.getName(), property.getType());
		}
	}
	
	private void createLoadedMethods(JsTypeInfo pTypeInfo, List<JsMethod> pMethods) throws JsInfoException {
		for (JsMethod method : pMethods) {
			String[] types = method.getParameters();
			JsParameter[] parameters = new JsParameter[types.length];
			for (int i=0; i<parameters.length; i++) {
				parameters[i] = createParameter(types[i]);
			}
			pTypeInfo.createMethod(method.getName(), method.getReturnTypes(), parameters);
		}
	}
	
	private JsParameter createParameter(String pType) {
		int r = (int) (Math.random() * 10);
		String name = pType + Integer.toString(r);
		return new JsParameter(name, pType);
	}
}
