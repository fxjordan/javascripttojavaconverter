package de.jstojconverter.jscore.info;

import java.util.HashMap;
import java.util.Map;

import de.jstojconverter.jscore.JsFile;
import de.jstojconverter.jscore.util.JsNameValidation;
import de.jstojconverter.jscore.util.JsParameter;

/**
 * @author Felix Jordan
 * @since 10.06.2015 - 16:07:52
 * @version 1.0
 */
public class JsTypeInfo {
	
	private String mName;
	private Map<String, JsVariableInfo> mProperties;
	private Map<String, JsFunctionInfo> mMethods;
	private JsCodeBlockInfo mHeadlessCodeBlock;
	private JsTypeManager mTypeManager;
	private boolean mEditable;
	
	protected JsTypeInfo(String pName) {
		mName = pName;
		mProperties = new HashMap<String, JsVariableInfo>();
		mMethods = new HashMap<String, JsFunctionInfo>();
		mTypeManager = JsTypeManager.getManager();
		mEditable = true;
	}
	
	public String getName() {
		return mName;
	}
	
	public JsVariableInfo createProperty(String pName, String pType) throws JsInfoException {
		checkEditable();
		if (!mTypeManager.existType(pType)) {
			throw new JsInfoException("Type '" + pType + "' does not exist!");
		}
		if (!JsNameValidation.isVariableNameValid(pName)) {
			throw new JsInfoException("Property name '" + pName + "' is not valid!");
		}
		if (mProperties.containsKey(pName)) {
			throw new JsInfoException("Property '" + pName + "' already exists!");
		}
		JsVariableInfo propertyInfo = new JsVariableInfo(this, pName, pType);
		mProperties.put(pName, propertyInfo);
		return propertyInfo;
	}
	
	public JsFunctionInfo createMethod(String pName, String[] pReturnTypes, JsParameter... pParameters) throws JsInfoException {
		checkEditable();
		for (String returnType : pReturnTypes) {
			if (!mTypeManager.existType(returnType)) {
				throw new JsInfoException("Type '" + returnType + "' does not exist!");
			}
		}
		for (JsParameter parameter : pParameters) {
			if (!mTypeManager.existType(parameter.getType())) {
				throw new JsInfoException("Type '" + parameter.getType() + "' does not exist!");
			}
		}
		if (!JsNameValidation.isMethodNameValid(pName)) {
			throw new JsInfoException("Method name '" + pName + "' is not valid!");
		}
		JsFunctionInfo methodInfo = new JsFunctionInfo(this, mProperties, null, pName, pReturnTypes, pParameters);
		mMethods.put(pName, methodInfo);
		return methodInfo;
	}
	
	public boolean existProperty(String pName) {
		return mProperties.containsKey(pName);
	}
	
	public boolean existMethod(String pName) {
		return mMethods.containsKey(pName);
	}
	
	public JsVariableInfo getProperty(String pName) throws JsInfoException {
		if (!mProperties.containsKey(pName)) {
			throw new JsInfoException("Property '" + pName + "' does not exist!");
		}
		return mProperties.get(pName);
	}
	
	public JsFunctionInfo getMethod(String pName) throws JsInfoException {
		if (!mMethods.containsKey(pName)) {
			throw new JsInfoException("Method '" + pName + "' does not exist!");
		}
		return mMethods.get(pName);
	}
	
	/**
	 * Returns the 'headless code block' for this JavaScript type.<br>
	 * <br>
	 * This is a workaround so that the main {@link JsFile} can create a code block
	 * that can hold the global JavaScript code.
	 * 
	 * @return The 'headless code block' for this type.
	 */
	public JsCodeBlockInfo getHeadlessCodeBlock() {
		if (mHeadlessCodeBlock == null) {
			return new JsCodeBlockInfo(this, mProperties, new HashMap<String, JsVariableInfo>());
		}
		return mHeadlessCodeBlock;
	}
	
	protected void setEditable(boolean pEditable) {
		mEditable = pEditable;
	}
	
	private void checkEditable() {
		if (!mEditable) {
			throw new IllegalStateException("Type " + getName() + " is not editable!");
		}
	}
}
