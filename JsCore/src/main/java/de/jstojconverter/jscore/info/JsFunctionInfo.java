package de.jstojconverter.jscore.info;

import java.util.Map;

import de.jstojconverter.jscore.util.JsParameter;

/**
 * @author Felix Jordan
 * @since 11.06.2015 - 21:42:38
 * @version 1.0
 */
public class JsFunctionInfo extends JsCodeBlockInfo {
	/*
	 * The JsFunction class must have a list of all possible return types. In fact only one value
	 * can be returned but in JavaScript this value can have different types at different times.
	 * To make the converting easier all possible return types are stored during the parsing of the
	 * JavaScript code.
	 */
	private String mName;
	private String[] mReturnTypes;
	private JsParameter[] mParameters;
	
	protected JsFunctionInfo(JsTypeInfo pParentType, Map<String, JsVariableInfo> pGlobalVariables,
			Map<String, JsVariableInfo> pParentVariables, String pName, String[] pReturnTypes, JsParameter... pParameters) {
		super(pParentType, pGlobalVariables, pParentVariables);
		mName = pName;
		mReturnTypes = pReturnTypes;
		mParameters = pParameters;
		try {
			for (JsParameter parameter : mParameters) {
				createVariable(parameter.getName(), parameter.getType());
			}
		} catch (JsInfoException e) {
			throw new RuntimeException("Internal error. Failed to create method parameters!", e);
		}
	}
	
	public String getName() {
		return mName;
	}
	
	public String[] getReturnTypes() {
		return mReturnTypes;
	}
	
	public JsParameter[] getParameters() {
		return mParameters;
	}
}
