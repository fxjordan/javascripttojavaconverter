package de.jstojconverter.jscore;

import de.jstojconverter.jscore.value.JsFunctionInvocation;

/**
 * @author Felix Jordan
 * @since 23.05.2015 - 20:35:07
 * @version 1.0
 */
public class JsFunctionInvocationStatement extends JsContent {
	
	private JsFunctionInvocation mFunctionInvocation;
	
	public JsFunctionInvocationStatement(JsFunctionInvocation pFunctionInvocation) {
		mFunctionInvocation = pFunctionInvocation;
	}
	
	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(mFunctionInvocation.getValue());
		srcBuilder.append(";\n");
		return srcBuilder.toString();
	}
}
