package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.operators.JsAssignmentOperator;

/**
 * @author Felix Jordan
 * @since 13.06.2015 - 23:30:10
 * @version 1.0
 */
public class JsAssignment extends JsCompositeValue {
	
	/*
	 * TODO This class is absolutley unnecessary, becuase it can be completly replaced by
	 *     the JSComposite class. This class only specifies the parameters that are necessary
	 *     to create an assignment.  
	 */
	
	public JsAssignment(JsVariable pVariable, JsAssignmentOperator pOperator, IJsValue pValue, boolean pBrackets)
			throws JsCoreException {
		super(pVariable, pValue, pOperator, pBrackets);
	}
	
	public JsVariable getVariable() {
		return (JsVariable) getValueA();
	}
	
	@Override
	public JsAssignmentOperator getOperator() {
		return (JsAssignmentOperator) super.getOperator();
	}
}
