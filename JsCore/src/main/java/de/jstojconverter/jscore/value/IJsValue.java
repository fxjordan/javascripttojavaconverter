package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCoreException;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:04:30
 * @version 1.0
 */
public interface IJsValue {
	
	public static final String TYPE_BOOLEAN = "boolean";
	public static final String TYPE_NUMBER = "number";
	public static final String TYPE_STRING = "string";
	public static final String TYPE_OBJECT = "object";
	public static final String TYPE_FUNCTION = "function";
	public static final String TYPE_UNDEFINED = "undefined";
	
	/**
	 * The type 'array' does not exist in JavaScript.
	 * In the parser it is used to better seperate between
	 * objects and arrays so that the converter must not
	 * control again if the objects are just obejcts or arrays.
	 * For more performence the JavaScript arrays will be
	 * native implemented in java.
	 */
	public static final String TYPE_ARRAY = "array";
	
	public String getValue();
	
	public String getType() throws JsCoreException;
	
	public boolean isVariable();
	
	public boolean isComposite();
}
