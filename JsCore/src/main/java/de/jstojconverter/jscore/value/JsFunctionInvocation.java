package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.info.JsFunctionInfo;
import de.jstojconverter.jscore.util.JsParameter;

/**
 * This value represents a JavaScript function invocaton.<br>
 * The function is invoked with a number of parameters and has a
 * target on which it is invoked. Also the function invocation must
 * have a type as it is a {@link IJsValue}. The type of this value
 * is the retun type of the function. This class keeps an array of
 * retun types, because JavaScript functions can return different types
 * in different situations. The parser must analyze the next code lines
 * to find out which return type is expected and then handle this value with
 * the
 * 
 * @author Felix Jordan
 * @since 24.05.2015 - 15:08:51
 * @version 1.0
 */
public class JsFunctionInvocation extends JsAbstractValue {
	
	private static final int RETURN_TYPE_OBJECT = -1;
	
	private IJsValue mTarget;
	private JsFunctionInfo mFunctionInfo;
	private int mReturnType;
	
	public JsFunctionInvocation(IJsValue pTarget, JsFunctionInfo pFunctionInfo) {
		mTarget = pTarget;
		mFunctionInfo = pFunctionInfo;
		if (mFunctionInfo.getReturnTypes().length == 1) {
			mReturnType = 0;
		} else {
			mReturnType = RETURN_TYPE_OBJECT;
		}
	}
	
	@Override
	public String getValue() {
		StringBuilder valueBuilder = new StringBuilder();
		valueBuilder.append(mTarget);
		valueBuilder.append(".");
		valueBuilder.append(mFunctionInfo.getName());
		valueBuilder.append("(");
		JsParameter[] parameters = mFunctionInfo.getParameters();
		for (int i=0; i<parameters.length; i++) {
			valueBuilder.append(parameters[i].getName());
			if (i < parameters.length - 1) {
				valueBuilder.append(", ");
			}
		}
		valueBuilder.append(")");
		return valueBuilder.toString();
	}
	
	@Override
	public String getType() throws JsCoreException {
		if (mReturnType == RETURN_TYPE_OBJECT) {
			return "object";
		}
		return mFunctionInfo.getReturnTypes()[mReturnType];
	}
	
	public String[] getPossibleReturnTypes() {
		return mFunctionInfo.getReturnTypes();
	}
	
	/**
	 * Sets the type of this function invocation. A function can have
	 * more than one possible return type, so the parser must set
	 * the right type later in the parsing process.
	 * 
	 * @param pType The return type of the function invocation.
	 * 
	 * @throws IllegalArgumentException If the return type is not possible
	 *     for this method.
	 */
	public void setReturnType(String pType) {
		if (pType.equals("object")) {
			mReturnType = RETURN_TYPE_OBJECT;
		}
		String[] returnTypes = mFunctionInfo.getReturnTypes();
		for (int i=0; i<returnTypes.length; i++) {
			if (returnTypes[i].equals(pType)) {
				mReturnType = i;
			}
		}
		throw new IllegalArgumentException("The return type '" + pType + "' is not possible for this function!");
	}
}
