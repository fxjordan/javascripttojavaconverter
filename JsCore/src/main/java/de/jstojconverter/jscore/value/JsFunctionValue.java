package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCodeBlock;
import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.util.JsParameter;

/**
 * @author Felix Jordan
 * @since 04.08.2015 - 00:12:43
 * @version 1.0
 */
public class JsFunctionValue extends JsCodeBlock implements IJsValue {
	
	private String[] mReturnTypes;
	private JsParameter[] mParameters;
	
	public JsFunctionValue(JsCodeBlockInfo pCodeBlockInfo, String[] pReturnTypes, JsParameter... pParameters) {
		super(pCodeBlockInfo);
		mReturnTypes = pReturnTypes;
		mParameters = pParameters;
	}
	
	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append("function (");
		for (int i=0; i<mParameters.length; i++) {
			beginBuilder.append(mParameters[i].getName());
			if (i < mParameters.length - 1) {
				beginBuilder.append(", ");
			}
		}
		return beginBuilder.toString();
	}

	@Override
	public String getValue() {
		return getSourceCode();
	}

	@Override
	public String getType() throws JsCoreException {
		return IJsValue.TYPE_FUNCTION;
	}

	@Override
	public boolean isVariable() {
		return false;
	}

	@Override
	public boolean isComposite() {
		return false;
	}
}
