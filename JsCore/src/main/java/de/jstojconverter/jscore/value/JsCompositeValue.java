package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.operators.IJsOperator;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 21:32:14
 * @version 1.0
 */
public class JsCompositeValue extends JsAbstractValue {
	
	private IJsValue mValueA;
	private IJsValue mValueB;
	private IJsOperator mOperator;
	private boolean mBrackets;
	
	public JsCompositeValue(IJsValue pValueA, IJsValue pValueB, IJsOperator pOperator, boolean pBrackets) throws JsCoreException {
		// Invoke method, because the operator throws automatically if types are not valid
		pOperator.getResultTypeFor(pValueA, pValueB);
		mValueA = pValueA;
		mValueB = pValueB;
		mOperator = pOperator;
		mBrackets = pBrackets;
	}
	
	@Override
	public String getValue() {
		StringBuilder valueBuilder = new StringBuilder();
		if (mBrackets) {
			valueBuilder.append("(");
		}
		valueBuilder.append(mValueA.getValue());
		valueBuilder.append(" ");
		valueBuilder.append(mOperator.toString());
		valueBuilder.append(" ");
		valueBuilder.append(mValueB.getValue());
		if (mBrackets) {
			valueBuilder.append(")");
		}
		return valueBuilder.toString();
	}
	
	public IJsValue getValueA() {
		return mValueA;
	}
	
	public IJsValue getValueB() {
		return mValueB;
	}
	
	public IJsOperator getOperator() {
		return mOperator;
	}
	
	public boolean hasBrackets() {
		return mBrackets;
	}
	
	@Override
	public String getType() throws JsCoreException {
		return mOperator.getResultTypeFor(mValueA, mValueB);
	}
	
	@Override
	public boolean isComposite() {
		return true;
	}
}
