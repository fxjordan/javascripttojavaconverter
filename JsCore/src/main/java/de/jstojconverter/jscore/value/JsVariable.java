package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.info.JsVariableInfo;

/**
 * @author Felix Jordan
 * @since 14.06.2015 - 00:15:18
 * @version 1.0
 */
public class JsVariable extends JsAbstractValue {
	
	private JsVariableInfo mVariableInfo;
	
	public JsVariable(JsVariableInfo pVariableInfo) {
		mVariableInfo = pVariableInfo;
	}
	
	@Override
	public String getValue() {
		return mVariableInfo.getName();
	}

	@Override
	public String getType() throws JsCoreException {
		return mVariableInfo.getType();
	}
	
	@Override
	public boolean isVariable() {
		return true;
	}
	
	public String getName() {
		return mVariableInfo.getName();
	}
}
