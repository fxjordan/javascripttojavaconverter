package de.jstojconverter.jscore.value;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 17:56:31
 * @version 1.0
 */
public class JsConstantValue extends JsAbstractValue {
	
	private final String mValue;
	private final String mType;
	
	public JsConstantValue(String pValue, String pType) {
		mValue = pValue;
		mType = pType;
	}
	
	@Override
	public String getValue() {
		return mValue;
	}
	
	@Override
	public String getType() {
		return mType;
	}
}
