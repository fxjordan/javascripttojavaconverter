package de.jstojconverter.jscore.value;

import de.jstojconverter.jscore.JsCoreException;

/**
 * @author Felix Jordan
 * @since 23.05.2015 - 16:57:18
 * @version 1.0
 */
public abstract class JsAbstractValue implements IJsValue {
	
	@Override
	public String toString() {
		return getValue();
	}
	
	@Override
	public boolean isVariable() {
		return false;
	}
	
	@Override
	public boolean isComposite() {
		return false;
	}
	
	@Override
	public abstract String getValue();
	
	@Override
	public abstract String getType() throws JsCoreException;
}
