package de.jstojconverter.jscore.value;

/**
 * @author Felix Jordan
 * @since 23.05.2015 - 16:06:42
 * @version 1.0
 */
public class JsArrayValue extends JsAbstractValue {
	
	private IJsValue[] mElements;
	
	public JsArrayValue(IJsValue... pElements) {
		mElements = pElements;
	}
	
	public IJsValue getElement(int pIndex) {
		return mElements[pIndex];
	}
	
	public int getLength() {
		return mElements.length;
	}
	
	@Override
	public String getValue() {
		StringBuilder valueBuilder = new StringBuilder();
		valueBuilder.append("[");
		for (int i=0; i<mElements.length; i++) {
			valueBuilder.append(mElements[i].getValue());
			if (i < mElements.length - 1) {
				valueBuilder.append(", ");
			}
		}
		valueBuilder.append("]");
		return valueBuilder.toString();
	}

	@Override
	public String getType() {
		return TYPE_ARRAY;
	}
}
