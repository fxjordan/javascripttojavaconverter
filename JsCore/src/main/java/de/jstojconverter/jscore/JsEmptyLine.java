package de.jstojconverter.jscore;

/**
 * @author Felix Jordan
 * @since 21.07.2015 - 23:18:19
 * @version 1.0
 */
public class JsEmptyLine extends JsContent {
	
	protected JsEmptyLine() {
	}
	
	@Override
	public String getSourceCode() {
		return "\n";
	}
}
