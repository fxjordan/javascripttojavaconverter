package de.jstojconverter.jscore;

import de.jstojconverter.jscore.info.JsFunctionInfo;
import de.jstojconverter.jscore.util.JsParameter;

/**
 * @author Felix Jordan
 * @since 21.07.2015 - 21:29:57
 * @version 1.0
 */
public class JsFunction extends JsCodeBlock {
	
	private JsFunctionInfo mInfo;
	
	protected JsFunction(JsFunctionInfo pFunctionInfo) {
		super(pFunctionInfo);
		mInfo = pFunctionInfo;
	}

	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append("function ");
		beginBuilder.append(mInfo.getName());
		beginBuilder.append("(");
		
		JsParameter[] parameters = mInfo.getParameters();
		for (int i=0; i<parameters.length; i++) {
			JsParameter parameter = parameters[i];
			beginBuilder.append(parameter.getType());
			beginBuilder.append(" ");
			beginBuilder.append(parameter.getName());
			if (i < parameters.length - 1) {
				beginBuilder.append(",");
			}
		}
		beginBuilder.append(")");
		return beginBuilder.toString();
	}
}
