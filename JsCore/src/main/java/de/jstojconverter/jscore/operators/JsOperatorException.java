package de.jstojconverter.jscore.operators;

import de.jstojconverter.jscore.JsCoreException;

/**
 * @author Felix Jordan
 * @since 13.06.2015 - 23:14:45
 * @version 1.0
 */
public class JsOperatorException extends JsCoreException {
	
	private static final long serialVersionUID = 8470147667994968908L;
	
	public JsOperatorException() {
	}

	public JsOperatorException(String pMessage) {
		super(pMessage);
	}

	public JsOperatorException(Throwable pThrowable) {
		super(pThrowable);
	}

	public JsOperatorException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
