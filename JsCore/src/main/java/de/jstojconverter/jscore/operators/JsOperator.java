package de.jstojconverter.jscore.operators;

/**
 * This class is a temporary container for the the static fields and methods of the
 * {@link IJsOperator} interface. For compatibility to Java 1.7 these methods moved
 * to this class because the compiler does not allow static fields and methods inside
 * interfaces.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 02:17:12
 * @version 1.0
 */
public class JsOperator {
	
	private JsOperator() {
	}
	
	/** Use this constant instead of {@link #stringValues()}. */
	public static final String[] STRING_VALUES = stringValues();

	/**
	 * Returns an array with all JavaScript operators that exist. The array is recalculated
	 * every time this method is called, so it's recommended to use the {@link #STRING_VALUES}
	 * constant instead. There the value is cached and only caluculated once.
	 * 
	 * @return A string array with all operators that exist.
	 */
	public static String[] stringValues() {
		String[] assignemntValues = JsAssignmentOperator.stringValues();
		String[] mathValues = JsMathOperator.stringValues();
		String[] compariosonValues = JsComparisonOperator.stringValues();
		int length = assignemntValues.length + mathValues.length + compariosonValues.length;
		String[] stringValues = new String[length];
		for (int i=0; i<assignemntValues.length; i++) {
			stringValues[i] = assignemntValues[i];
		}
		for (int i=0; i<mathValues.length; i++) {
			stringValues[assignemntValues.length + i] = mathValues[i];
		}
		for (int i=0; i<compariosonValues.length; i++) {
			stringValues[assignemntValues.length + mathValues.length + i] = compariosonValues[i];
		}

		// Sort by length
		int begin = 0;
		int end = stringValues.length - 1;
		int best = begin;
		String cache;
		
		while (begin != end) {
			for (int pointer=begin+1; pointer<end+1; pointer++) {
				if (stringValues[pointer].length() > stringValues[best].length()) {
					best = pointer;
				}
			}
			cache = stringValues[begin];
			stringValues[begin] = stringValues[best];
			stringValues[best] = cache;
			
			begin++;
			best = begin;
		}
		return stringValues;
	}
	
	public static IJsOperator getOperator(String pOperator) throws JsOperatorException {
		if (JsAssignmentOperator.isOperator(pOperator)) {
			return JsAssignmentOperator.getOperator(pOperator);
		} else if (JsMathOperator.isOperator(pOperator)) {
			return JsMathOperator.getOperator(pOperator);
		} else if(JsComparisonOperator.isOperator(pOperator)) {
			return JsComparisonOperator.getOperator(pOperator);
		}
		throw new IllegalArgumentException("No operator with value '" + pOperator + "' exist!");
	}
}
