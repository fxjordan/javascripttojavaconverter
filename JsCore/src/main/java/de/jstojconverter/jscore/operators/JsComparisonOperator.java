package de.jstojconverter.jscore.operators;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * JavaScript comparison operators are defiend for all different data types.
 * 
 * @author Felix Jordan
 * @since 22.07.2015 - 01:45:25
 * @version 1.0
 */
public enum JsComparisonOperator implements IJsOperator {
	
	EQUAL("=="),
	EQUAL_VALUE_TYPE("==="),
	NOT_EQUAL("!="),
	NOT_EQUAL_VALUE_TYPE("!==="),
	GREATER(">"),
	LESS("<"),
	GREATER_OR_EQUAL(">="),
	LESS_OR_EQUAL("<=");
	
	private String mOperator;
	
	private JsComparisonOperator(String pOperator) {
		mOperator = pOperator;
	}
	
	@Override
	public String toString() {
		return mOperator;
	}

	public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		return "boolean";
	}
	
	@Override
	public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		return true;
	}
	
	@Override
	public boolean isAssignmentOperator() {
		return false;
	}
	
	@Override
	public boolean isMathOperator() {
		return false;
	}
	
	@Override
	public boolean isComparisonOperator() {
		return true;
	}
	
	public static boolean isOperator(String pString) {
		for (JsComparisonOperator operator : values()) {
			if (operator.toString().equals(pString)) {
				return true;
			}
		}
		return false;
	}
	
	public static JsComparisonOperator getOperator(String pOperator) {
		for (JsComparisonOperator operator : values()) {
			if (operator.toString().equals(pOperator)) {
				return operator;
			}
		}
		throw new IllegalArgumentException("No assignment operator with value '" + pOperator + "' exists!");
	}
	
	public static String[] stringValues() {
		JsComparisonOperator[] values = values();
		String[] stringValues = new String[values.length];
		for (int i=0; i<values.length; i++) {
			stringValues[i] = values[i].toString();
		}
		return stringValues;
	}
}
