package de.jstojconverter.jscore.operators;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:18:12
 * @version 1.0
 */
public interface IJsOperator {
	
	public String toString();
	
	/**
	 * Returns the type of the result value from two {@link IJavaValue}s combined with this
	 * operator. If the operator is an assignment operator the type of the variable has to
	 * be changed to the result type from this method, because in JavaScript variable types
	 * are mutable.
	 * 
	 * @param pValueA
	 * @param pValueB
	 * @return
	 * @throws JsCoreException
	 */
	public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException;
	
	public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException;
	
	public boolean isAssignmentOperator();
	
	public boolean isMathOperator();
	
	public boolean isComparisonOperator();
	
//	These static fields and methods are moved temporary to the new class 'JsOperators' for
//	compatibility with the java compiler 1.7, where no static fields and methods inside
//	interfaces are allowed.
//	
//	/** Use this constant instead of {@link #stringValues()}. */
//	public static final String[] STRING_VALUES = stringValues();
//
//	/**
//	 * Returns an array with all JavaScript operators that exist. The array is recalculated
//	 * every time this method is called, so it's recommended to use the {@link #STRING_VALUES}
//	 * constant instead. There the value is cached and only caluculated once.
//	 * 
//	 * @return A string array with all operators that exist.
//	 */
//	public static String[] stringValues() {
//		String[] assignemntValues = JsAssignmentOperator.stringValues();
//		String[] mathValues = JsMathOperator.stringValues();
//		String[] compariosonValues = JsComparisonOperator.stringValues();
//		int length = assignemntValues.length + mathValues.length + compariosonValues.length;
//		String[] stringValues = new String[length];
//		for (int i=0; i<assignemntValues.length; i++) {
//			stringValues[i] = assignemntValues[i];
//		}
//		for (int i=0; i<mathValues.length; i++) {
//			stringValues[assignemntValues.length + i] = mathValues[i];
//		}
//		for (int i=0; i<compariosonValues.length; i++) {
//			stringValues[assignemntValues.length + mathValues.length + i] = compariosonValues[i];
//		}
//
//		// Sort by length
//		int begin = 0;
//		int end = stringValues.length - 1;
//		int best = begin;
//		String cache;
//		
//		while (begin != end) {
//			for (int pointer=begin+1; pointer<end+1; pointer++) {
//				if (stringValues[pointer].length() > stringValues[best].length()) {
//					best = pointer;
//				}
//			}
//			cache = stringValues[begin];
//			stringValues[begin] = stringValues[best];
//			stringValues[best] = cache;
//			
//			begin++;
//			best = begin;
//		}
//		return stringValues;
//	}
//	
//	public static IJsOperator getOperator(String pOperator) throws JsOperatorException {
//		if (JsAssignmentOperator.isOperator(pOperator)) {
//			return JsAssignmentOperator.getOperator(pOperator);
//		} else if (JsMathOperator.isOperator(pOperator)) {
//			return JsMathOperator.getOperator(pOperator);
//		} else if(JsComparisonOperator.isOperator(pOperator)) {
//			return JsComparisonOperator.getOperator(pOperator);
//		}
//		throw new IllegalArgumentException("No operator with value '" + pOperator + "' exist!");
//	}
}
