package de.jstojconverter.jscore.operators;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 20:33:48
 * @version 1.0
 */
public enum JsMathOperator implements IJsOperator {
	
	ADD("+") {
		
		@Override
		public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
			if (pValueA.getType().equals(IJsValue.TYPE_STRING) || pValueB.getType().equals(IJsValue.TYPE_STRING)) {
				return IJsValue.TYPE_STRING;
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
			if (pValueA.getType().equals(IJsValue.TYPE_STRING) || pValueB.getType().equals(IJsValue.TYPE_STRING)) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	
	SUB("-"),
	MUL("*"),
	DIV("/");
	
	private String mOperator;
	
	private JsMathOperator(String pOperator) {
		mOperator = pOperator;
	}
	
	@Override
	public String toString() {
		return mOperator;
	}

	@Override
	public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		checkValid(pValueA, pValueB);
		return IJsValue.TYPE_NUMBER;
	}

	@Override
	public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		return pValueA.getType().equals(IJsValue.TYPE_NUMBER) && pValueB.getType().equals(IJsValue.TYPE_NUMBER);
	}
	
	private void checkValid(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		if (!isValidFor(pValueA, pValueB)) {
			throw new JsCoreException("The operator " + mOperator + " is undefined for the argument type(s) "
					+ pValueA.getType() + ", " + pValueB.getType());
		}
	}

	@Override
	public boolean isAssignmentOperator() {
		return false;
	}

	@Override
	public boolean isMathOperator() {
		return true;
	}

	@Override
	public boolean isComparisonOperator() {
		return false;
	}
	
	public static boolean isOperator(String pString) {
		for (JsMathOperator operator : values()) {
			if (operator.toString().equals(pString)) {
				return true;
			}
		}
		return false;
	}
	
	public static JsMathOperator getOperator(String pOperator) {
		for (JsMathOperator operator : values()) {
			if (operator.toString().equals(pOperator)) {
				return operator;
			}
		}
		throw new IllegalArgumentException("No math operator with value '" + pOperator + "' exists!");
	}
	
	public static String[] stringValues() {
		JsMathOperator[] values = values();
		String[] stringValues = new String[values.length];
		for (int i=0; i<values.length; i++) {
			stringValues[i] = values[i].toString();
		}
		return stringValues;
	}
}
