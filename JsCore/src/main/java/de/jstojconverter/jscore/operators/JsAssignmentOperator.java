package de.jstojconverter.jscore.operators;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:24:11
 * @version 1.0
 */
public enum JsAssignmentOperator implements IJsOperator {
	
	NORMAL("=") {
		@Override
		public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
			return pValueB.getType();
		}
		
		/**
		 * Returns {@code true} in all cases because in JavaScript variables can be changed to any type.
		 */
		@Override
		public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
			return true;
		}
	},
	ADD("+=") {
		@Override
		public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
			if (pValueA.getType().equals(IJsValue.TYPE_STRING) || pValueB.getType().equals(IJsValue.TYPE_STRING)) {
				return IJsValue.TYPE_STRING;
			}
			return super.getResultTypeFor(pValueA, pValueB);
		}
		
		@Override
		public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
			if (pValueA.getType().equals(IJsValue.TYPE_STRING) || pValueB.getType().equals(IJsValue.TYPE_STRING)) {
				return true;
			}
			return super.isValidFor(pValueA, pValueB);
		}
	},
	SUB("-="),
	MUL("*="),
	DIV("/=");
	
	private String mOperator;
	
	private JsAssignmentOperator(String pOperator) {
		mOperator = pOperator;
	}
	
	@Override
	public String toString() {
		return mOperator;
	}
	
	@Override
	public String getResultTypeFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		checkValid(pValueA, pValueB);
		return pValueA.getType();
	}

	@Override
	public boolean isValidFor(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		return pValueA.getType().equals(pValueB.getType()) && pValueA.isVariable();
	}
	
	private void checkValid(IJsValue pValueA, IJsValue pValueB) throws JsCoreException {
		if (!pValueA.isVariable()) {
			throw new JsOperatorException("The left hand side of an assignment must be a variable! " + pValueA.getValue() + " "
					+ mOperator + " " + pValueB.getValue());
		}
		if (!isValidFor(pValueA, pValueB)) {
			throw new JsOperatorException("The operator " + mOperator + " is undefined for the argument type(s) "
					+ pValueA.getType() + ", " + pValueB.getType());
		}
	}

	@Override
	public boolean isAssignmentOperator() {
		return true;
	}

	@Override
	public boolean isMathOperator() {
		return false;
	}

	@Override
	public boolean isComparisonOperator() {
		return false;
	}
	
	public static boolean isOperator(String pString) {
		for (JsAssignmentOperator operator : values()) {
			if (operator.toString().equals(pString)) {
				return true;
			}
		}
		return false;
	}
	
	public static JsAssignmentOperator getOperator(String pOperator) {
		for (JsAssignmentOperator operator : values()) {
			if (operator.toString().equals(pOperator)) {
				return operator;
			}
		}
		throw new IllegalArgumentException("No assignment operator with value '" + pOperator + "' exists!");
	}
	
	public static String[] stringValues() {
		JsAssignmentOperator[] values = values();
		String[] stringValues = new String[values.length];
		for (int i=0; i<values.length; i++) {
			stringValues[i] = values[i].toString();
		}
		return stringValues;
	}
}
