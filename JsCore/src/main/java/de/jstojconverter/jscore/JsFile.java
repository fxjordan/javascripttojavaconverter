package de.jstojconverter.jscore;

import java.util.UUID;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.info.JsInfoException;
import de.jstojconverter.jscore.info.JsTypeInfo;
import de.jstojconverter.jscore.info.JsTypeManager;

/**
 * @author Felix Jordan
 * @since 21.07.2015 - 20:51:51
 * @version 1.0
 */
public class JsFile extends JsCodeBlock {
	
	/** Internal type name for the context of this {@link JsContent}. */
	private static final String INTERNAL_TYPE_NAME = "GLOBAL_FILE_SCOPE_TYPE";
	
	private JsFile(JsCodeBlockInfo pCodeBlockInfo) {
		super(pCodeBlockInfo);
	}
	
	public static JsFile create() {
		try {
			JsTypeInfo typeInfo = JsTypeManager.getManager().createType(INTERNAL_TYPE_NAME + UUID.randomUUID());
			return new JsFile(typeInfo.getHeadlessCodeBlock());
		} catch (JsInfoException e) {
			throw new RuntimeException("Internal error: Could not create internal type for JsFile.", e);
		}
	}
	
	@Override
	public String getSourceCode() {
		String original = super.getSourceCode();
		String replaced = original.replace("\n\t", "\n");
		return replaced.substring(3, replaced.length() - 2);
	}
	
	@Override
	protected String getBlockBegin() {
		return "";
	}
}
