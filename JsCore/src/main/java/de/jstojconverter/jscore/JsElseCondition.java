package de.jstojconverter.jscore;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;

/**
 * @author Felix Jordan
 * @since 22.07.2015 - 00:50:16
 * @version 1.0
 */
public class JsElseCondition extends JsCodeBlock {

	protected JsElseCondition(JsCodeBlockInfo pCodeBlockInfo) {
		super(pCodeBlockInfo);
	}

	@Override
	protected String getBlockBegin() {
		return "else";
	}

}
