package de.jstojconverter.jscore;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * @author Felix Jordan
 * @since 22.07.2015 - 00:52:36
 * @version 1.0
 */
public class JsElseIfCondition extends JsCodeBlock {
	
	private IJsValue mCondition;
	
	protected JsElseIfCondition(JsCodeBlockInfo pCodeBlockInfo, IJsValue pCondition) {
		super(pCodeBlockInfo);
		mCondition = pCondition;
	}
	
	public IJsValue getCondition() {
		return mCondition;
	}
	
	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append("else if (");
		beginBuilder.append(mCondition.getValue());
		beginBuilder.append(")");
		return beginBuilder.toString();
	}

}
