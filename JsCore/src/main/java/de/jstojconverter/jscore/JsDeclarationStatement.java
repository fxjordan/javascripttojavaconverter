package de.jstojconverter.jscore;

import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsVariable;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 17:21:41
 * @version 1.0
 */
public class JsDeclarationStatement extends JsContent {
	
	private JsVariable mVariable;
	private IJsValue mValue;
	
	protected JsDeclarationStatement(JsVariable pVariable, IJsValue pValue) {
		mVariable = pVariable;
		mValue = pValue;
	}
	
	public JsDeclarationStatement(JsVariable pVariable) {
		mVariable = pVariable;
	}
	
	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append("var ");
		srcBuilder.append(mVariable.getValue());
		if (mValue != null) {
			srcBuilder.append(" = ");
			srcBuilder.append(mValue.getValue());
		}
		srcBuilder.append(";\n");
		return srcBuilder.toString();
	}
	
	public JsVariable getVariable() {
		return mVariable;
	}
	
	public boolean hasValue() {
		return mValue != null;
	}
	
	public IJsValue getValue() {
		return mValue;
	}
}
