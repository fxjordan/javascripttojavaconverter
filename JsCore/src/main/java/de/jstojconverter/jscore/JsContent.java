package de.jstojconverter.jscore;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:02:13
 * @version 1.0
 */
public abstract class JsContent {
	
	private JsContent mParent;
	
	@Override
	public String toString() {
		return getSourceCode();
	}
	
	protected void attachChild(JsContent pContent) {
		pContent.mParent = this;
	}
	
	public boolean hasParent() {
		return mParent != null;
	}
	
	public JsContent getParent() {
		return mParent;
	}
	
	public boolean isContainer() {
		return true;
	}
	
	public abstract String getSourceCode();
}
