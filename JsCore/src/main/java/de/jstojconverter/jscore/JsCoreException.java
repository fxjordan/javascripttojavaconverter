package de.jstojconverter.jscore;

/**
 * @author Felix Jordan
 * @since 13.06.2015 - 23:13:08
 * @version 1.0
 */
public class JsCoreException extends Exception {
	
	private static final long serialVersionUID = -4709914464194218741L;
	
	public JsCoreException() {
	}

	public JsCoreException(String pMessage) {
		super(pMessage);
	}

	public JsCoreException(Throwable pThrowable) {
		super(pThrowable);
	}

	public JsCoreException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
