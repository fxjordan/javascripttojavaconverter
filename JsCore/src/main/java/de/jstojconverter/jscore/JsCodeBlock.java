package de.jstojconverter.jscore;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.info.JsFunctionInfo;
import de.jstojconverter.jscore.info.JsInfoException;
import de.jstojconverter.jscore.info.JsTypeManager;
import de.jstojconverter.jscore.info.JsVariableInfo;
import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.jscore.util.JsParameter;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsAssignment;
import de.jstojconverter.jscore.value.JsFunctionInvocation;
import de.jstojconverter.jscore.value.JsVariable;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:08:41
 * @version 1.0
 */
public abstract class JsCodeBlock extends JsContent {
	
	private JsCodeBlockInfo mInfo;
	private List<JsContent> mContent;
	
	protected JsCodeBlock(JsCodeBlockInfo pCodeBlockInfo) {
		mInfo = pCodeBlockInfo;
		mContent = new ArrayList<JsContent>();
	}
	
	public JsCodeBlockInfo getInfo() {
		return mInfo;
	}
	
	public JsFunction createFunction(String pName, String[] pReturnTypes, JsParameter[] pParameters) throws JsInfoException {
		JsFunctionInfo functionInfo = mInfo.createFunction(pName, pReturnTypes, pParameters);
		JsFunction function = new JsFunction(functionInfo);
		super.attachChild(function);
		mContent.add(function);
		return function;
	}
	
	public JsEmptyLine createEmptyLine() {
		JsEmptyLine emptyLine = new JsEmptyLine();
		super.attachChild(emptyLine);
		mContent.add(emptyLine);
		return emptyLine;
	}
	
	public JsDeclarationStatement createDeclarationStatement(String pName) throws JsInfoException {
		JsVariableInfo variable = mInfo.createVariable(pName, IJsValue.TYPE_UNDEFINED);
		JsDeclarationStatement statement = new JsDeclarationStatement(new JsVariable(variable));
		super.attachChild(statement);
		mContent.add(statement);
		return statement;
	}
	
	public JsDeclarationStatement createDeclarationStatement(String pName, IJsValue pValue) throws JsCoreException {
		JsVariableInfo variable = mInfo.createVariable(pName, pValue.getType());
		JsDeclarationStatement statement = new JsDeclarationStatement(new JsVariable(variable), pValue);
		super.attachChild(statement);
		mContent.add(statement);
		return statement;
	}
	
	public JsAssignmentStatement createAssignmentStatement(String pVariableName, JsAssignmentOperator pOperator,
			IJsValue pValue) throws JsCoreException {
		JsAssignment assignment = createAssignment(pVariableName, pOperator, pValue, false);
		JsAssignmentStatement statement = new JsAssignmentStatement(assignment);
		super.attachChild(statement);
		mContent.add(statement);
		return statement;
	}
	
	public JsAssignment createAssignment(String pVariableName, JsAssignmentOperator pOperator,
			IJsValue pValue, boolean pBrackets) throws JsCoreException {
		JsVariable variable = new JsVariable(mInfo.getVariable(pVariableName));
		return new JsAssignment(variable, pOperator, pValue, pBrackets);
	}
	
	public JsFunctionInvocation createFunctionInvocation(IJsValue pTarget, String pFunctionName) throws JsCoreException {
		JsFunctionInfo functionInfo = JsTypeManager.getManager().getType(pTarget.getType()).getMethod(pFunctionName);
		return new JsFunctionInvocation(pTarget, functionInfo);
	}
	
	public JsIfCondition createIf(IJsValue pCondition) throws JsCoreException {
		assertConditionValue(pCondition);
		JsIfCondition ifCondition = new JsIfCondition(mInfo, pCondition);
		super.attachChild(ifCondition);
		mContent.add(ifCondition);
		return ifCondition;
	}
	
	public JsElseCondition createElse() {
		JsElseCondition elseCondition = new JsElseCondition(mInfo);
		super.attachChild(elseCondition);
		mContent.add(elseCondition);
		return elseCondition;
	}
	
	public JsElseIfCondition createElseIf(IJsValue pCondition) throws JsCoreException {
		assertConditionValue(pCondition);
		JsElseIfCondition elseIfCondition = new JsElseIfCondition(mInfo, pCondition);
		super.attachChild(elseIfCondition);
		mContent.add(elseIfCondition);
		return elseIfCondition;
	}
	
	public JsWhileLoop createWhile(IJsValue pCondition) throws JsCoreException {
		assertConditionValue(pCondition);
		JsWhileLoop whileLoop = new JsWhileLoop(mInfo, pCondition);
		super.attachChild(whileLoop);
		mContent.add(whileLoop);
		return whileLoop;
	}
	
	public JsDoWhileLoop createDoWhile(IJsValue pCondition) throws JsCoreException {
		assertConditionValue(pCondition);
		JsDoWhileLoop doWhileLoop = new JsDoWhileLoop(mInfo, pCondition);
		super.attachChild(doWhileLoop);
		mContent.add(doWhileLoop);
		return doWhileLoop;
	}
	
	public boolean isEmpty() {
		return mContent.isEmpty();
	}
	
	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(getBlockBegin());
		srcBuilder.append(" {\n\t");
		
		for (int i=0; i<mContent.size(); i++) {
			JsContent content = mContent.get(i);
			String bodyText = content.getSourceCode().replace("\n", "\n\t");
			srcBuilder.append(bodyText);
		}
		srcBuilder.delete(srcBuilder.length()-2, srcBuilder.length());
		srcBuilder.append("\n}");
		String blockEnd = getBlockEnd();
		if (blockEnd != null) {
			srcBuilder.append(" ");
			srcBuilder.append(blockEnd);
		}
		srcBuilder.append("\n");
		return srcBuilder.toString();
	}
	
	protected abstract String getBlockBegin();
	
	protected String getBlockEnd() {
		return null;
	}
	
	private void assertConditionValue(IJsValue pCondition) throws JsCoreException {
		try {
			if (!pCondition.getType().equals(IJsValue.TYPE_BOOLEAN)) {
				throw new JsCoreException("Condition must be of type 'boolean'.");
			}
		} catch (JsCoreException e) {
			throw new JsCoreException("Exception while checking condition type.", e);
		}
	}
}