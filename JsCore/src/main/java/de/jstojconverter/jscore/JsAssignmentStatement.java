package de.jstojconverter.jscore;

import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.jscore.value.JsAssignment;
import de.jstojconverter.jscore.value.*;

/**
 * @author Felix Jordan
 * @since 23.05.2015 - 18:18:35
 * @version 1.0
 */
public class JsAssignmentStatement extends JsContent {
	
	private JsAssignment mAssignment;
	
	public JsAssignmentStatement(JsAssignment pAssignment) {
		mAssignment = pAssignment;
	}
	
	public String getVariableName() {
		return mAssignment.getValueA().getValue();
	}
	
	public JsAssignmentOperator getOperator() {
		return mAssignment.getOperator();
	}
	
	public IJsValue getValue(){
		return mAssignment.getValueB();
	}
	
	@Override
	public String getSourceCode() {
		StringBuilder srcBuilder = new StringBuilder();
		srcBuilder.append(mAssignment.getValue());
		srcBuilder.append(";\n");
		return srcBuilder.toString();
	}
}
