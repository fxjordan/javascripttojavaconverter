package de.jstojconverter.jscore.util;

/**
 * @author Felix Jordan
 * @since 21.07.2015 - 14:35:53
 * @version 1.0
 */
public class JsNameValidation {
	
	private JsNameValidation() {
	}
	
	public static boolean isVariableNameValid(String pName) {
		// TODO Check if name is valid and later log some warnings if the name is
		// valid but not good, like a variable name with upper cases at beginning etc. (FooVariable)
		return true;
	}
	
	public static boolean isTypeNameValid(String pName) {
		// TODO Check if name is valid and later log some warnings if the name is
		// valid but not good, like a variable name with upper cases at beginning etc. (FooVariable)
		return true;
	}

	public static boolean isMethodNameValid(String pName) {
		// TODO Check if name is valid and later log some warnings if the name is
		// valid but not good, like a variable name with upper cases at beginning etc. (FooVariable)
		return true;
	}
}
