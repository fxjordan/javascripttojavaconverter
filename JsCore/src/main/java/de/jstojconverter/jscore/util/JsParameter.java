package de.jstojconverter.jscore.util;

/**
 * @author Felix Jordan
 * @since 21.07.2015 - 14:34:52
 * @version 1.0
 */
public class JsParameter {
	
	private String mName;
	private String mType;
	
	public JsParameter(String pName, String pType) {
		mName = pName;
		mType = pType;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getType() {
		return mType;
	}
	
	public static String[] getTypeArray(JsParameter[] pParameters) {
		String[] types = new String[pParameters.length];
		for (int i=0; i<pParameters.length; i++) {
			types[i] = pParameters[i].getType();
		}
		return types;
	}
}
