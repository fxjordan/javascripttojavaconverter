package de.jstojconverter.jscore;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.value.IJsValue;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:03:46
 * @version 1.0
 */
public class JsIfCondition extends JsCodeBlock {
	
	private IJsValue mCondition;
	
	protected JsIfCondition(JsCodeBlockInfo pCodeBlockInfo, IJsValue pCondition) {
		super(pCodeBlockInfo);
		mCondition = pCondition;
	}
	
	public IJsValue getCondition() {
		return mCondition;
	}

	@Override
	protected String getBlockBegin() {
		StringBuilder beginBuilder = new StringBuilder();
		beginBuilder.append("if (");
		beginBuilder.append(mCondition.getValue());
		beginBuilder.append(")");
		return beginBuilder.toString();
	}
}
