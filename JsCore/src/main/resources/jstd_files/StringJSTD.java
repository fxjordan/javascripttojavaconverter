package jstd_files;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 03:48:26
 * @version 1.0
 */
public class StringJSTD {
	
	private static final String DATA = "TYPE:\n"
			+ "String;\n"
			+ "\n"
			+ "PROPERTIES:\n"
			+ "\n"
			+ "METHODS:\n"
			+ "[number] lastIndexOf(string);";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}
