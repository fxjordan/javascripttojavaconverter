package de.jstojconverter.jscore;

import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.jscore.operators.JsComparisonOperator;
import de.jstojconverter.jscore.util.JsParameter;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsCompositeValue;
import de.jstojconverter.jscore.value.JsConstantValue;
import de.jstojconverter.util.Log;

/**
 * @author Felix Jordan
 * @since 21.07.2015 - 21:00:13
 * @version 1.0
 */
public class JsFileTest {
	
	public static final String LOG_TAG = "JsFileTest";

	public static void main(String[] pArgs) {
		JsFile file = JsFile.create();
		
		try {
			JsDeclarationStatement numberVarDeclaration = file.createDeclarationStatement("numberVar",
					new JsConstantValue("42", IJsValue.TYPE_NUMBER));
			
			file.createDeclarationStatement("stringVar",
					new JsConstantValue("'foo'", IJsValue.TYPE_STRING));
			file.createDeclarationStatement("booleanVar",
					new JsConstantValue("true", IJsValue.TYPE_BOOLEAN));
			file.createDeclarationStatement("arrayVar",
					new JsConstantValue("['foo', 'boo', 42, 73]", IJsValue.TYPE_ARRAY));
			
			file.createEmptyLine();
			
			JsFunction fooFunction = file.createFunction("foo", new String[] {"number"}, new JsParameter[] {});
			fooFunction.createAssignmentStatement("numberVar", JsAssignmentOperator.ADD,
					new JsConstantValue("73", IJsValue.TYPE_NUMBER));
			JsFunction booFunction = file.createFunction("boo", new String[] {"number"}, new JsParameter[] {});
			JsIfCondition ifCondition = booFunction.createIf(new JsCompositeValue(numberVarDeclaration.getVariable(),
					new JsConstantValue("42", IJsValue.TYPE_NUMBER), JsComparisonOperator.EQUAL, false));
			ifCondition.createDoWhile(new JsCompositeValue(numberVarDeclaration.getVariable(),
					new JsConstantValue("73", IJsValue.TYPE_NUMBER), JsComparisonOperator.LESS_OR_EQUAL, false));
			Log.d(LOG_TAG, "JSFile created.");
		} catch (JsCoreException e) {
			e.printStackTrace();
		}
		
		Log.i(LOG_TAG, "Rendering file content...");
		Log.d(LOG_TAG, file.getSourceCode());
		Log.d(LOG_TAG, "EOF");
	}
}
