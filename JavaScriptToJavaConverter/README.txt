JavaScript to Java Converter:
=============================

This converter should be able to completely convert your JavaScript code into java code.
Every special JavaScript specification that is supported is listed here:

1. Dialogs:
    - window.alert() and alert() functions are converted to javax.swing.JOptionPane.showMessageDialog()
    
2. Console:
    - console.log() function is converted to System.out.println()
    
3. Variables
    - If a value is assigned to a non defined variable a global varibale is created.
    - If a value is declared without an assignment the type is set to NOT_DEFINED (not 'undefined' because it has different meaning)
      and later if a value is assigned the type is automatically changed. Can only happen once.