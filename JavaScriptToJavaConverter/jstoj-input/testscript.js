var maxValue = 100;
var minValue = 10;
foo();

var running = true;

var x = function(a, b) {return a * b;};

console.log(x(42, 73));

while (running) {
	if (!processInput()) {
		running = false;
	}
}

function processInput() {
	window.alert("Next you have to put a value higher than " + minValue + " and lower than " + maxValue + "!");
	
	var valueString = window.prompt("Please type in a number:");
	var value = parseInt(valueString);

	if (isNaN(value)) {
		var retry = window.confirm("Your value is not a number!  Do you want to try again?");
		if (retry) {
			return true;
		} else {
			window.alert("OK. Good bye!");
			return false;
		}
	}

	if (value < minValue) {
		var retry = window.confirm("Your value is too low. Do you want to try again?");
		if (retry) {
			return true;
		} else {
			window.alert("OK. Good bye!");
			return false;
			
		}
	} else if (value > maxValue) {
		var retry = window.confirm("Your value is too high. Do you want to try again?");
		if (retry) {
			return true;
		} else {
			window.alert("OK. Good bye!");
			return false;
		}
	} else {
		window.alert("Your value is correct. Good bye!");
		return false;
	}
}

function foo() {
	console.log("foo");
	
	if (declareOtherFunction) {
		function other() {
			console.log("other");
		}
	}
}