package de.jstojconverter;

import java.util.HashMap;

import de.jstojconverter.javacore.value.JavaConstantValue;
import junit.framework.TestCase;
import de.jstojconverter.javacore.*;
import de.jstojconverter.javacore.info.*;
import de.jstojconverter.javacore.value.*;

public class TypeDeclarationChangerTest extends TestCase {
	
	private JavaCodeBlock mCodeBlock;
	
	public TypeDeclarationChangerTest() {
		try {
			mCodeBlock = new JavaFile("de.foo", "Foo").getJavaClass()
				.createMethod("public", null, "foo", "void", null);
		} catch (JavaInfoException e) {
			throw new RuntimeException(e);
		}
	}
	
	public void testChangeStringNormal() {
		String jsStringValue = "\"foo\"";
		JavaConstantValue javaStringValue = TypeDeclarationChanger.changeStringDeclaration(jsStringValue);
		assertEquals("String", javaStringValue.getType());
		assertEquals("\"foo\"", javaStringValue.getValue());
	}
	
	public void testChangeMalformedString() {
		String jsStringValue = "'foo'";
		JavaConstantValue javaStringValue = TypeDeclarationChanger.changeStringDeclaration(jsStringValue);
		assertEquals("String", javaStringValue.getType());
		assertEquals("\"foo\"", javaStringValue.getValue());
	}
	
	public void testChangeStringArray() throws ConverterException, JavaCoreException {
		String jsStringArray = "[\"foo\", \"boo\", \"hello\"]";
		JavaConstantValue javaStringArray = TypeDeclarationChanger.changeArrayDeclaration(jsStringArray, mCodeBlock);
		assertEquals("String[]", javaStringArray.getType());
		assertEquals("new String[] {\"foo\", \"boo\", \"hello\"}", javaStringArray.getValue());
	}
	
	public void testChangeIntegerArray() throws ConverterException, JavaCoreException {
		String jsIntArray = "[42, 12, 120]";
		JavaConstantValue javaIntArray = TypeDeclarationChanger.changeArrayDeclaration(jsIntArray, mCodeBlock);
		assertEquals("int[]", javaIntArray.getType());
		assertEquals("new int[] {42, 12, 120}", javaIntArray.getValue());
	}
	
	public void testDiffernetTypeArray() throws ConverterException, JavaCoreException {
		String jsArray = "[\"foo\", 5, 42]";
		JavaConstantValue javaArray = TypeDeclarationChanger.changeArrayDeclaration(jsArray, mCodeBlock);
		assertEquals("Object[]", javaArray.getType());
		assertEquals("new Object[] {\"foo\", 5, 42}", javaArray.getValue());
	}
	
	public void testArraysInsideArray() throws ConverterException, JavaCoreException {
		String jsIntArray = "[[10], [-3], [5]]";
		JavaConstantValue javaIntArray = TypeDeclarationChanger.changeArrayDeclaration(jsIntArray, mCodeBlock);
		assertEquals("int[][]", javaIntArray.getType());
		assertEquals("new int[][] {new int[] {10}, new int[] {-3}, new int[] {5}}", javaIntArray.getValue());
	}
	
	public void testOneArrayInsideArray() throws ConverterException, JavaCoreException {
		// IMPORTANT
		// The array-element inside the array is an object, so the array type cannot be int[]
		String jsStringArray = "[5, [10, 20, 30], 42]";
		JavaConstantValue javaStringArray = TypeDeclarationChanger.changeArrayDeclaration(jsStringArray, mCodeBlock);
		assertEquals("Object[]", javaStringArray.getType());
		assertEquals("new Object[] {5, new int[] {10, 20, 30}, 42}", javaStringArray.getValue());
	}
	
	public void testChangeCompositeDeclaration() throws ConverterException, JavaCoreException {
		String jsComposite = "5 + 5 / 2";
		IJavaValue javaComposite = TypeDeclarationChanger.changeCompositeDeclaration(jsComposite, mCodeBlock);
		assertEquals("int", javaComposite.getType());
		assertEquals("5 + 5 / 2", javaComposite.getValue());
	}
	
	public void testChangeCompositeDeclarationBrackets() throws ConverterException, JavaCoreException {
		String jsComposite = "(5 + 5) / 2 + 5";
		IJavaValue javaComposite = TypeDeclarationChanger.changeCompositeDeclaration(jsComposite, mCodeBlock);
		assertEquals("int", javaComposite.getType());
		assertEquals("(5 + 5) / 2 + 5", javaComposite.getValue());
	}
	
	public void testChangeStringComposite() throws ConverterException, JavaCoreException {
		String jsComposite = "\"foo\" + \"boo\"";
		IJavaValue javaComposite = TypeDeclarationChanger.changeCompositeDeclaration(jsComposite, mCodeBlock);
		assertEquals("String", javaComposite.getType());
		assertEquals("\"foo\" + \"boo\"", javaComposite.getValue());
	}
	
	public void testChangeCompositeWithOneValueBoolean() throws ConverterException, JavaCoreException {
		String jsValue = "false";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("boolean", javaValue.getType());
		assertEquals("false", javaValue.getValue());
	}
	
	public void testChangeCompositeWithOneValueInteger() throws ConverterException, JavaCoreException {
		String jsValue = "42";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("int", javaValue.getType());
		assertEquals("42", javaValue.getValue());
	}
	
	public void testChangeCompositeWithOneValueString() throws ConverterException, JavaCoreException {
		String jsValue = "\"foo\"";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("String", javaValue.getType());
		assertEquals("\"foo\"", javaValue.getValue());
	}
	
	public void testIllegalOperator() throws ConverterException, JavaCoreException{
		String jsValue = "true + false";
		boolean exception = false;
		try {
			TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		} catch (ConverterException e) {
			exception = true;
		}
		assertTrue(exception);
	}
	
	public void testAddIntegers() throws ConverterException, JavaCoreException {
		String jsValue = "42 + 15";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("int", javaValue.getType());
		assertEquals("42 + 15", javaValue.getValue());
	}
	
	public void testAddDoubles() throws ConverterException, JavaCoreException {
		String jsValue = "42.03 + 15.2";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("double", javaValue.getType());
		assertEquals("42.03d + 15.2d", javaValue.getValue());
	}
	
	public void testAddStringToInteger() throws ConverterException, JavaCoreException {
		String jsValue = "42 + \"foo\"";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("String", javaValue.getType());
		assertEquals("42 + \"foo\"", javaValue.getValue());
	}
	
	public void testAddStringToTwoIntegers() throws ConverterException, JavaCoreException {
		String jsValue = "42 + 21 + \"foo\"";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("String", javaValue.getType());
		assertEquals("42 + 21 + \"foo\"", javaValue.getValue());
	}
	
	public void testAddIntegerToString() throws ConverterException, JavaCoreException {
		String jsValue = "\"foo\" + 42";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("String", javaValue.getType());
		assertEquals("\"foo\" + 42", javaValue.getValue());
	}
	
	public void testAddTwoInteegrsToString() throws ConverterException, JavaCoreException {
		String jsValue = "\"foo\" + 42 + 21";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("String", javaValue.getType());
		assertEquals("\"foo\" + 42 + 21", javaValue.getValue());
	}
	
	public void testAddIntegerAndDouble() throws ConverterException, JavaCoreException {
		String jsValue = "30.28 + 10";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("double", javaValue.getType());
		assertEquals("30.28d + 10", javaValue.getValue());
	}
	
	public void testAddDoubleAndInteger() throws ConverterException, JavaCoreException {
		String jsValue = "10 + 30.28";
		IJavaValue javaValue = TypeDeclarationChanger.changeCompositeDeclaration(jsValue, mCodeBlock);
		assertEquals("double", javaValue.getType());
		assertEquals("10 + 30.28d", javaValue.getValue());
	}
}
