package de.jstojconverter;

import de.jstojconverter.converter.value.*;
import de.jstojconverter.javacore.value.*;
import junit.framework.*;

public class PrimitiveValueConverterTest extends TestCase {
	
	public void testChangeBoolean() {
		String jsBooleanValue = "true";
		JavaConstantValue javaBooleanValue = PrimitiveValueConverter.convertBooleanValue(jsBooleanValue);
		assertEquals("boolean", javaBooleanValue.getType());
		assertEquals("true", javaBooleanValue.getValue());
	}

	public void testChangeInteger() {
		String jsIntegerValue = "42";
		JavaConstantValue javaIntegerValue = PrimitiveValueConverter.convertIntegerValue(jsIntegerValue);
		assertEquals("int", javaIntegerValue.getType());
		assertEquals("42", javaIntegerValue.getValue());
	}

	public void testChangeNegativeInteger() {
		String jsIntegerValue = "-42";
		JavaConstantValue javaIntegerValue = PrimitiveValueConverter.convertIntegerValue(jsIntegerValue);
		assertEquals("int", javaIntegerValue.getType());
		assertEquals("-42", javaIntegerValue.getValue());
	}

	public void testChangeDouble() {
		String jsDoubleValue = "0.25";
		JavaConstantValue javaDoubleValue = PrimitiveValueConverter.convertDoubleValue(jsDoubleValue);
		assertEquals("double", javaDoubleValue.getType());
		assertEquals("0.25d", javaDoubleValue.getValue());
	}

	public void testChangeNegativeDouble() {
		String jsDoubleValue = "-42.3";
		JavaConstantValue javaDoubleValue = PrimitiveValueConverter.convertDoubleValue(jsDoubleValue);
		assertEquals("double", javaDoubleValue.getType());
		assertEquals("-42.3d", javaDoubleValue.getValue());
	}
}
