package de.jstojconverter.jscore;

import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsConstantValue;
import de.jstojconverter.old.jsparser.JsParserException;
import junit.framework.TestCase;

/**
 * @author Felix Jordan
 * @since 23.05.2015 - 19:04:32
 * @version 1.0
 */
public class JsAssignmentStatementTest extends TestCase {
	
//	public void testCreate() throws JsParserException {
//		JsAssignmentStatement statement = new JsAssignmentStatement("foo", JsAssignmentOperator.NORMAL,
//				new JsConstantValue("42", IJsValue.TYPE_NUMBER));
//		assertEquals("foo", statement.getVariableName());
//		IJsValue value = statement.getValue();
//		assertEquals("42", value.getValue());
//		assertEquals(IJsValue.TYPE_NUMBER, value.getType());
//		assertEquals("foo = 42;\n", statement.getSourceCode());
//	}
}
