package de.jstojconverter.jscore;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsConstantValue;
import de.jstojconverter.old.jsparser.JsParserException;
import junit.framework.TestCase;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 17:52:49
 * @version 1.0
 */
public class JsDeclarationStatementTest extends TestCase {
	
//	public void testCreateWithoutAssignment() {
//		JsDeclarationStatement statement = new JsDeclarationStatement();
//		assertEquals("foo", statement.getVariableName());
//		assertFalse(statement.hasValue());
//		assertNull(statement.getValue());
//		assertEquals("var foo;\n", statement.getSourceCode());
//	}
//	
//	public void testCreateWithAssignment() throws JsParserException {
//		JsDeclarationStatement statement = new JsDeclarationStatement("foo", new JsConstantValue("42", IJsValue.TYPE_NUMBER));
//		assertEquals("foo", statement.getVariableName());
//		assertTrue(statement.hasValue());
//		IJsValue value = statement.getValue();
//		assertNotNull(value);
//		assertEquals("42", value.getValue());
//		assertEquals(IJsValue.TYPE_NUMBER, value.getType());
//		assertEquals("var foo = 42;\n", statement.getSourceCode());
//	}
}
