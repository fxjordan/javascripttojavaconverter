package de.jstojconverter;

import de.jstojconverter.old.jsparser.JsStatementTypeDetection;
import junit.framework.TestCase;

public class StatementTypeDetectionTest extends TestCase {
	
	public void testDeclerationStatement() {
		String statement = "var test;";
		assertTrue(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
	
	public void testDeclerationWithAssignmentStatement() {
		String statement = "var test = \"foo\";";
		assertFalse(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertTrue(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
	
	public void testDeclerationWithMethodAssignmentStatement() {
		String statement = "var value = test.getValue();";
		assertFalse(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertTrue(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
	
	public void testAssignmentStatement() {
		String statement = "test = \"foo\";";
		assertFalse(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertTrue(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
	
	public void testMethodAssignmentStatement() {
		String statement = "value = test.getValue();";
		assertFalse(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertTrue(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
	
	public void testMethodInvocationStatement() {
		String statement = "alert(\"foo\");";
		assertFalse(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
	
	public void testMethodInvocationOnVariableStatement() {
		String statement = "test.setValue(42);";
		assertFalse(JsStatementTypeDetection.isDeclarationStatement(statement));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement(statement));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatement(statement));
	}
}
