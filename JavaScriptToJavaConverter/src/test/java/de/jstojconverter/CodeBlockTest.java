package de.jstojconverter;

import de.jstojconverter.javacore.JavaClass;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.JavaConstructor;
import de.jstojconverter.javacore.JavaDoWhileLoop;
import de.jstojconverter.javacore.JavaElseCondition;
import de.jstojconverter.javacore.JavaElseIfCondition;
import de.jstojconverter.javacore.JavaForLoop;
import de.jstojconverter.javacore.JavaIfCondition;
import de.jstojconverter.javacore.JavaMethod;
import de.jstojconverter.javacore.JavaWhileLoop;
import de.jstojconverter.javacore.info.JavaTypeManager;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.javacore.*;
import de.jstojconverter.javacore.info.*;
import de.jstojconverter.converter.*;
import de.jstojconverter.javacore.value.*;
import de.jstojconverter.javacore.operators.*;

public class CodeBlockTest {
	
	public static void main(String[] args) {
		new CodeBlockTest();
	}
	
	public CodeBlockTest() {
		try {
			testGetSourceCode();
		} catch (JavaCoreException e) {
			e.printStackTrace();
		}
	}
	
	public void testGetSourceCode() throws JavaInfoException, JavaCoreException {
		JavaFile file = new JavaFile("de.jstojconverter.test", "CodeBlockTest");
		JavaClass clazz = file.getJavaClass();
		JavaMethod mainMethod = clazz.createMethod("public", "static", "main", "void", new JavaParameter("pArgs", "String[]"));
		JavaConstructor constructor = clazz.createConstructor();
		
		constructor.createIf(new JavaComparison(new JavaConstantValue("10","int"),
			new JavaConstantValue("5", "int"), JavaComparisonOperator.GREATER))
			.createDeclarationStatement("foo", "int",new JavaConstantValue("42", "int"));
		
		
		/*JavaCodeBlock codeBlock = new JavaDoWhileLoop("10 < 5");
		codeBlock.addJsContent(new JsContent("var test = 42;", JsContent.TYPE_STATEMENT));
		JavaIfCondition ifBlock = new JavaIfCondition("test == 21");
		ifBlock.addJsContent(new JsContent("alert('foo');", JsContent.TYPE_STATEMENT));
		codeBlock.addContent(ifBlock);
		codeBlock.addContent(new JavaElseIfCondition("false"));
		codeBlock.addContent(new JavaElseCondition());
		
		JavaCodeBlock whileLoop = new JavaWhileLoop("test > 21");
		whileLoop.addJsContent(new JsContent("var test = 'foo';", JsContent.TYPE_STATEMENT));
		whileLoop.addJsContent(new JsContent("var test = 'foo';", JsContent.TYPE_STATEMENT));
		
		codeBlock.addContent(whileLoop);
		codeBlock.addContent(new JavaForLoop("int i=0", "i<42", "i++"));
		codeBlock.addJsContent(new JsContent("var test = 42;", JsContent.TYPE_STATEMENT));*/
		System.out.println(clazz.getSourceCode());
	}
}
