package de.jstojconverter;

import junit.framework.TestCase;

public class MethodDetectionTest extends TestCase {
	
	public void testSimpleMethod() {
		String method = "startRunning()";
		assertTrue(MethodDetection.isMethodValue( method));
		method = "startRunning(42)";
		assertTrue(MethodDetection.isMethodValue(method));
		
		method = "player.startRunning()";
		assertTrue(MethodDetection.isMethodValue(method));
		
		method = "palyer.startRunning(player.getSpeed())";
		assertTrue(MethodDetection.isMethodValue(method));
	}
}
