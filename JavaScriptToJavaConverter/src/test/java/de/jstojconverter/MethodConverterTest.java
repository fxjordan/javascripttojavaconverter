package de.jstojconverter;

import de.jstojconverter.converter.*;
import de.jstojconverter.converter.value.*;
import de.jstojconverter.javacore.*;
import de.jstojconverter.javacore.util.*;
import de.jstojconverter.javacore.value.*;
import de.jstojconverter.jsreader.*;
import java.util.*;

public class MethodConverterTest {
	
	public static void main(String[] args) {
		new MethodConverterTest();
	}
	
	public MethodConverterTest() {
		try {
			JavaFile file = new JavaFile("de.jstojconverter.codetest", "CodeTest");
			JavaClass javaClass = file.getJavaClass();
			JavaMethod mainMethod = javaClass.createMethod("public", "static", "main", "void", new JavaParameter("pArgs", "java.lang.String[]"));
			mainMethod.createDeclarationStatement("comment", "java.lang.String", new JavaConstantValue("'Call Constructor here.'", "java.lang.String"));
			JavaConstructor constructor = javaClass.createConstructor();

			MethodValueConverter.convertMethod(constructor, "player.setSpeed(42)");

			System.out.println(file.getSourceCode());
		} catch (JavaCoreException e) {
			throw new RuntimeException("Failed to convert JavaScript into Java", e);
		} catch (ConverterException e) {
			throw new RuntimeException("Failed to convert JavaScript into Java", e);
		}
	}
}
