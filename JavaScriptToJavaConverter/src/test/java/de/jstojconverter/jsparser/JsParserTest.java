package de.jstojconverter.jsparser;

import java.io.File;
import java.util.List;

import de.jstojconverter.jsreader.JsReader;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.jsreader.JsReaderException;
import de.jstojconverter.jsparser.JsParser;
import de.jstojconverter.util.*;
import de.jstojconverter.jscore.*;

/**
 * @author Felix Jordan
 * @since 01.08.2015 - 16:30:02
 * @version 1.0
 */
public class JsParserTest {
	
	public static final String LOG_TAG = "JsParserTest";
	
	private static final boolean RUNNING_ON_ANDROID = true;
	
	private static final String JS_READER_INPUT_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "JavaScriptToJavaConverter/jstoj-input/";
	private static final String JS_READER_INPUT_FOLDER_ANDROID = "/storage/emulated/0/AppProjects/JavaScriptToJavaConverter/"
	+ "JavaScriptToJavaConverter/jstoj-input/";
	
	public static void main(String[] pArgs) {
		new JsParserTest();
	}
	
	public JsParserTest() {
		String folder;
		if (RUNNING_ON_ANDROID) {
			folder = JS_READER_INPUT_FOLDER_ANDROID;
		} else {
			folder = JS_READER_INPUT_FOLDER;
		}
		JsReader reader = new JsReader(new File(folder, "testscript.js"));
		List<JsReaderContent> readerContent;
		try {
			readerContent= reader.readContent();
		} catch (JsReaderException e) {
			throw new RuntimeException(e);
		}
		JsParser parser = new JsParser(readerContent);
		JsFile file;
		try {
			file = parser.parse();
		} catch (JsParserException e) {
			throw new RuntimeException(e);
		}
		Log.i(LOG_TAG, file.getSourceCode());
	}
}
