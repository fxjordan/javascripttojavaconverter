package de.jstojconverter.jsparser;

import de.jstojconverter.jscore.JsAssignmentStatement;
import de.jstojconverter.jscore.JsContent;
import de.jstojconverter.jscore.JsDeclarationStatement;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jsparser.JsParserException;
import de.jstojconverter.jsparser.JsStatementParser;
import junit.framework.TestCase;
import de.jstojconverter.jscore.*;
import de.jstojconverter.jscore.info.*;
import org.junit.runner.*;
import org.junit.internal.runners.statements.*;

/**
 * @author Felix Jordan
 * @since 23.05.2015 - 17:56:16
 * @version 1.0
 */
public class JsStatementParserTest extends TestCase {
	
	private JsFile mJsFile = JsFile.create();
	
	public void testParsingDeclarationStatement() throws JsParserException, JsInfoException {
		JsFunction function = mJsFile.createFunction("testDeclarationStatement", null, null);
		JsContent jsContent = JsStatementParser.parse(function, "var foo;");
		assertTrue(jsContent instanceof JsDeclarationStatement);
		JsDeclarationStatement statement = (JsDeclarationStatement) jsContent;
		assertEquals("foo", statement.getVariable().getName());
		assertFalse(statement.hasValue());
		assertNull(statement.getValue());
		assertEquals("var foo;\n", jsContent.getSourceCode());
	}
	
	public void testParseDeclarationWithAssignmentStatement() throws JsParserException, JsCoreException {
		JsFunction function = mJsFile.createFunction("testDeclarationWithAssignmentStatement", null, null);
		JsContent jsContent = JsStatementParser.parse(function, "var foo = 42;");
		assertTrue(jsContent instanceof JsDeclarationStatement);
		JsDeclarationStatement statement = (JsDeclarationStatement) jsContent;
		assertEquals("foo", statement.getVariable().getName());
		assertTrue(statement.hasValue());
		IJsValue value = statement.getValue();
		assertNotNull(value);
		assertEquals("42", value.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, value.getType());
		assertEquals("var foo = 42;\n", statement.getSourceCode());
	}
	
	public void testParseAssignmentStatement() throws JsParserException, JsCoreException {
		JsFunction function = mJsFile.createFunction("testAssignmentStatement", null, null);
		JsContent jsContent = JsStatementParser.parse(function, "foo = 42;");
		assertTrue(jsContent instanceof JsAssignmentStatement);
		JsAssignmentStatement statement = (JsAssignmentStatement) jsContent;
		assertEquals("foo", statement.getVariableName());
		IJsValue value = statement.getValue();
		assertEquals("42", value.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, value.getType());
		assertEquals("foo = 42;\n", statement.getSourceCode());
		
		jsContent = JsStatementParser.parse(function, "boo.foo = 42;");
		assertTrue(jsContent instanceof JsAssignmentStatement);
		statement = (JsAssignmentStatement) jsContent;
		assertEquals("boo.foo", statement.getVariableName());
		value = statement.getValue();
		assertEquals("42", value.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, value.getType());
		assertEquals("boo.foo = 42;\n", statement.getSourceCode());
	}
}
