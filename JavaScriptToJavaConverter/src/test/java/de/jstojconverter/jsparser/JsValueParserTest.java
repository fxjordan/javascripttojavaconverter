package de.jstojconverter.jsparser;

import de.jstojconverter.jscore.operators.JsMathOperator;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsArrayValue;
import de.jstojconverter.jscore.value.JsCompositeValue;
import de.jstojconverter.jscore.value.JsConstantValue;
import de.jstojconverter.old.jsparser.JsParserException;
import de.jstojconverter.old.jsparser.JsValueParser;
import junit.framework.TestCase;
import de.jstojconverter.jscore.*;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 22:08:53
 * @version 1.0
 */
public class JsValueParserTest extends TestCase {
	
	public void testParseBooleanValue() throws JsParserException, JsCoreException {
		IJsValue jsvalue = JsValueParser.parseValue("true");
		assertEquals("true", jsvalue.getValue());
		assertEquals(IJsValue.TYPE_BOOLEAN, jsvalue.getType());
		
		jsvalue = JsValueParser.parseValue("false");
		assertEquals("false", jsvalue.getValue());
		assertEquals(IJsValue.TYPE_BOOLEAN, jsvalue.getType());
	}
	
	public void testParseNumberValue() throws JsParserException, JsCoreException {
		IJsValue jsValue = JsValueParser.parseValue("42");
		assertEquals("42", jsValue.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("-42");
		assertEquals("-42", jsValue.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("42.73");
		assertEquals("42.73", jsValue.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("-42.73");
		assertEquals("-42.73", jsValue.getValue());
		assertEquals(IJsValue.TYPE_NUMBER, jsValue.getType());
	}
	
	public void testParseStringValue() throws JsParserException, JsCoreException {
		IJsValue jsValue = JsValueParser.parseValue("\"foo\"");
		assertEquals("\"foo\"", jsValue.getValue());
		assertEquals(IJsValue.TYPE_STRING, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("'foo'");
		assertEquals("'foo'", jsValue.getValue());
		assertEquals(IJsValue.TYPE_STRING, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("\"\"");
		assertEquals("\"\"", jsValue.getValue());
		assertEquals(IJsValue.TYPE_STRING, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("''");
		assertEquals("''", jsValue.getValue());
		assertEquals(IJsValue.TYPE_STRING, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("\"42\"");
		assertEquals("\"42\"", jsValue.getValue());
		assertEquals(IJsValue.TYPE_STRING, jsValue.getType());
		
		jsValue = JsValueParser.parseValue("'42'");
		assertEquals("'42'", jsValue.getValue());
		assertEquals(IJsValue.TYPE_STRING, jsValue.getType());
	}
	
	public void testParseCompositeValue() throws JsParserException, JsCoreException {
		IJsValue jsValue = JsValueParser.parseValue("42 + 73");
		assertEquals("42 + 73", jsValue.getValue());
		assertTrue(jsValue instanceof JsCompositeValue);
		JsCompositeValue compositeValue = (JsCompositeValue) jsValue;
		assertFalse(compositeValue.hasBrackets());
		assertEquals(JsMathOperator.ADD, compositeValue.getOperator());
		IJsValue valueA = compositeValue.getValueA();
		assertTrue(valueA instanceof JsConstantValue);
		assertTypeAndValue(valueA, "42", IJsValue.TYPE_NUMBER);
		IJsValue valueB = compositeValue.getValueB();
		assertTrue(valueB instanceof JsConstantValue);
		assertTypeAndValue(valueB, "73", IJsValue.TYPE_NUMBER);
		
		// TODO Fix bug in JsValueParser that MUL and DIV are parsed before ADD and SUB
		// so that the terms are composed correct if they are 'rendered'.
//		jsValue = JsValueParser.parseValue("42 + 73 * 2");
//		assertEquals("42 + 73 * 2", jsValue.getValue());
//		assertTrue(jsValue instanceof JsCompositeValue);
//		compositeValue = (JsCompositeValue) jsValue;
//		assertFalse(compositeValue.hasBrackets());
//		assertEquals(JsMathOperator.ADD, compositeValue.getOperator());
//		valueA = compositeValue.getValueA();
//		assertTrue(valueA instanceof JsConstantValue);
//		assertTypeAndValue(valueA, "42", IJsValue.TYPE_NUMBER);
//		valueB = compositeValue.getValueB();
//		assertTrue(valueB instanceof JsConstantValue);
//		assertTypeAndValue(valueB, "73", IJsValue.TYPE_NUMBER);
		
		jsValue = JsValueParser.parseValue("((42 + 73) * 2)");
		assertEquals("((42 + 73) * 2)", jsValue.getValue());
		assertTrue(jsValue instanceof JsCompositeValue);
		compositeValue = (JsCompositeValue) jsValue;
		assertEquals(JsMathOperator.MUL, compositeValue.getOperator());
		valueA = compositeValue.getValueA();
		assertTrue(valueA instanceof JsCompositeValue);
		JsCompositeValue composuteValueA = (JsCompositeValue) valueA;
		assertTrue(composuteValueA.hasBrackets());
		assertTypeAndValue(valueA, "(42 + 73)", IJsValue.TYPE_NUMBER);
		valueB = compositeValue.getValueB();
		assertTrue(valueB instanceof JsConstantValue);
		assertTypeAndValue(valueB, "2", IJsValue.TYPE_NUMBER);
	}
	
	public void testParseArrayValue() throws JsParserException, JsCoreException {
		IJsValue jsValue = JsValueParser.parseValue("[42, 73, 21, 37]");
		assertTypeAndValue(jsValue, "[42, 73, 21, 37]", IJsValue.TYPE_ARRAY);
		assertTrue(jsValue instanceof JsArrayValue);
		JsArrayValue arrayValue = (JsArrayValue) jsValue;
		assertEquals(4, arrayValue.getLength());
		assertTypeAndValue(arrayValue.getElement(0), "42", IJsValue.TYPE_NUMBER);
		assertTypeAndValue(arrayValue.getElement(1), "73", IJsValue.TYPE_NUMBER);
		assertTypeAndValue(arrayValue.getElement(2), "21", IJsValue.TYPE_NUMBER);
		assertTypeAndValue(arrayValue.getElement(3), "37", IJsValue.TYPE_NUMBER);
		
		jsValue = JsValueParser.parseValue("['foo', 'boo', [42, 73], 5 + 3]");
		assertTypeAndValue(jsValue, "['foo', 'boo', [42, 73], 5 + 3]", IJsValue.TYPE_ARRAY);
		assertTrue(jsValue instanceof JsArrayValue);
		arrayValue = (JsArrayValue) jsValue;
		assertEquals(4, arrayValue.getLength());
		assertTypeAndValue(arrayValue.getElement(0), "'foo'", IJsValue.TYPE_STRING);
		assertTypeAndValue(arrayValue.getElement(1), "'boo'", IJsValue.TYPE_STRING);
		IJsValue subArrayValue = arrayValue.getElement(2);
		assertTypeAndValue(subArrayValue, "[42, 73]", IJsValue.TYPE_ARRAY);
		assertTrue(subArrayValue instanceof JsArrayValue);
		JsArrayValue subArray = (JsArrayValue) subArrayValue;
		assertEquals(2, subArray.getLength());
		assertTypeAndValue(subArray.getElement(0), "42", IJsValue.TYPE_NUMBER);
		assertTypeAndValue(subArray.getElement(1), "73", IJsValue.TYPE_NUMBER);
		IJsValue subCompositeValue = arrayValue.getElement(3);
		assertTypeAndValue(subCompositeValue, "5 + 3", IJsValue.TYPE_NUMBER);
		assertTrue(subCompositeValue instanceof JsCompositeValue);
		JsCompositeValue subComposite = (JsCompositeValue) subCompositeValue;
		assertEquals(JsMathOperator.ADD, subComposite.getOperator());
		assertTypeAndValue(subComposite, "5", IJsValue.TYPE_NUMBER);
		assertTypeAndValue(subComposite, "3", IJsValue.TYPE_NUMBER);
		
		/*
		 * TODO Add test where methods or constructors are invoked inside the array.
		 *      The commas inside the method/function invocations must be ignored by
		 *      the parser while the ements are splitted. (see todo comment inside
		 *      JsValueParser class
		 */
	}
	
	private void assertTypeAndValue(IJsValue pValue, String pExpectedValue, String pExpectedType) throws JsParserException, JsCoreException {
		assertEquals(pExpectedValue, pValue.getValue());
		assertEquals(pExpectedType, pValue.getType());
	}
}
