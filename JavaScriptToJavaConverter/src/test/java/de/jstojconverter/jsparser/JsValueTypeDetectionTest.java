package de.jstojconverter.jsparser;

import de.jstojconverter.old.jsparser.JsValueTypeDetection;
import junit.framework.TestCase;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 18:27:47
 * @version 1.0
 */
public class JsValueTypeDetectionTest extends TestCase {
	
	public void testIsBooleanValue() {
		assertTrue(JsValueTypeDetection.isBooleanValue("true"));
		assertTrue(JsValueTypeDetection.isBooleanValue("false"));
		assertFalse(JsValueTypeDetection.isBooleanValue("foo"));
		assertFalse(JsValueTypeDetection.isBooleanValue("0"));
	}
	
	public void testIsNumberValue() {
		assertTrue(JsValueTypeDetection.isNumberValue("42"));
		assertTrue(JsValueTypeDetection.isNumberValue("42.37"));
		assertTrue(JsValueTypeDetection.isNumberValue("-42"));
		assertTrue(JsValueTypeDetection.isNumberValue("-42.37"));
		assertFalse(JsValueTypeDetection.isNumberValue("--42"));
		assertFalse(JsValueTypeDetection.isNumberValue("-4-2"));
		assertFalse(JsValueTypeDetection.isNumberValue("42.3.5"));
		assertFalse(JsValueTypeDetection.isNumberValue("42..21"));
		assertFalse(JsValueTypeDetection.isNumberValue("foo"));
	}
	
	public void testIsStringValue() {
		assertTrue(JsValueTypeDetection.isStringValue("\"foo\""));
		assertTrue(JsValueTypeDetection.isStringValue("'foo'"));
		assertTrue(JsValueTypeDetection.isStringValue("'42'"));
		assertTrue(JsValueTypeDetection.isStringValue("'true'"));
		assertFalse(JsValueTypeDetection.isStringValue("foo"));
		assertFalse(JsValueTypeDetection.isStringValue("\"foo"));
		assertFalse(JsValueTypeDetection.isStringValue("'foo"));
		assertFalse(JsValueTypeDetection.isStringValue("foo\""));
		assertFalse(JsValueTypeDetection.isStringValue("foo'"));
		assertFalse(JsValueTypeDetection.isStringValue("42"));
	}
	
	public void testIsArrayValue() {
		assertTrue(JsValueTypeDetection.isArrayValue("[42, 21, 73, 37, 21, 12, 37]"));
		assertTrue(JsValueTypeDetection.isArrayValue("['foo', 'boo', '42']"));
		assertTrue(JsValueTypeDetection.isArrayValue("[]"));
		assertFalse(JsValueTypeDetection.isArrayValue("[42, 73"));
		assertFalse(JsValueTypeDetection.isArrayValue("42, 73, 'foo']"));
		assertFalse(JsValueTypeDetection.isArrayValue("42"));
		assertFalse(JsValueTypeDetection.isArrayValue("'foo'"));
	}
	
	public void testIsCompositeValue() {
		assertTrue(JsValueTypeDetection.isCompositeValue("5 + 3"));
		assertTrue(JsValueTypeDetection.isCompositeValue("(5 / 3"));
		// TODO Implement logical operators
//		assertTrue(JsValueTypeDetection.isCompositeValue("true && false"));
		assertTrue(JsValueTypeDetection.isCompositeValue("foo *= 42"));
	}
}
