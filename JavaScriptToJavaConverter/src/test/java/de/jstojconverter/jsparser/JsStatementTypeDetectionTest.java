package de.jstojconverter.jsparser;

import de.jstojconverter.jsparser.JsStatementTypeDetection;

import junit.framework.TestCase;

/**
 * @author Felix Jordan
 * @since 22.05.2015 - 22:47:49
 * @version 1.0
 */
public class JsStatementTypeDetectionTest extends TestCase {

	public void testIsDetectionStatement() {
		assertTrue(JsStatementTypeDetection.isDeclarationStatement("var foo;"));
		assertFalse(JsStatementTypeDetection.isDeclarationStatement("var foo = 42;"));
		assertFalse(JsStatementTypeDetection.isDeclarationStatement("foo;"));
		// TODO Type detection must check if there is a variable name
//		assertFalse(JsStatementTypeDetection.isStatementDeclaration("var;"));
	}
	
	public void testIsDetectionWithAssignmentStatement() {
		assertTrue(JsStatementTypeDetection.isDeclarationWithAssignmentStatement("var foo = 42;"));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement("var foo;"));
		assertFalse(JsStatementTypeDetection.isDeclarationWithAssignmentStatement("foo = 42;"));
		// TODO Type detection must check if there is a variable name
//		assertFalse(JsStatementTypeDetection.isStatementDeclarationWithAssignment("var = 42;"));
	}
	
	public void testIsAssignmentStatement() {
		assertTrue(JsStatementTypeDetection.isAssignmentStatement("foo = 42;"));
		assertTrue(JsStatementTypeDetection.isAssignmentStatement("foo += 42;"));
		assertTrue(JsStatementTypeDetection.isAssignmentStatement("boo.foo = 42;"));
		assertTrue(JsStatementTypeDetection.isAssignmentStatement("boo().foo = 42;"));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement("var foo = 42;"));
		assertFalse(JsStatementTypeDetection.isAssignmentStatement("= 42;"));
	}
	
	public void testIsMethodInvocationStatementOLD() {
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatementOLD("foo();"));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatementOLD("boo.foo();"));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatementOLD("foo(42, getArgs());"));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatementOLD("boo().foo();"));
		
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatementOLD("getPerson().name = getName();"));
	}
	
	public void testIsMethodInvocationStatement() {
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatement("foo();"));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatement("boo.foo();"));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatement("foo(42, getArgs());"));
		assertTrue(JsStatementTypeDetection.isFunctionInvocationStatement("boo().foo();"));
		
		assertFalse(JsStatementTypeDetection.isFunctionInvocationStatement("getPerson().name = getName();"));
	}
}
