package de.jstojconverter;

import junit.framework.TestCase;

public class TypeDetectionTest extends TestCase {
	
	public void testIsValueInteger() {
		String jsValue = "42";
		assertTrue(TypeDetection.isIntegerValue(jsValue));
		jsValue =  "42 + 5";
		assertFalse(TypeDetection.isIntegerValue(jsValue));
		jsValue = "(42)";
		assertFalse(TypeDetection.isIntegerValue(jsValue));
		jsValue = "(42 + 5)";
		assertFalse(TypeDetection.isIntegerValue(jsValue));
	}
	
	
	public void testIsValueComposite() {
		String value = "(5)";
		assertTrue(TypeDetection.isCompositeValue(value));
		value = "5 + 10";
		assertTrue(TypeDetection.isCompositeValue(value));
		value = "5";
		assertFalse(TypeDetection.isCompositeValue(value));
		value = "5 + 3 / (10 + 5)";
		assertTrue(TypeDetection.isCompositeValue(value));
	}
}
