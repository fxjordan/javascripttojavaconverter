package de.jstojconverter;

import de.jstojconverter.javacore.operators.JavaComparisonOperator;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaConstantValue;

public class StringComparisonConverter {

	private StringComparisonConverter() {
	}
	
	public static IJavaValue getValue(IJavaValue pValueA, IJavaValue pValueB, JavaComparisonOperator pOperator) {
		String valueA = "java.lang.String.valueOf(" + pValueA.getValue() + ")";
		String valueB = "java.lang.String.valueOf(" + pValueB.getValue() + ")";
		
		return new JavaConstantValue(valueA + ".compareTo(" + valueB + ") " + pOperator.toString() + " 0", "boolean");
	}
}
