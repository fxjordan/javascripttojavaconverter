package de.jstojconverter;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.converter.function.JsFunctionConverter;
import de.jstojconverter.converter.statement.StatementConverter;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.JavaElseCondition;
import de.jstojconverter.javacore.JavaElseIfCondition;
import de.jstojconverter.javacore.JavaFile;
import de.jstojconverter.javacore.JavaIfCondition;
import de.jstojconverter.javacore.JavaWhileLoop;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.util.StringUtils;
import de.jstojconverter.javacore.info.*;

public class JsToJConverter {
	
	private static final String DEFAULT_CONVERTER_PACKAGE = "de.jstojconverter";
	private static final String DEFAULT_JAVA_FILE_NAME = "JsToJOutput";
	
	/** JavaFile which contains all converted content. */
	private JavaFile mJavaFile;
	
	/** CodeBlock to add content which is not a field, method or constructor. */
	private JavaCodeBlock mDefaultContentContainer;
	
	private JsFunctionConverter mFunctionConverter;
	
	private List<JsReaderContent> mJsContent;
	
	public JsToJConverter() {
		mJsContent = new ArrayList<JsReaderContent>();
		try {
			mJavaFile = new JavaFile(DEFAULT_CONVERTER_PACKAGE, DEFAULT_JAVA_FILE_NAME);
			mDefaultContentContainer = mJavaFile.getJavaClass().createMethod("public", "static", "main", "void",
					new JavaParameter("pArgs", "String[]"));
		} catch (JavaInfoException e) {
			throw new RuntimeException("Failed to create Converter");
		}
	}
	
	public void setJsContent(List<JsReaderContent> pJsContent) {
		mJsContent.clear();
		mJsContent.addAll(pJsContent);
		for (JsReaderContent content : mJsContent) {
			if (content.getType() == JsReaderContent.TYPE_FUNCTION_DECLARATION) {
				mFunctionConverter.addJsFunction(content);
			}
		}
	}
}
