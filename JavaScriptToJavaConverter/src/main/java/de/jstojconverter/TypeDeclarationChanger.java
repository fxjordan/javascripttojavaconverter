package de.jstojconverter;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.converter.value.MethodValueConverter;
import de.jstojconverter.converter.value.PrimitiveValueConverter;
import de.jstojconverter.converter.value.SpecialValueConverter;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.javacore.info.JavaVariableInfo;
import de.jstojconverter.javacore.operators.JavaComparisonOperator;
import de.jstojconverter.javacore.operators.IJavaOperator;
import de.jstojconverter.javacore.operators.JavaOperator;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaConstantValue;
import de.jstojconverter.javacore.value.JavaVariable;
import de.jstojconverter.util.StringUtils;
import de.jstojconverter.javacore.operators.*;
import de.jstojconverter.javacore.info.*;
import de.jstojconverter.javacore.*;

public class TypeDeclarationChanger {

	private TypeDeclarationChanger() {
	}
	
	protected static JavaConstantValue changeStringDeclaration(String pDeclaration) {
		if (pDeclaration.startsWith("") && pDeclaration.endsWith("\"")) {
			return new JavaConstantValue(pDeclaration, "java.lang.String");
		}
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("\"");
		stringBuilder.append(pDeclaration.substring(1, pDeclaration.length()-1));
		stringBuilder.append("\"");
		return new JavaConstantValue(stringBuilder.toString(), "java.lang.String");
	}
	
	protected static JavaConstantValue changeArrayDeclaration(String pDeclaration, JavaCodeBlock pCodeBlock) throws ConverterException, JavaInfoException, JavaOperatorException, JavaCoreException {
		String content = pDeclaration.substring(1, pDeclaration.length()-1);
		String type = null;
		
		List<String> elements = new ArrayList<String>();
		int elementIndex = -1;
		boolean waitForClose = false;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '[') {
				waitForClose = true;
				continue;
			}
			if (content.charAt(i) == ']' && waitForClose) {
				waitForClose = false;
				continue;
			}
			if (!waitForClose) {
				if (content.charAt(i) == ',') {
					elements.add(content.substring(elementIndex+1, i));
					elementIndex = i;
				}
			}
		}
		elements.add(content.substring(elementIndex+1, content.length()));
		
		for (String element : elements) {
			IJavaValue changedElement = getValueObject(StringUtils.removeSpaceAndTabOutside(element), pCodeBlock);
			content = content.replace(StringUtils.removeSpaceAndTabOutside(element), changedElement.getValue());
			if (type == null) {
				type = changedElement.getType();
				continue;
			}
			if (!type.equals(changedElement.getType())) {
				type = "java.lang.Object";
				break;
			}
		}
		type = type + "[]";
		
		StringBuilder arrayBuilder = new StringBuilder();
		arrayBuilder.append("new ");
		arrayBuilder.append(type);
		arrayBuilder.append(" {");
		arrayBuilder.append(content);
		arrayBuilder.append("}");
		
		JavaConstantValue value = new JavaConstantValue(arrayBuilder.toString(), type);
		return value;
	}
	
	protected static IJavaValue changeCompositeDeclaration(String pDeclaration, JavaCodeBlock pCodeBlock) throws ConverterException, JavaOperatorException, JavaInfoException, JavaCoreException {
		String content;
		boolean removedBrackets = false;
		if (pDeclaration.startsWith("(") && pDeclaration.endsWith(")")) {
			content = pDeclaration.substring(1, pDeclaration.length()-1);
			removedBrackets = true;
		} else {
			content = pDeclaration;
		}
		//String type = null;
		
		List<String> elements = new ArrayList<String>();
		List<String> operators = new ArrayList<String>();
		int elementIndex = 0;
		boolean waitForClose = false;
		int openedElements = 0;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '(') {
				waitForClose = true;
				openedElements++;
				continue;
			}
			if (content.charAt(i) == ')') {
				waitForClose = false;
				openedElements--;
				continue;
			}
			if (openedElements == 0 && !waitForClose) {
				String operator = StringUtils.beginsWithOneOf(content, i, JavaOperator.STRING_VALUES);
				if (operator != null) {
					elements.add(content.substring(elementIndex, i));
					operators.add(operator);
					elementIndex = i+operator.length();
					i += operator.length()-1;
				}
			}
		}
		elements.add(content.substring(elementIndex, content.length()));
		
		String lastElement = elements.get(0);
		IJavaValue lastValue = getValueObject(StringUtils.removeSpaceAndTabOutside(lastElement), pCodeBlock);
		content = content.replace(StringUtils.removeSpaceAndTabOutside(lastElement), lastValue.getValue());
		lastElement = lastValue.getValue();
		
		for (int i=1; i<elements.size(); i++) {
			String element = elements.get(i);
			IJavaValue value = getValueObject(StringUtils.removeSpaceAndTabOutside(element), pCodeBlock);
			content = content.replace(StringUtils.removeSpaceAndTabOutside(element), value.getValue());
			element = value.getValue();
			
			String operatorString = operators.get(i-1);
			
			IJavaOperator operator = JavaOperator.getOperator(operatorString);
			if (operator.isComparisonOperator() &&
					(lastValue.getType().equals("java.lang.String") || value.getType().equals("java.lang.String"))) {
				lastValue = StringComparisonConverter.getValue(lastValue, value, (JavaComparisonOperator) operator);
			} else {
				String newValue = lastValue.getValue() + " " + operator + " " + value.getValue();
				
				String newType = operator.getResultTypeFor(lastValue, value);
				if (lastValue.isVariable()) {
					JavaVariableInfo variableInfo = pCodeBlock.getInfo().getVariable(lastValue.getValue());
					if (variableInfo.getType().equals("undefined")) {
						variableInfo.changeType(newType);
					}
				}
				
				// getResultType throws if the operator is undefined for the types
				lastValue = new JavaConstantValue(newValue, newType);
			}
				
			lastElement = element;
		}
		if (removedBrackets) {
			return new JavaConstantValue("(" + lastValue.getValue() + ")", lastValue.getType());
		}
		return new JavaConstantValue(lastValue.getValue(), lastValue.getType());
	}

	private static JavaVariable changeVariableDeclaration(String pName, JavaCodeBlockInfo pInfo) throws ConverterException, JavaInfoException {
		return new JavaVariable(pName, pInfo.getVariable(pName).getType());
	}
	
	public static IJavaValue getValueObject(String pValue, JavaCodeBlock pCodeBlock) throws ConverterException, JavaInfoException, JavaOperatorException, JavaCoreException {
		IJavaValue specialValue = SpecialValueConverter.convertValue(pValue);
		if (specialValue != null) {
			return specialValue;
		}
		
		if (TypeDetection.isBooleanValue(pValue)) {
			return PrimitiveValueConverter.convertBooleanValue(pValue);
		}
		if (TypeDetection.isIntegerValue(pValue)) {
			return PrimitiveValueConverter.convertIntegerValue(pValue);
		}
		if (TypeDetection.isDoubleValue(pValue)) {
			return PrimitiveValueConverter.convertDoubleValue(pValue);
		}
		if (TypeDetection.isStringValue(pValue)) {
			return changeStringDeclaration(pValue);
		}
		if (TypeDetection.isArrayValue(pValue)) {
			return changeArrayDeclaration(pValue, pCodeBlock);
		}
		if (TypeDetection.isCompositeValue(pValue)) {
			return changeCompositeDeclaration(pValue, pCodeBlock);
		}
		
		/*if (TypeDetection.isValueAssignemnt(pValue, pCodeBlock.getInfo())) {
			return changeAssignemt(pValue, pCodeBlock);
		}*/
		
		// Check method return values
		if (MethodDetection.isMethodValue(pValue)) {
			return MethodValueConverter.convertMethod(pCodeBlock, pValue);
		}
		
		if (TypeDetection.isVariableValue(pValue, pCodeBlock.getInfo())) {
			return changeVariableDeclaration(pValue, pCodeBlock.getInfo());
		}
		throw new ConverterException("The variable " + pValue + " does not exist!");
		
		//return new JavaValue(pValue, "Object");
	}
	
	/*
	private static JavaAssignment changeAssignemt(String pValue, JavaCodeBlock pCodeBlock) {
		int equalsSign = pValue.indexOf('=');
		String variable = StringUtils.removeSpaceAndTabOutside(pValue.substring(0, equalsSign));
//		String value = StringUtils.removeSpaceAndTabOutside(pValue.substring(equalsSign+1, endIndex));
		return null;
	}*/
}
