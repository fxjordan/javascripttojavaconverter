package de.jstojconverter;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.javacore.operators.JavaOperator;
import de.jstojconverter.util.CodeValidation;
import de.jstojconverter.util.StringUtils;

public class TypeDetection {

	private TypeDetection() {
	}
	
	public static boolean isBooleanValue(String pValue) {
		return pValue.equals("true") || pValue.equals("false");
	}
	
	public static boolean isIntegerValue(String pValue) {
		int minusSignCount = StringUtils.countCharacter(pValue, '-');
		if (minusSignCount > 1) {
			return false;
		}
		if (minusSignCount == 1) {
			if (pValue.indexOf('-') != 0) {
				return false;
			}
		}
		String numbers = pValue.replace("-", "");
		return numbers.matches("[0-9]+");
	}
	
	public static boolean isDoubleValue(String pValue) {
		if (StringUtils.countCharacter(pValue, '.') != 1) {
			return false;
		}
		String numbers = pValue.replace(".", "");
		int minusSignCount = StringUtils.countCharacter(numbers, '-');
		if (minusSignCount > 1) {
			return false;
		}
		if (minusSignCount == 1) {
			if (pValue.indexOf('-') != 0) {
				return false;
			}
		}
		numbers = numbers.replace("-", "");
		return numbers.matches("[0-9]+");
	}
	
	public static boolean isStringValue(String pValue) {
		return (pValue.startsWith("\"") && pValue.endsWith("\"")
				|| (pValue.startsWith("'") && pValue.endsWith("'")));
	}
	
	public static boolean isArrayValue(String pValue) {
		return pValue.startsWith("[") && pValue.endsWith("]");
	}
	
	public static boolean isCompositeValue(String pValue) {
		if (pValue.startsWith("(") && pValue.endsWith(")")) {
			return true;
		}
		int index = -1;
		while ((index = StringUtils.containsOneOf(pValue, index+1, JavaOperator.STRING_VALUES)) != -1) {
			if (CodeValidation.isCodeValid(pValue, index)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isVariableValue(String pVariable, JavaCodeBlockInfo pInfo) {
		return pInfo.existVariable(pVariable);
	}
	
	@Deprecated
	public static boolean isValueAssignemnt(String pValue, JavaCodeBlockInfo pInfo) {
		int equalsSign = pValue.indexOf('=');
		if (equalsSign == -1) {
			return false;
		}
		String variable = StringUtils.removeSpaceAndTabOutside(pValue.substring(0, equalsSign));
		if (pInfo.existVariable(variable)) {
			return true;
		}
		return false;
	}
}
