package de.jstojconverter;

import de.jstojconverter.util.CodeValidation;

public class MethodDetection {
	
	private MethodDetection() {
	}

	public static boolean isMethodValue(String pValue) {
		if (pValue.startsWith("var")) {
			return false;
		}
		int openBracket;
		while ((openBracket = pValue.indexOf('(')) != -1) {
			if (CodeValidation.isCodeValid(pValue, openBracket)) {
				int closeBracket;
				while ((closeBracket = pValue.indexOf('(')) != -1) {
					if (CodeValidation.isCodeValid(pValue, closeBracket)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
