package de.jstojconverter;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jstojconverter.javacore.JavaClass;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.JavaConstructor;
import de.jstojconverter.javacore.JavaField;
import de.jstojconverter.javacore.JavaFile;
import de.jstojconverter.javacore.JavaMethod;
import de.jstojconverter.javacore.info.JavaTypeManager;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.javacore.value.JavaConstantValue;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.jsreader.JsReader;
import de.jstojconverter.jsreader.JsReaderException;
import de.jstojconverter.javacore.info.*;
import de.jstojconverter.javacore.*;
import de.jstojconverter.converter.*;

public class JsFileConverterTest {
	
	private static final String JS_READER_INPUT_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "JavaScriptToJavaConverter/jstoj-input/";
	
	public static void main(String[] args) {
		new JsFileConverterTest();
	}
	
	public JsFileConverterTest() {
		JsReader reader = new JsReader(new File(JS_READER_INPUT_FOLDER, "testscript.js"));
		List<JsReaderContent> contentList = null;
		try {
			contentList = reader.readContent();
		} catch (JsReaderException e) {
			throw new RuntimeException("Failed to read JavaScript", e);
		}
		if (contentList == null) {
			throw new RuntimeException("Cannot read JavaScript");
		}
		
		try {
			JavaFile file = new JavaFile("de.jstojconverter.codetest", "CodeTest");
			JavaClass javaClass = file.getJavaClass();
			JavaMethod mainMethod = javaClass.createMethod("public", "static", "main", "void", new JavaParameter("pArgs", "java.lang.String[]"));
			mainMethod.createDeclarationStatement("comment", "java.lang.String", new JavaConstantValue("'Call Constructor here.'", "java.lang.String"));
			JavaConstructor constructor = javaClass.createConstructor();
			
			// Preconvert functions
			List<JsReaderContent> jsFunctionList = new ArrayList<JsReaderContent>();
			for (JsReaderContent content : contentList) {
				if (content.getType() == JsReaderContent.TYPE_FUNCTION_DECLARATION) {
					jsFunctionList.add(content);
				}
			}
			
			JavaCodeBlockEditor constructorEditor = new JavaCodeBlockEditor(constructor);
			
			try {
				for (JsReaderContent content : contentList) {
					if (content.getType() == JsReaderContent.TYPE_FUNCTION_DECLARATION) {
						
					} else {
						constructorEditor.addJsContent(content);
					}
				}
			} catch (ConverterException e) {
				throw new RuntimeException("Failed to convert JavaScript into Java", e);
			}
			
			System.out.println(file.getSourceCode());
		} catch (JavaCoreException e) {
			throw new RuntimeException("Failed to convert JavaScript into Java", e);
		}
	}
}
