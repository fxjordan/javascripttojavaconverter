package de.jstojconverter;

import java.io.File;

import de.jstojconverter.converter.function.JsFunctionConverter;
import de.jstojconverter.javacore.JavaFile;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.jsreader.JsReader;
import de.jstojconverter.jsreader.JsReaderException;
import de.jstojconverter.javacore.*;

public class JsMethodConverterTest {
	
	private static final String JS_READER_INPUT_FOLDER = "e:/EclipseJavaEE/workspace/JavaScriptToJavaConverter/jstoj-input/";
	
	public static void main(String[] args) {
		new JsMethodConverterTest();
	}
	
	public JsMethodConverterTest() {
		try {
			JavaFile javaFile = new JavaFile("de.jstojconverter.codetest", "FunctionTest");
			
			JsReaderContent testFunction = new JsReader(new File(JS_READER_INPUT_FOLDER, "testfunction.js")).readContent().get(0);
			
			JsFunctionConverter functionConverter = new JsFunctionConverter();
			System.out.println("");
			
			functionConverter.addJsFunction(testFunction);
			
			functionConverter.convertMethod(javaFile.getJavaClass(), "foo", "int");
			
			System.out.println(javaFile.getSourceCode());
			
		} catch (ConverterException e) {
			throw new RuntimeException("Failed to convert JavaScript into Java", e);
		} catch (JsReaderException e) {
			throw new RuntimeException("Failed to read JavaScript", e);
		} catch (JavaCoreException e) {
			throw new RuntimeException(e);
		}
	}
}
