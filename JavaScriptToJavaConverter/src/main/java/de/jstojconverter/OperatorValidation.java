package de.jstojconverter;

import de.jstojconverter.javacore.operators.JavaAssignmentOperator;
import de.jstojconverter.javacore.value.IJavaValue;

public class OperatorValidation {
	
	private static final String[] PLUS_OPERATOR_TYPES = new String[] {
		"int", "double", "String"
	};
	private static final String[] SUB_OPERATOR_TYPES = new String[] {
		"int", "double"
	};
	private static final String[] MUL_OPERATOR_TYPES = new String[] {
		"int", "double"
	};
	private static final String[] DIV_OPERATOR_TYPES = new String[] {
		"int", "double"
	};
	
	private OperatorValidation() {
	}
	
	public static boolean isOperatorValidForValues(IJavaValue pValueA, IJavaValue pValueB, String pOperator) throws ConverterException {
		if (JavaAssignmentOperator.isOperator(pOperator)) {
			if (pValueA.isVariable() && pValueA.getType().equals(pValueB.getType())) {
				return true;
			}
			throw new ConverterException("The left hand side of an assignment must be a variable! " + pValueA.getValue() + " "
					+ pOperator + " " + pValueB.getValue());
		}
		
		if (pOperator.equals("+")) {
			return isInsideArray(pValueA.getType(), PLUS_OPERATOR_TYPES)
					&& isInsideArray(pValueB.getType(), PLUS_OPERATOR_TYPES);
		}
		if (pOperator.equals("-")) {
			return isInsideArray(pValueA.getType(), SUB_OPERATOR_TYPES)
					&& isInsideArray(pValueB.getType(), SUB_OPERATOR_TYPES);
		}
		if (pOperator.equals("*")) {
			return isInsideArray(pValueA.getType(), MUL_OPERATOR_TYPES)
					&& isInsideArray(pValueB.getType(), MUL_OPERATOR_TYPES);
		}
		if (pOperator.equals("/")) {
			return isInsideArray(pValueA.getType(), DIV_OPERATOR_TYPES)
					&& isInsideArray(pValueB.getType(), DIV_OPERATOR_TYPES);
		}
		throw new IllegalArgumentException("The operator '" + pOperator + "' is not valid");
	}
	
	private static boolean isInsideArray(String pValue, String[] pArray) {
		for (int i=0; i<pArray.length; i++) {
			if (pArray[i].equals(pValue)) {
				return true;
			}
		}
		return false;
	}
}
