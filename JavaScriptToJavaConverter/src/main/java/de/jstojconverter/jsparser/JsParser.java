package de.jstojconverter.jsparser;

import java.util.List;

import de.jstojconverter.jscore.JsCodeBlock;
import de.jstojconverter.jscore.JsFile;
import de.jstojconverter.jscore.JsFunction;
import de.jstojconverter.jsreader.JsReaderContent;
import org.junit.internal.runners.*;

/**
 * @author Felix Jordan
 * @since 01.08.2015 - 17:27:19
 * @version 1.0
 */
public class JsParser {
	
	private List<JsReaderContent> mContentList;
	private JsFile mJsFile;
	
	public JsParser(List<JsReaderContent> pContentList) {
		mContentList = pContentList;
		mJsFile = JsFile.create();
	}
	
	public JsFile parse() throws JsParserException {
		parse(mJsFile, mContentList);
		return mJsFile;
	}
	
	private void parse(JsCodeBlock pCodeBlock, List<JsReaderContent> pContentList) throws JsParserException {
		for (JsReaderContent content : mContentList) {
			switch (content.getType()) {
			case JsReaderContent.TYPE_STATEMENT:
				JsStatementParser.parse(mJsFile, content.getText());
				break;
			case JsReaderContent.TYPE_FUNCTION_DECLARATION:
				JsFunction function = JsFunctionParser.parse(content.getText());
				break;
			case JsReaderContent.TYPE_IF_CONDITION:
				parseIfBlock(content);
				break;
			case JsReaderContent.TYPE_ELSE_CONDITION:
				parseElseBlock(content);
				break;
			case JsReaderContent.TYPE_ELE_IF_CONDITION:
				parseElseIfBlock(content);
				break;
			case JsReaderContent.TYPE_WHILE_LOOP:
				parseWhileLoop(content);
				break;
			case JsReaderContent.TYPE_DO_WHILE_LOOP:
				parseDoWhileLoop(content);
				break;
			case JsReaderContent.TYPE_FOR_LOOP:
				parseForLoop(content);
				break;
			default:
				throw new IllegalArgumentException("Unknown JsReaderContent (" + content.getType() + ")");
			}
		}
	}

	private void parseIfBlock(JsReaderContent pContent)
	{
		// TODO: Implement this method
	}

	private void parseElseBlock(JsReaderContent content)
	{
		// TODO: Implement this method
	}

	private void parseElseIfBlock(JsReaderContent content)
	{
		// TODO: Implement this method
	}

	private void parseWhileLoop(JsReaderContent content)
	{
		// TODO: Implement this method
	}

	private void parseDoWhileLoop(JsReaderContent content)
	{
		// TODO: Implement this method
	}

	private void parseForLoop(JsReaderContent pContent)
	{
		// TODO: Implement this method
	}
}
