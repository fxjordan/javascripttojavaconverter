package de.jstojconverter.jsparser;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.info.JsInfoException;
import de.jstojconverter.jscore.operators.IJsOperator;
import de.jstojconverter.jscore.operators.JsOperator;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsArrayValue;
import de.jstojconverter.jscore.value.JsCompositeValue;
import de.jstojconverter.jscore.value.JsConstantValue;
import de.jstojconverter.jscore.value.JsFunctionValue;
import de.jstojconverter.jscore.value.JsVariable;
import de.jstojconverter.util.StringUtils;
import de.jstojconverter.jsreader.*;

/**
 * @author Felix Jordan
 * @since 02.08.2015 - 00:56:45
 * @version 1.0
 */
public class JsValueParser {
	
	private JsValueParser() {
	}
	
	public static IJsValue parseValue(String pValue, JsCodeBlockInfo pInfo) throws JsParserException {
		if (JsValueTypeDetection.isBooleanValue(pValue)) {
			return parseBooleanValue(pValue);
		}
		if (JsValueTypeDetection.isNumberValue(pValue)) {
			return parseNumberValue(pValue);
		}
		if (JsValueTypeDetection.isStringValue(pValue)) {
			return parseStringValue(pValue);
		}
		// Must check array type before composite value
		if (JsValueTypeDetection.isArrayValue(pValue)) {
			return parseArrayValue(pValue, pInfo);
		}
		// Must check function type before composite type
		if (JsValueTypeDetection.isFunctionValue(pValue)) {
			return parseFunctionValue(pInfo, pValue);
		}
		if (JsValueTypeDetection.isCompositeValue(pValue)) {
			return parseCompositeValue(pValue, pInfo);
		}
		if (JsValueTypeDetection.isVariableValue(pValue, pInfo)) {
			return parseVariableValue(pValue, pInfo);
		}
		throw new JsParserException("Cannot detect value type '" + pValue + "'");
	}
	
	private static IJsValue parseBooleanValue(String pValue) {
		return new JsConstantValue(pValue, IJsValue.TYPE_BOOLEAN);
	}
	
	private static IJsValue parseNumberValue(String pValue) {
		return new JsConstantValue(pValue, IJsValue.TYPE_NUMBER);
	}
	
	private static IJsValue parseStringValue(String pValue) {
		return new JsConstantValue(pValue, IJsValue.TYPE_STRING);
	}
	
	private static IJsValue parseCompositeValue(String pValue, JsCodeBlockInfo pInfo) throws JsParserException {
		String content;
		boolean removedBrackets = false;
		if (pValue.startsWith("(") && pValue.endsWith(")")) {
			content = pValue.substring(1, pValue.length()-1);
			removedBrackets = true;
		} else {
			content = pValue;
		}
		
		List<String> elements = new ArrayList<String>();
		List<String> operators = new ArrayList<String>();
		int elementIndex = 0;
		boolean waitForClose = false;
		int openedElements = 0;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '(') {
				waitForClose = true;
				openedElements++;
				continue;
			}
			if (content.charAt(i) == ')') {
				waitForClose = false;
				openedElements--;
				continue;
			}
			if (openedElements == 0 && !waitForClose) {
				String operator = StringUtils.beginsWithOneOf(content, i, JsOperator.STRING_VALUES);
				if (operator != null) {
					elements.add(content.substring(elementIndex, i));
					operators.add(operator);
					elementIndex = i+operator.length();
					i += operator.length()-1;
				}
			}
		}
		elements.add(content.substring(elementIndex, content.length()));
		
		String lastElement = elements.get(0);
		IJsValue lastValue = parseValue(StringUtils.removeSpaceAndTabOutside(lastElement), pInfo);
		content = content.replace(StringUtils.removeSpaceAndTabOutside(lastElement), lastValue.getValue());
		lastElement = lastValue.getValue();
		
		for (int i=1; i<elements.size(); i++) {
			String element = elements.get(i);
			IJsValue value = parseValue(StringUtils.removeSpaceAndTabOutside(element), pInfo);
			content = content.replace(StringUtils.removeSpaceAndTabOutside(element), value.getValue());
			element = value.getValue();
			
			String operatorString = operators.get(i-1);
			
			try {
				IJsOperator operator = JsOperator.getOperator(operatorString);
				if (i == elements.size()-1) {
					// If it's the last time a composite value is created, the brackets are recreated 
					// if the were removed before
					lastValue = new JsCompositeValue(lastValue, value, operator, removedBrackets);
				} else {
					lastValue = new JsCompositeValue(lastValue, value, operator, false);
				}
			} catch (JsCoreException e) {
				throw new JsParserException("Exception while parsing composite value '" + pValue + "'", e);
			}
			
			lastElement = element;
		}
		return lastValue;
	}
	
	/*
	 * TODO Change first part so that commas inside functions/constructors
	 * are ignored for splitting the array elements.
	 */
	private static IJsValue parseArrayValue(String pValue, JsCodeBlockInfo pInfo) throws JsParserException {
		String content = pValue.substring(1, pValue.length()-1);
		
		List<String> elements = new ArrayList<String>();
		int elementIndex = -1;
		boolean waitForClose = false;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '[') {
				waitForClose = true;
				continue;
			}
			if (content.charAt(i) == ']' && waitForClose) {
				waitForClose = false;
				continue;
			}
			if (!waitForClose) {
				if (content.charAt(i) == ',') {
					elements.add(content.substring(elementIndex+1, i));
					elementIndex = i;
				}
			}
		}
		elements.add(content.substring(elementIndex+1, content.length()));
		
		IJsValue[] arrayElements = new IJsValue[elements.size()];
		
		for (int i=0; i<elements.size(); i++) {
			arrayElements[i] = parseValue(StringUtils.removeSpaceAndTabOutside(elements.get(i)), pInfo);
		}
		return new JsArrayValue(arrayElements);
	}
	
	private static IJsValue parseVariableValue(String pValue, JsCodeBlockInfo pInfo) throws JsParserException {
		try {
			return new JsVariable(pInfo.getVariable(pValue));
		} catch (JsInfoException e) {
			throw new JsParserException("Exception while parsing variable value '" + pValue + "'", e);
		}
	}
	
	private static IJsValue parseFunctionValue(JsCodeBlockInfo pInfo, String pValue) throws JsParserException {
		int firstOpenBracket = StringUtils.firstValidIndexOf(pValue, '(');
		int firstCloseBracket = StringUtils.firstValidIndexOf(pValue, ')');
		String parameterString = pValue.substring(firstOpenBracket+1, firstCloseBracket);
		String[] parameterNames;
		if (parameterString.isEmpty()) {
			parameterNames = new String[0];
		} else {
			parameterNames = parameterString.split(",");
			for (int i=0; i<parameterNames.length; i++) {
				parameterNames[i] = StringUtils.removeSpaceAndTabOutside(parameterNames[i]);
			}
		}
		String contentString = StringUtils.removeSpaceAndTabOutside(
				pValue.substring(StringUtils.firstValidIndexOf(pValue, '{')+1, pValue.length()));
		List<JsReaderContent> content;
		try {
			content = new JsReader(contentString).readContent();
		} catch (JsReaderException e) {
			throw new JsParserException("Exception while reading content of function value", e);
		}
		
		JsFunctionParserHelper helper = new JsFunctionParserHelper(parameterNames, content);
		helper.parse();
		
		JsFunctionValue function = new JsFunctionValue(pInfo.createCodeBlock(),
				helper.getReturnTypes(), helper.getParameters());
		helper.appendContent(function);
		return function;
	}
}
