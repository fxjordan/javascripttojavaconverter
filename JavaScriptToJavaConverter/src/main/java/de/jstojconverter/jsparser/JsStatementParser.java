package de.jstojconverter.jsparser;

import de.jstojconverter.jscore.JsAssignmentStatement;
import de.jstojconverter.jscore.JsCodeBlock;
import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.JsDeclarationStatement;
import de.jstojconverter.jscore.info.JsInfoException;
import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.util.StringUtils;
import de.jstojconverter.jscore.*;

/**
 * @author Felix Jordan
 * @since 01.08.2015 - 17:30:43
 * @version 1.0
 */
public class JsStatementParser {
	
	private JsStatementParser() {
	}
	
	public static JsContent parse(JsCodeBlock pCodeBlock, String pStatement) throws JsParserException {
		if (JsStatementTypeDetection.isDeclarationStatement(pStatement )) {
			return parseDeclarationStatement(pCodeBlock, pStatement);
		} 
		if (JsStatementTypeDetection.isDeclarationWithAssignmentStatement(pStatement)) {
			return parseDeclarationWithAssignmentStatement(pCodeBlock, pStatement);
		} 
		if (JsStatementTypeDetection.isAssignmentStatement(pStatement)) {
			return parseAssignmentStatement(pCodeBlock, pStatement);
		} 
		if (JsStatementTypeDetection.isFunctionInvocationStatementOLD(pStatement)) {
			return parseFunctionInvocationStatement(pCodeBlock, pStatement);
		}
		throw new JsParserException("Cannot detect statement type '" + pStatement + "'");
	}
	
	private static JsDeclarationStatement parseDeclarationStatement(JsCodeBlock pCodeBlock, String pStatement)
			throws JsParserException {
		String declaration = pStatement.substring(4);
		int variableNameEnd = declaration.length() - 2;
		String variableName = declaration.substring(0, variableNameEnd+1);
		variableName = StringUtils.removeSpaceAndTabOutside(variableName);
		
		try {
			return pCodeBlock.createDeclarationStatement(variableName);
		} catch (JsInfoException e) {
			throw new JsParserException("Exception while parsing declaration statement", e);
		}
	}
	
	private static JsDeclarationStatement parseDeclarationWithAssignmentStatement(JsCodeBlock pCodeBlock, String pStatement) throws JsParserException {
		String declaration = pStatement.substring(4);
		int variableNameEnd = declaration.indexOf(' ') - 1;
		String variableName = declaration.substring(0, variableNameEnd+1);
		variableName = StringUtils.removeSpaceAndTabOutside(variableName);
		
		int equalSignIndex = declaration.indexOf('=');
		String valueString = declaration.substring(equalSignIndex + 1, declaration.length()-1);
		valueString = StringUtils.removeSpaceAndTabOutside(valueString);
		IJsValue value = JsValueParser.parseValue(valueString, pCodeBlock.getInfo());
		
		try {
			return pCodeBlock.createDeclarationStatement(variableName, value);
		} catch (JsCoreException e) {
			throw new JsParserException("Exception while parsing declaration statement '" + pStatement + "'", e);
		}
	}
	
	private static JsAssignmentStatement parseAssignmentStatement(JsCodeBlock pCodeBlock, String pStatement) throws JsParserException {
		int variableNameEnd = pStatement.indexOf(' ') - 1;
		String variableName = pStatement.substring(0, variableNameEnd+1);
		
		String operatorString = StringUtils.getFirstValidValue(pStatement, 0, JsAssignmentOperator.stringValues());
		JsAssignmentOperator operator = JsAssignmentOperator.getOperator(operatorString);
		
		int operatorIndex = StringUtils.firstValidIndexOf(pStatement, operatorString);
		int operatorLength = operatorString.length();
		String valueString = pStatement.substring(operatorIndex + operatorLength, pStatement.length()-1);
		IJsValue value = JsValueParser.parseValue(StringUtils.removeSpaceAndTabOutside(valueString), pCodeBlock.getInfo());
		
		try {
			return pCodeBlock.createAssignmentStatement(variableName, operator, value);
		} catch (JsCoreException e) {
			throw new JsParserException("Exception while parsing assignment statement '" + pStatement + "'", e);
		}
	}
	
	private static JsFunctionInvocationStatement parseFunctionInvocationStatement(JsCodeBlock pCodeBlock, String statement) {
		// TODO Auto-generated method stub
		return null;
	}
}
