package de.jstojconverter.jsparser;

/**
 * @author Felix Jordan
 * @since 01.08.2015 - 18:22:13
 * @version 1.0
 */
public class JsParserException extends Exception {
	
	private static final long serialVersionUID = 2288564203215674779L;
	
	public JsParserException() {
	}
	
	public JsParserException(String pMessage) {
		super(pMessage);
	}
	
	public JsParserException(Throwable pThrowable) {
		super(pThrowable);
	}
	
	public JsParserException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
