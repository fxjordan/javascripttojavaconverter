package de.jstojconverter.jsparser;

import de.jstojconverter.jscore.JsCodeBlock;
import de.jstojconverter.jscore.util.JsParameter;
import de.jstojconverter.util.*;
import java.util.*;
import de.jstojconverter.jsreader.*;

/**
 * @author Felix Jordan
 * @since 04.08.2015 - 00:40:26
 * @version 1.0
 */
public class JsFunctionParserHelper {
	
	public static final String LOG_TAG = "JsFunctionParserHelper";
	
	
	
	public JsFunctionParserHelper(String[] pParameterNames, List<JsReaderContent> pContent) {
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Start parsing function with parameters: (");
		for (int i=0; i<pParameterNames.length; i++) {
			messageBuilder.append(pParameterNames[i]);
			if (i < pParameterNames.length - 1) {
				messageBuilder.append(", ");
			}
		}
		messageBuilder.append(") and content:\n");
		messageBuilder.append(pContent);
		Log.d(LOG_TAG, messageBuilder.toString());
	}
	
	/**
	 * Parses the content of the function with the given parameters.
	 * The content, the return types and the parameter sare stored
	 */
	public void parse() {
		// TODO Implement this method
	}
	
	public String[] getReturnTypes() {
		// TODO Implement this method
		return null;
	}
	
	public JsParameter[] getParameters() {
		// TODO Implement this method
		return null;
	}
	
	/**
	 * Appends the parsed content to a {@link JsCodeBlock} (mostly the function).
	 * 
	 * @param pCodeBlock
	 */
	public void appendContent(JsCodeBlock pCodeBlock) {
		// TODO Implement this method
	}
}
