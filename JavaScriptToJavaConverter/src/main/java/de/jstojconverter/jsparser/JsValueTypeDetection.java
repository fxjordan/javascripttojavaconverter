package de.jstojconverter.jsparser;

import de.jstojconverter.jscore.info.JsCodeBlockInfo;
import de.jstojconverter.jscore.operators.JsOperator;
import de.jstojconverter.util.CodeValidation;
import de.jstojconverter.util.StringUtils;

/**
 * @author Felix Jordan
 * @since 03.08.2015 - 18:10:54
 * @version 1.0
 */
public class JsValueTypeDetection {
	
	private JsValueTypeDetection() {
	}
	
	public static boolean isBooleanValue(String pValue) {
		return pValue.equals("true") || pValue.equals("false");
	}
	
	public static boolean isNumberValue(String pValue) {
		String number;
		int dotCount = StringUtils.countCharacter(pValue, '.');
		if (dotCount > 1) {
			return false;
		} else if (dotCount == 1) {
			number = pValue.replace(".", "");
		} else {
			number = pValue;
		}
		int minusSignCount = StringUtils.countCharacter(number, '-');
		if (minusSignCount > 1) {
			return false;
		}
		if (minusSignCount == 1) {
			if (pValue.indexOf('-') != 0) {
				return false;
			}
		}
		number = number.replace("-", "");
		return number.matches("[0-9]+");
	}
	
	public static boolean isStringValue(String pValue) {
		return (pValue.startsWith("\"") && pValue.endsWith("\"")
				|| (pValue.startsWith("'") && pValue.endsWith("'")));
	}
	
	public static boolean isArrayValue(String pValue) {
		return pValue.startsWith("[") && pValue.endsWith("]");
	}
	
	public static boolean isVariableValue(String pVariable, JsCodeBlockInfo pInfo) {
		return pInfo.existVariable(pVariable);
	}
	
	public static boolean isFunctionValue(String pValue) {
		if (!pValue.startsWith("function")) {
			return false;
		}
		if (pValue.contains("{") && pValue.endsWith("}")) {
			return true;
		}
		return false;
	}
	
	// TODO Fix bug that method returns true for arrays and anonymous functions
	public static boolean isCompositeValue(String pValue) {
		if (pValue.startsWith("(") && pValue.endsWith(")")) {
			return true;
		}
		int index = -1;
		while ((index = StringUtils.containsOneOf(pValue, index+1, JsOperator.STRING_VALUES)) != -1) {
			if (CodeValidation.isCodeValid(pValue, index)) {
				return true;
			}
		}
		return false;
	}
}
