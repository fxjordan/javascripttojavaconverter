package de.jstojconverter.old.jsparser;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.jscore.JsCoreException;
import de.jstojconverter.jscore.operators.IJsOperator;
import de.jstojconverter.jscore.operators.JsOperator;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.jscore.value.JsArrayValue;
import de.jstojconverter.jscore.value.JsCompositeValue;
import de.jstojconverter.jscore.value.JsConstantValue;
import de.jstojconverter.util.StringUtils;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 18:12:10
 * @version 1.0
 */
public class JsValueParser {
	
	private JsValueParser() {
	}
	
	public static IJsValue parseValue(String pValue) throws JsParserException {
		if (JsValueTypeDetection.isBooleanValue(pValue)) {
			return parseBooleanValue(pValue);
		}
		if (JsValueTypeDetection.isNumberValue(pValue)) {
			return parseNumberValue(pValue);
		}
		if (JsValueTypeDetection.isStringValue(pValue)) {
			return parseStringValue(pValue);
		}
		// Must check array type before composite value
		if (JsValueTypeDetection.isArrayValue(pValue)) {
			return parseArrayValue(pValue);
		}
		if (JsValueTypeDetection.isCompositeValue(pValue)) {
			try {
				return parseCompositeValue(pValue);
			} catch (JsCoreException e) {
				throw new JsParserException("Failed to parse composite value.", e);
			}
		}
		throw new JsParserException("Cannot detect value type.");
	}
	
	private static IJsValue parseBooleanValue(String pValue) {
		return new JsConstantValue(pValue, IJsValue.TYPE_BOOLEAN);
	}
	
	private static IJsValue parseNumberValue(String pValue) {
		return new JsConstantValue(pValue, IJsValue.TYPE_NUMBER);
	}
	
	private static IJsValue parseStringValue(String pValue) {
		return new JsConstantValue(pValue, IJsValue.TYPE_STRING);
	}
	
	private static IJsValue parseCompositeValue(String pValue) throws JsParserException, JsCoreException {
		String content;
		boolean removedBrackets = false;
		if (pValue.startsWith("(") && pValue.endsWith(")")) {
			content = pValue.substring(1, pValue.length()-1);
			removedBrackets = true;
		} else {
			content = pValue;
		}
		
		List<String> elements = new ArrayList<String>();
		List<String> operators = new ArrayList<String>();
		int elementIndex = 0;
		boolean waitForClose = false;
		int openedElements = 0;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '(') {
				waitForClose = true;
				openedElements++;
				continue;
			}
			if (content.charAt(i) == ')') {
				waitForClose = false;
				openedElements--;
				continue;
			}
			if (openedElements == 0 && !waitForClose) {
				String operator = StringUtils.beginsWithOneOf(content, i, JsOperator.STRING_VALUES);
				if (operator != null) {
					elements.add(content.substring(elementIndex, i));
					operators.add(operator);
					elementIndex = i+operator.length();
					i += operator.length()-1;
				}
			}
		}
		elements.add(content.substring(elementIndex, content.length()));
		
		String lastElement = elements.get(0);
		IJsValue lastValue = parseValue(StringUtils.removeSpaceAndTabOutside(lastElement));
		content = content.replace(StringUtils.removeSpaceAndTabOutside(lastElement), lastValue.getValue());
		lastElement = lastValue.getValue();
		
		for (int i=1; i<elements.size(); i++) {
			String element = elements.get(i);
			IJsValue value = parseValue(StringUtils.removeSpaceAndTabOutside(element));
			content = content.replace(StringUtils.removeSpaceAndTabOutside(element), value.getValue());
			element = value.getValue();
			
			String operatorString = operators.get(i-1);
			
			IJsOperator operator = JsOperator.getOperator(operatorString);
			if (i == elements.size()-1) {
				// If it's the last time a composite value is created, the brackets are recreated 
				// if the were removed before
				lastValue = new JsCompositeValue(lastValue, value, operator, removedBrackets);
			} else {
				lastValue = new JsCompositeValue(lastValue, value, operator, false);
			}
			
			lastElement = element;
		}
		return lastValue;
	}
	
	/*
	 * TODO Change first part so that commas inside functions/constructors
	 * are ignored for splitting the array elements.
	 */
	private static IJsValue parseArrayValue(String pValue) throws JsParserException {
		String content = pValue.substring(1, pValue.length()-1);
		
		List<String> elements = new ArrayList<String>();
		int elementIndex = -1;
		boolean waitForClose = false;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '[') {
				waitForClose = true;
				continue;
			}
			if (content.charAt(i) == ']' && waitForClose) {
				waitForClose = false;
				continue;
			}
			if (!waitForClose) {
				if (content.charAt(i) == ',') {
					elements.add(content.substring(elementIndex+1, i));
					elementIndex = i;
				}
			}
		}
		elements.add(content.substring(elementIndex+1, content.length()));
		
		IJsValue[] arrayElements = new IJsValue[elements.size()];
		
		for (int i=0; i<elements.size(); i++) {
			arrayElements[i] = parseValue(StringUtils.removeSpaceAndTabOutside(elements.get(i)));
		}
		return new JsArrayValue(arrayElements);
	}
}
