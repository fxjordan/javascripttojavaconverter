package de.jstojconverter.old.jsparser;

import de.jstojconverter.javacore.info.JavaCodeBlockInfo;
import de.jstojconverter.jscore.operators.JsOperator;
import de.jstojconverter.util.CodeValidation;
import de.jstojconverter.util.StringUtils;

public class JsValueTypeDetection {

	private JsValueTypeDetection() {
	}
	
	public static boolean isBooleanValue(String pValue) {
		return pValue.equals("true") || pValue.equals("false");
	}
	
	public static boolean isNumberValue(String pValue) {
		String number;
		int dotCount = StringUtils.countCharacter(pValue, '.');
		if (dotCount > 1) {
			return false;
		} else if (dotCount == 1) {
			number = pValue.replace(".", "");
		} else {
			number = pValue;
		}
		int minusSignCount = StringUtils.countCharacter(number, '-');
		if (minusSignCount > 1) {
			return false;
		}
		if (minusSignCount == 1) {
			if (pValue.indexOf('-') != 0) {
				return false;
			}
		}
		number = number.replace("-", "");
		return number.matches("[0-9]+");
	}
	
	public static boolean isStringValue(String pValue) {
		return (pValue.startsWith("\"") && pValue.endsWith("\"")
				|| (pValue.startsWith("'") && pValue.endsWith("'")));
	}
	
	public static boolean isArrayValue(String pValue) {
		return pValue.startsWith("[") && pValue.endsWith("]");
	}
	
	// TODO Implement own info api for JavaScript package
	public static boolean isVariableValue(String pVariable, JavaCodeBlockInfo pInfo) {
		return pInfo.existVariable(pVariable);
	}
	
	// TODO Fix bug that method returns true for arrays
	public static boolean isCompositeValue(String pValue) {
		if (pValue.startsWith("(") && pValue.endsWith(")")) {
			return true;
		}
		int index = -1;
		while ((index = StringUtils.containsOneOf(pValue, index+1, JsOperator.STRING_VALUES)) != -1) {
			if (CodeValidation.isCodeValid(pValue, index)) {
				return true;
			}
		}
		return false;
	}
}
