package de.jstojconverter.old.jsparser;

import java.util.ArrayList;
import java.util.List;

import de.jstojconverter.jscore.JsContent;
import de.jstojconverter.jscore.JsFile;
import de.jstojconverter.jsreader.JsReaderContent;

/**
 * @author Felix Jordan
 * @since 21.05.2015 - 16:54:59
 * @version 1.0
 */
public class JsParser {
	
	private List<JsReaderContent> mContentList;
	private JsFile mJsFile;
	private List<JsReaderContent> mFunctionDeclarations;
	
	public JsParser(List<JsReaderContent> pReaderContent) {
		mContentList = pReaderContent;
//		mFunctionDeclarations = getAllFunctionDeclarations(pReaderContent);
		mJsFile = JsFile.create();
	}
	
	public void parse() {
		for (JsReaderContent content : mContentList) {
			switch (content.getType()) {
			case JsReaderContent.TYPE_STATEMENT:
				break;

			default:
				break;
			}
		}
	}
	
	private List<JsReaderContent> getAllFunctionDeclarations(List<JsReaderContent> pReaderContent) {
		List<JsReaderContent> functions = new ArrayList<>();
		for (JsReaderContent content : pReaderContent) {
			if (content.getType() == JsReaderContent.TYPE_FUNCTION_DECLARATION) {
				functions.add(content);
			}
			if (content.isContainer()) {
				functions.addAll(getAllFunctionDeclarations(content.getContent()));
			}
		}
		return functions;
	}
	
	private JsContent parseStatement(String pStatement) throws JsParserException {
		if (JsStatementTypeDetection.isDeclarationStatement(pStatement)) {
			
		} 
		if (JsStatementTypeDetection.isDeclarationWithAssignmentStatement(pStatement)) {
			
		} 
		if (JsStatementTypeDetection.isAssignmentStatement(pStatement)) {
			
		} 
		if (JsStatementTypeDetection.isFunctionInvocationStatement(pStatement)) {
			
		} 
		throw new JsParserException("Cannot detect type of statement");
	}
}
