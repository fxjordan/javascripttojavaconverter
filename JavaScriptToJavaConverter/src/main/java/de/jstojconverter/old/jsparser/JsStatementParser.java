package de.jstojconverter.old.jsparser;

import de.jstojconverter.jscore.JsAssignmentStatement;
import de.jstojconverter.jscore.JsContent;
import de.jstojconverter.jscore.JsDeclarationStatement;
import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.jscore.value.IJsValue;
import de.jstojconverter.util.StringUtils;

/**
 * @author Felix Jordan
 * @since 22.05.2015 - 22:35:10
 * @version 1.0
 */
public class JsStatementParser {
	
	public static JsContent parseStatement(String pStatement) throws JsParserException {
		if (JsStatementTypeDetection.isDeclarationStatement(pStatement)) {
			return parseDeclarationStatement(pStatement);
		} 
		if (JsStatementTypeDetection.isDeclarationWithAssignmentStatement(pStatement)) {
			return parseDeclarationWithAssignmentStatement(pStatement);
		} 
		if (JsStatementTypeDetection.isAssignmentStatement(pStatement)) {
			return parseAssignmentStatement(pStatement);
		} 
		if (JsStatementTypeDetection.isFunctionInvocationStatement(pStatement)) {
			
		}
		throw new JsParserException("Cannot detect type of statement");
	}
	
	private static JsDeclarationStatement parseDeclarationStatement(String pStatement) {
		String declaration = pStatement.substring(4);
		int variableNameEnd = declaration.length() - 2;
		String variableName = declaration.substring(0, variableNameEnd+1);
		variableName = StringUtils.removeSpaceAndTabOutside(variableName);
		//return new JsDeclarationStatement(variableName);
		return null;
	}
	
	private static JsDeclarationStatement parseDeclarationWithAssignmentStatement(String pStatement)
			throws JsParserException {
		String declaration = pStatement.substring(4);
		int variableNameEnd = declaration.indexOf(' ') - 1;
		String variableName = declaration.substring(0, variableNameEnd+1);
		variableName = StringUtils.removeSpaceAndTabOutside(variableName);
		
		int equalSignIndex = declaration.indexOf('=');
		String valueString = declaration.substring(equalSignIndex + 1, declaration.length()-1);
		valueString = StringUtils.removeSpaceAndTabOutside(valueString);
		IJsValue value = JsValueParser.parseValue(valueString);
		
		//return new JsDeclarationStatement(variableName, value);
		return null;
	}
	
	private static JsAssignmentStatement parseAssignmentStatement(String pStatement) throws JsParserException {
		int variableNameEnd = pStatement.indexOf(' ') - 1;
		String variableName = pStatement.substring(0, variableNameEnd+1);
		
		String operatorString = StringUtils.getFirstValidValue(pStatement, 0, JsAssignmentOperator.stringValues());
		JsAssignmentOperator operator = JsAssignmentOperator.getOperator(operatorString);
		
		int operatorIndex = StringUtils.firstValidIndexOf(pStatement, operatorString);
		int operatorLength = operatorString.length();
		String valueString = pStatement.substring(operatorIndex + operatorLength, pStatement.length()-1);
		IJsValue value = JsValueParser.parseValue(StringUtils.removeSpaceAndTabOutside(valueString));
		//return new JsAssignmentStatement(variableName, operator, value);
		return null;
	}
}
