package de.jstojconverter.old.jsparser;

import de.jstojconverter.jscore.operators.JsAssignmentOperator;
import de.jstojconverter.util.CodeValidation;
import de.jstojconverter.util.StringUtils;

public class JsStatementTypeDetection {

	private JsStatementTypeDetection() {
	}
	
	public static boolean isDeclarationStatement(String pStatement) {
		if (!pStatement.startsWith("var") || !pStatement.endsWith(";")) {
			return false;
		}
		int equalsSignIndex = pStatement.indexOf('=');
		if (equalsSignIndex == -1) {
			return true;
		}
		if (CodeValidation.isCodeValid(pStatement, equalsSignIndex)) {
			return false;
		}
		return true;
	}
	
	public static boolean isDeclarationWithAssignmentStatement(String pStatement) {
		if (!pStatement.startsWith("var") || !pStatement.endsWith(";")) {
			return false;
		}
		int equalsSignIndex = pStatement.indexOf('=');
		if (equalsSignIndex == -1) {
			return false;
		}
		if (!CodeValidation.isCodeValid(pStatement, equalsSignIndex)) {
			return false;
		}
		return true;
	}
	
	public static boolean isAssignmentStatement(String pStatement) {
		if (pStatement.startsWith("var")) {
			return false;
		}
		// Second element must be assignment operator
		String[] parts = pStatement.split(" ");
		if (parts.length < 2) {
			return false;
		}
		String operator = StringUtils.removeSpaceAndTabOutside(parts[1]);
		if (!JsAssignmentOperator.isOperator(operator)) {
			return false;
		}
		return pStatement.endsWith(";");
	}
	
	@Deprecated
	public static boolean isFunctionInvocationStatementOLD(String pStatement) {
		int firstBracket = StringUtils.firstValidIndexOf(pStatement, '(');
		if (firstBracket == -1) {
			return false;
		}
		String firstPart = pStatement.substring(0, firstBracket);
		// All letters and dots
		if (!firstPart.matches("[a-zA-Z/.]+")) {
			return false;
		}
		return pStatement.endsWith(");");
	}
	
	public static boolean isFunctionInvocationStatement(String pStatement) {
		int lastBracket = StringUtils.lastValidIndexOf(pStatement, ')');
		if (lastBracket == -1) {
			return false;
		}
		/*
		 * TODO Implement new method for detecting function invocation statements and
		 *      differ from statements like in the JSStatementTypeDetectionTest class
		 */
		
		return false;
	}
}
