package de.jstojconverter.old;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class ConverterTest {

	public static void main(String[] pArgs) {
		new ConverterTest();
	}
	
	private Map<String, String> mVariableTypes;
	
	public ConverterTest() {
		mVariableTypes = new HashMap<String, String>();
		
		String jsStatement = "var fooVariable = \"foo\";";
		String javaStatement = convertStatement(jsStatement);
		String javaStatement2 = convertStatement("var booVar = \"boo\" + fooVariable;");
		System.out.println(javaStatement);
		System.out.println(javaStatement2);
	}
	
	
	private String convertStatement(String pStatement) {
		if (pStatement.startsWith("var") && pStatement.endsWith(";")) {
			return convertDeclarationStatement(pStatement);
		}
		throw new RuntimeException("Cannot convert statement '" + pStatement + "'");
	}
	
	private String convertDeclarationStatement(String pStatement) {
		System.out.println("Converting declaration stement");
		// String without 'var' declaration
		String declaration = pStatement.substring(4);
		int nameEndIndex = declaration.indexOf(' ')-1;
		String variableName = declaration.substring(0, nameEndIndex+1);
		System.out.println("Variable name: " + variableName);
		int equalsSignIndex = declaration.indexOf('=');
		int firstAssignIndex = equalsSignIndex+1;
		while (declaration.charAt(firstAssignIndex) == ' ') {
			firstAssignIndex++;
		}
		int statementEndIndex = declaration.indexOf(';');
		int lastAssignIndex = statementEndIndex-1;
		while (declaration.charAt(lastAssignIndex) == ' ') {
			lastAssignIndex--;
		}
		String assignmentValue = declaration.substring(firstAssignIndex, lastAssignIndex+1);
		System.out.println("Assignment value: " + assignmentValue);
		String variableType = VariableTypeUtils.getVariableType(assignmentValue, mVariableTypes);
		System.out.println("Variable type: " + variableType);
		mVariableTypes.put(variableName, variableType);
		return variableType + " " + variableName + " = " + assignmentValue + ";";
	}
	
	

	/*private void runClass(Class pClass) {
		File classpath = new File("e:/EclipseJavaEE/workspace/JavaScriptToJavaConverter/jstoj-output/bin/");
		
		try {
			String command = "cmd /c start \"" + pClass.getName() + "\" java -classpath " + classpath.getAbsolutePath() + " " + pClass.getName();
			ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
			processBuilder.redirectErrorStream(true);
			Process process = processBuilder.start();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void compileClass(Class pClass) throws IOException {
		File srcFolder = new File("e:/intelligent-code-generator/testprogramm/src");
		File binFolder = new File("e:/intelligent-code-generator/testprogramm/bin");
		if (!binFolder.exists()) {
			binFolder.mkdir();
		}
		
		File javaFile = new File(srcFolder, pClass.getName() + ".java");
		
		String command = "javac " + "-d " + binFolder + " " + javaFile.getAbsolutePath();
		ProcessBuilder processBuilder = new ProcessBuilder(command.split(" "));
		processBuilder.redirectErrorStream(true);
		Process process = processBuilder.start();
	}
	
	private void writeClassToFile(Class pClass) throws IOException {
		
		File folder = new File("e:/intelligent-code-generator/testprogramm/src");
		if (!folder.exists()) {
			folder.mkdirs();
		}
		
		File javaFile = new File(folder, pClass.getName() + ".java");
		if (!javaFile.exists()) {
			javaFile.createNewFile();
		}
		
		FileWriter fileWriter = new FileWriter(javaFile);
		fileWriter.write(pClass.getPrintableText());
		fileWriter.flush();
		fileWriter.close();
	}*/
}
