package de.jstojconverter.old;

import java.util.ArrayList;
import java.util.List;

public class Method {
	
	private String mName;
	private List<String> mStatements;
	
	public Method(String pName) {
		mName = pName;
		mStatements = new ArrayList<String>();
	}
	
	public void addStatements(List<String> pStatements) {
		mStatements.addAll(pStatements);
	}
	
	public void addStatement(String pStatement) {
		mStatements.add(pStatement);
	}
}
