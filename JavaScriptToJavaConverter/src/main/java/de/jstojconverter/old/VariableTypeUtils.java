package de.jstojconverter.old;

import java.util.Iterator;
import java.util.Map;

public class VariableTypeUtils {
	
	private VariableTypeUtils() {
		// TODO Auto-generated constructor stub
	}
	
	public static String getVariableType(String pVariableValue, Map<String, String> pVariableTypes) {
		String value = insertVariableTypes(pVariableValue, pVariableTypes);
		
		
		if (isDirectBoolean(value)) {
			return "boolean";
		}
		if (isDirectInteger(value)) {
			return "int";
		}
		if (isDirectDouble(value)) {
			return "double";
		}
		if (isDirectHexdecimal(value)) {
			return "/*hex*/int";
		}
		if (StringTypeDetector.isStringType(value)) {
			return "String";
		}
		throw new RuntimeException("Cannot find Variable type for value: (" + pVariableValue + ")");
	}
	
	private static String insertVariableTypes(String pVariableValue, Map<String, String> pVariableTypes) {
		Iterator<String> variableIterator = pVariableTypes.keySet().iterator();
		String variableValue = pVariableValue;
		while (variableIterator.hasNext()) {
			String variable = variableIterator.next();
			int variableIndex = variableValue.indexOf(variable);
			if (variableIndex == -1) {
				continue;
			}
			int quoteMarksBefore = 0;
			int lastIndex = -1;
			while ((lastIndex = variableValue.indexOf('"', lastIndex+1)) != -1 && lastIndex < variableIndex) {
				quoteMarksBefore++;
			}
			if (quoteMarksBefore % 2 == 1) {
				// Variable name is inside a string
				continue;
			}
			// Replace variable with a primitive type so that
			variableValue = variableValue.replace(variable, getReplacementForVariableType(pVariableTypes.get(variable)));
			System.out.println(variableValue);
		}
		return variableValue;
	}

	private static String getReplacementForVariableType(String pType) {
		switch (pType) {
		case "String":
			return "\"\"";
		case "int":
			return "42";
		case "double":
			return "42.21";
		default:
			return pType;
		}
	}

	private static boolean isDirectHexdecimal(String pValue) {
		if (pValue.startsWith("0x")) {
			return !pValue.substring(2).matches("[a-zA-Z]+");
		}
		return false;
	}
	
	private static boolean isDirectDouble(String pValue) {
		int dotCount = 0;
		int dotIndex = -1;
		while ((dotIndex = pValue.indexOf('.', dotIndex+1)) != -1) {
			dotCount++;
		}
		if (dotCount != 1) {
			return false;
		}
		return pValue.replace(".", "").matches("[0-9]+");
	}
	
	private static boolean isDirectInteger(String pValue) {
		return pValue.matches("[0-9]+");
	}
	
	private static boolean isDirectBoolean(String pValue) {
		return pValue.equals("true") || pValue.equals("false");
	}
	
	private static boolean hasOnlyDirectNumbers(String pValue) {
		if (pValue.matches("[a-zA-Z]+")) {
			if (pValue.endsWith("f")) {
				return hasOnlyDirectNumbers(pValue.substring(0, pValue.length()-1));
			}
			if (pValue.endsWith("d")) {
				return hasOnlyDirectNumbers(pValue.substring(0, pValue.length()-1));
			}
		}
		return true;
	}
}
