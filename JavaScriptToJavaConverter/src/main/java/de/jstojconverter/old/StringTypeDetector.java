package de.jstojconverter.old;

import java.util.Map;

import de.jstojconverter.util.StringUtils;

public class StringTypeDetector {

	private StringTypeDetector() {
	}
	
	public static boolean isStringType(String pValue) {
		if (isDirectComposedString(pValue)) {
			return true;
		}
		return false;
	}
	
	private static boolean isDirectComposedString(String pValue) {
		String[] parts = pValue.split("\\+");
		for (String part : parts) {
			if (!isDirectSingleString(part)) {
				return false;
			}
		}
		return true;
	}

	private static boolean isDirectSingleString(String pValue) {
		String value = StringUtils.removeSpaceAndTabOutside(pValue);
		int quoteMarkCount = 0;
		int quoteMarkIndex = -1;
		while ((quoteMarkIndex = value.indexOf('"', quoteMarkIndex+1)) != -1) {
			quoteMarkCount++;
		}
		if (quoteMarkCount == 2) {
			return value.startsWith("\"") && value.endsWith("\"");
		}
		return false;
	}
}
