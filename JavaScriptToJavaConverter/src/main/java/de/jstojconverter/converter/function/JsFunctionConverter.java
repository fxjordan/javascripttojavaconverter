package de.jstojconverter.converter.function;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.jstojconverter.ConverterException;
import de.jstojconverter.TypeDetection;
import de.jstojconverter.javacore.JavaClass;
import de.jstojconverter.javacore.JavaMethod;
import de.jstojconverter.javacore.operators.JavaOperator;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.util.StringUtils;
import de.jstojconverter.javacore.info.*;
import de.jstojconverter.converter.*;
import de.jstojconverter.javacore.*;

public class JsFunctionConverter {
	
	private Map<String, JsReaderContent> mFunctionData;
	
	public JsFunctionConverter() {
		mFunctionData = new HashMap<String, JsReaderContent>();
	}
	
	public void addJsFunction(JsReaderContent pJsContent) {
		String functionName = getFunctionName(pJsContent.getText());
		if (mFunctionData.containsKey(functionName)) {
			throw new IllegalArgumentException("The function '" + functionName + "' already exists!");
		}
		mFunctionData.put(functionName, pJsContent);
	}
	
	public JavaMethod convertMethod(JavaClass pClass, String pName, String... pParameterTypes) throws ConverterException, JavaCoreException {
		if (!mFunctionData.containsKey(pName)) {
			throw new IllegalArgumentException("The JavaScript function '" + pName + "' does not exist!");
		}
		JsReaderContent function = mFunctionData.get(pName);
		List<String> parameterNames = getParameterNames(function.getText());
		String[] jsParameters = new String[parameterNames.size()];
		parameterNames.toArray(jsParameters);
		
		function = replaceParameterNamesWithArguments(parameterNames, jsParameters, function);
		System.out.println(function.getText());
		System.out.println("content: " + function.getContent().get(0).getText());
		
		// 2. Compare java method invocation parameter count with JavaScript parameter count
		if (jsParameters.length > pParameterTypes.length) {
			while (parameterNames.size() != pParameterTypes.length) {
				parameterNames.remove(parameterNames.size()-1);
			}
		} else if (jsParameters.length < pParameterTypes.length) {
			for (int i=0; i<pParameterTypes.length; i++) {
				if (i < parameterNames.size()) {
					continue;
				}
				parameterNames.add("pArg" + i);
			}
		}
		StringBuilder parameterString = new StringBuilder();
		for (int i=0; i<parameterNames.size(); i++) {
			parameterString.append(parameterNames.get(i));
			if (i < parameterNames.size()-1) {
				parameterString.append(", ");
			}
		}
		String functionHead = "function " + pName + "(" + parameterString + ")";
		function = changeJSContentText(function, functionHead );
		System.out.println(function.getText());
		System.out.println("content: " + function.getContent().get(0).getText());
		
		// 5. Replace test conditions, wheather parameters exist
		function = replaceTestConditions(function, parameterNames);
		
		// TODO 6. Replace arguments.length with number of parameters
		// TODO 7. Remove dead code
		// TODO 8. Create array for parameters if it is needed
		
		// 9. Replace all arguments[n] calls with the parameter names
		function = replaceArgumentsWithParameterNames(function, jsParameters);
		
		
		// 11. Run normal converter for all function content
		JavaParameter[] parameters = new JavaParameter[parameterNames.size()];
		for (int i=0; i<parameters.length; i++) {
			parameters[i] = new JavaParameter(parameterNames.get(i), pParameterTypes[i]);
		}
		return normalFunctionConverting(pClass, pName, parameters , function.getContent());
	}
	
	private JsReaderContent replaceTestConditions(JsReaderContent pJsContent, List<String> pParameterNames) throws ConverterException {
		JsReaderContent newJSContent;
		if (pJsContent.getType() == JsReaderContent.TYPE_IF_CONDITION) {
			// Remove conditions
			String text = StringUtils.removeSpaceAndTabOutside(pJsContent.getText().substring(2));
			String condition = StringUtils.removeSpaceAndTabOutside(text.substring(1, text.length()-1));
			System.out.println("condition: " + condition);
			String newCondition = replaceArgumentExistConditions(condition, pParameterNames);
			System.out.println("new condition: " + newCondition);
			newJSContent = new JsReaderContent("if (" + newCondition + ")", pJsContent.getType());
		} else {
			newJSContent = new JsReaderContent(pJsContent.getText(), pJsContent.getType());
		}
		if (pJsContent.isContainer()) {
			List<JsReaderContent> contendList = pJsContent.getContent();
			for (JsReaderContent content : contendList) {
				newJSContent.addContent(replaceTestConditions(content, pParameterNames));
			}
		}
		return newJSContent;
	}
	
	private JsReaderContent replaceArgumentsWithParameterNames(JsReaderContent pJsContent, String[] pParameterNames) {
		JsReaderContent newJsContent;
		if (pJsContent.getType() != JsReaderContent.TYPE_FUNCTION_DECLARATION) {
			String text = pJsContent.getText();
			for (int i=0; i<pParameterNames.length; i++) {
				String argumentsCall = "arguments[" + i + "]";
				int argumentIndex = 0;
				while ((argumentIndex = StringUtils.firstValidIndexOf(text, argumentsCall)) != -1) {
					String before = text.substring(0, argumentIndex);
					String after = text.substring(argumentIndex + argumentsCall.length());
					text = before + pParameterNames[i] + after;
				}
			}
			newJsContent = new JsReaderContent(text, pJsContent.getType());
		} else {
			newJsContent = new JsReaderContent(pJsContent.getText(), JsReaderContent.TYPE_FUNCTION_DECLARATION);
		}
		if (pJsContent.isContainer()) {
			List<JsReaderContent> contentList = pJsContent.getContent();
			for (int i=0; i<contentList.size(); i++) {
				JsReaderContent content = contentList.get(i);
				if (pJsContent.isContainer()) {
					newJsContent.addContent(replaceArgumentsWithParameterNames(content, pParameterNames));
				}
			}
		}
		return newJsContent;
	}

	private JsReaderContent replaceParameterNamesWithArguments(List<String> pParameters, String[] pJsParameters, JsReaderContent pJsContent) {
		JsReaderContent newJsContent;
		if (pJsContent.getType() != JsReaderContent.TYPE_FUNCTION_DECLARATION) {
			String text = pJsContent.getText();
			String parameterName;
			int parameterIndex = 0;
			while ((parameterName = StringUtils.getFirstValidValue(text, 0, pJsParameters)) != null) {
				parameterIndex = StringUtils.firstValidIndexOf(text, parameterName);
				String before = text.substring(0, parameterIndex);
				String after = text.substring(parameterIndex + parameterName.length());
				int parameterPosition = pParameters.indexOf(parameterName);
				text = before + "arguments[" + parameterPosition + "]" + after;
			}
			newJsContent = new JsReaderContent(text, pJsContent.getType());
		} else {
			newJsContent = new JsReaderContent(pJsContent.getText(), JsReaderContent.TYPE_FUNCTION_DECLARATION);
		}
		if (pJsContent.isContainer()) {
			List<JsReaderContent> contentList = pJsContent.getContent();
			for (int i=0; i<contentList.size(); i++) {
				JsReaderContent content = contentList.get(i);
				if (pJsContent.isContainer()) {
					newJsContent.addContent(replaceParameterNamesWithArguments(pParameters, pJsParameters, content));
				}
			}
		}
		return newJsContent;
	}
	
	private JsReaderContent changeJSContentText(JsReaderContent pContent, String pHead) {
		JsReaderContent newContent = new JsReaderContent(pHead, pContent.getType());
		for (JsReaderContent content : pContent.getContent()) {
			newContent.addContent(content);
		}
		return newContent;
	}
	
	private String getFunctionName(String pFunctionHead) {
		int bracketIndex = pFunctionHead.indexOf('(');
		String withoutPaameters = StringUtils.removeSpaceAndTabOutside(pFunctionHead.substring(0, bracketIndex));
		int lastSpaceIndex = withoutPaameters.lastIndexOf(' ');
		return withoutPaameters.substring(lastSpaceIndex+1);
	}
	
	private List<String> getParameterNames(String pFunctionHead) {
		int bracketIndex = pFunctionHead.indexOf('(');
		String parameterString = StringUtils.removeSpaceAndTabOutside(pFunctionHead.substring(bracketIndex+1,
				pFunctionHead.length()-1));
		String[] parameters = parameterString.split("\\,");
		List<String> parameterNames = new ArrayList<String>();
		if (parameters[0].isEmpty()) {
			return parameterNames;
		}
		for (int i=0; i<parameters.length; i++) {
			parameterNames.add(StringUtils.removeSpaceAndTabOutside(parameters[i]));
		}
		return parameterNames;
	}
	
	/**
	 * Converts a JavaScript function into a Java method.<br>
	 * <br>
	 * The function must be complete cleared from JavaScript specific things, like use
	 * of the {@code arguments} object
	 * 
	 * @param pClass
	 * @param pJsFunction
	 * @return
	 */
	private JavaMethod normalFunctionConverting(JavaClass pClass, String pName, JavaParameter[] pParameters,
			List<JsReaderContent> pFunctionContent) throws JavaCoreException, ConverterException {
		JavaMethod method = pClass.createMethod("public", null, pName, "void", pParameters);
		JavaCodeBlockEditor methodEditor = new JavaCodeBlockEditor(method);
		for (JsReaderContent content : pFunctionContent) {
			methodEditor.addJsContent(content);
		}
		return method;
	}
	
	private static String getOptimizedCondition(String pCondition, List<String> pParameterNames) throws ConverterException {
		if (TypeDetection.isCompositeValue(pCondition)) {
			return replaceArgumentExistConditions(pCondition, pParameterNames);
		}
		return pCondition;
	}
	
	private static String replaceArgumentExistConditions(String pCondition, List<String> pParameterNames) throws ConverterException {
		String content;
		boolean removedBrackets = false;
		if (pCondition.startsWith("(") && pCondition.endsWith(")")) {
			content = pCondition.substring(1, pCondition.length()-1);
			removedBrackets = true;
		} else {
			content = pCondition;
		}
		//String type = null;
		
		List<String> elements = new ArrayList<String>();
		List<String> operators = new ArrayList<String>();
		int elementIndex = 0;
		boolean waitForClose = false;
		int openedElements = 0;
		for (int i=0; i<content.length(); i++) {
			if (content.charAt(i) == '(') {
				waitForClose = true;
				openedElements++;
				continue;
			}
			if (content.charAt(i) == ')') {
				waitForClose = false;
				openedElements--;
				continue;
			}
			if (openedElements == 0 && !waitForClose) {
				String operator = StringUtils.beginsWithOneOf(content, i, JavaOperator.STRING_VALUES);
				if (operator != null) {
					elements.add(content.substring(elementIndex, i));
					operators.add(operator);
					elementIndex = i+operator.length();
					i += operator.length()-1;
				}
			}
		}
		elements.add(content.substring(elementIndex, content.length()));
		
		String lastElement = elements.get(0);
		String lastValue = getOptimizedCondition(StringUtils.removeSpaceAndTabOutside(lastElement), pParameterNames);
		content = content.replace(StringUtils.removeSpaceAndTabOutside(lastElement), lastValue);
		lastElement = lastValue;
		
		for (int i=1; i<elements.size(); i++) {
			String element = elements.get(i);
			String value = getOptimizedCondition(StringUtils.removeSpaceAndTabOutside(element), pParameterNames);
			content = content.replace(StringUtils.removeSpaceAndTabOutside(element), value);
			element = value;
			
			String operatorString = operators.get(i-1);
			
			if ((lastValue.startsWith("arguments[") || value.startsWith("arguments[")) && 
					(lastValue.equals("undefined") || value.equals("undefined"))) {
				String indexString = lastValue.substring(10, lastValue.length()-1);
				System.out.println("arguemnt index: " + indexString);
				try {
					int index = Integer.parseInt(StringUtils.removeSpaceAndTabOutside(indexString));
					boolean exist = index < pParameterNames.size();
					System.out.println("argument with index " + index + (exist ? " exists" : " does not exist"));
					if (operatorString.equals("==") || operatorString.equals("===")) {
						lastValue = Boolean.toString(!exist);
					} else if (operatorString.equals("!=") || operatorString.equals("!==")) {
						lastValue = Boolean.toString(exist);
					}
				} catch (NumberFormatException e) {
					lastValue = lastValue + " " + operatorString + " " + value;
				}
			} else {
				lastValue = lastValue + " " + operatorString + " " + value;
			}
				
			lastElement = element;
		}
		if (removedBrackets) {
			return "(" + lastValue + ")";
		}
		return lastValue;
	}
}
