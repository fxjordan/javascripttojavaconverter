package de.jstojconverter.converter.value;

import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaConstantValue;

public class SpecialValueConverter {
	
	private SpecialValueConverter() {
	}
	
	public static IJavaValue convertValue(String pValue) {
		if (pValue.equals("Number.MAX_VALUE")) {
			return new JavaConstantValue("Double.MAX_VALUE", "double");
		}
		if (pValue.equals("Number.MIN_VALUE")) {
			return new JavaConstantValue("Double.MIN_VALUE", "double");
		}
		if (pValue.equals("Number.NaN")) {
			return new JavaConstantValue("Double.NaN", "double");
		}
		if (pValue.equals("Number.POSITIVE_INFINITY")) {
			return new JavaConstantValue("Double.POSITIVE_INFINITY", "double");
		}
		if (pValue.equals("Number.NEGATIVE_INFINITY")) {
			return new JavaConstantValue("Double.NEGATIVE_INFINITY", "double");
		}
		return null;
	}
}
