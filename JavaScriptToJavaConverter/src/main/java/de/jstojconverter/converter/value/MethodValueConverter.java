package de.jstojconverter.converter.value;

import de.jstojconverter.ConverterException;
import de.jstojconverter.TypeDeclarationChanger;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.operators.JavaComparisonOperator;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaComparison;
import de.jstojconverter.javacore.value.JavaMethodInvocation;
import de.jstojconverter.javacore.value.JavaConstantValue;
import de.jstojconverter.javacore.info.*;
import de.jstojconverter.javacore.operators.*;
import de.jstojconverter.javacore.*;

public class MethodValueConverter {

	private MethodValueConverter() {
	}

	public static IJavaValue convertMethod(JavaCodeBlock pCodeBlock, String pValue) throws ConverterException, JavaCoreException {
		// Replace special java script methods
		IJavaValue methodInvocation = convertSpecialCases(pCodeBlock, pValue);
		if (methodInvocation != null) {
			return methodInvocation;
		}
		throw new RuntimeException("Only pre defined javascript functions are supported yet!");
	}
	
	private static IJavaValue convertSpecialCases(JavaCodeBlock pCodeBlock, String pStatement) throws ConverterException, JavaCoreException {
		if (pStatement.startsWith("console.log(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(12, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("java.lang.System.out", "println", value);
		}
		if (pStatement.startsWith("window.alert(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(13, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showMessageDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("window.confirm(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(15, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			
			/*
			 * TODO create own class for method invocation statement converter, because method invocation statements need the
			 * 'JavaMethodInvocation' object, but of a method is converted as a value it can be for example
			 * a 'JavaCOmparison' object.
			 * 
			 * Second possibility: Use js wrapper classes for the methods
			 * 
			 */
			
			JavaMethodInvocation methodInvocation = pCodeBlock.createMethodInvocation("javax.swing.JOptionPane",
					"showConfirmDialog", JavaConstantValue.NULL, value);
			return new JavaComparison(methodInvocation, new JavaConstantValue("0", "int"), JavaComparisonOperator.EQUAL);
		}
		if (pStatement.startsWith("window.prompt(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(14, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showInputDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("alert(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(6, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showMessageDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("confirm(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(8, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			JavaMethodInvocation methodInvocation = pCodeBlock.createMethodInvocation("javax.swing.JOptionPane",
					"showConfirmDialog", JavaConstantValue.NULL, value);
			return new JavaComparison(methodInvocation, new JavaConstantValue("0", "int"), JavaComparisonOperator.EQUAL);
		}
		if (pStatement.startsWith("prompt(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(7, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showInputDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("parseInt(") && pStatement.endsWith(")")) {
			String valueString = pStatement.substring(9, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(valueString, pCodeBlock);
			return pCodeBlock.createMethodInvocation("de.jstojconverter.jswrapper.Global", "parseInt", value);
		}
		if (pStatement.startsWith("parseFloat(") && pStatement.endsWith(")")) {
			String valueString = pStatement.substring(11, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(valueString, pCodeBlock);
			return pCodeBlock.createMethodInvocation("de.jstojconverter.jswrapper.Global", "parseFloat", value);
		}
		if (pStatement.startsWith("isNaN(") && pStatement.endsWith(")")) {
			String valueString = pStatement.substring(6, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(valueString, pCodeBlock);
			return pCodeBlock.createMethodInvocation("java.lang.Double", "isNaN", value);
		}
		return null;
	}
}
