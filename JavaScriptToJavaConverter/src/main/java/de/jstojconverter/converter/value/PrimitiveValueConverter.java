package de.jstojconverter.converter.value;

import de.jstojconverter.javacore.value.JavaConstantValue;

public class PrimitiveValueConverter {

	private PrimitiveValueConverter() {
	}
	
	public static JavaConstantValue convertBooleanValue(String pValue) {
		return new JavaConstantValue(pValue, "boolean");
	}
	
	public static JavaConstantValue convertIntegerValue(String pValue) {
		return new JavaConstantValue(pValue, "int");
	}
	
	public static JavaConstantValue convertDoubleValue(String pValue) {
		return new JavaConstantValue(pValue + "d", "double");
	}
}
