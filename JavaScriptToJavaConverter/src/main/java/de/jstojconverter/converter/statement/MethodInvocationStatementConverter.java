package de.jstojconverter.converter.statement;

import de.jstojconverter.ConverterException;
import de.jstojconverter.TypeDeclarationChanger;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.javacore.value.JavaMethodInvocation;
import de.jstojconverter.javacore.value.JavaConstantValue;
import de.jstojconverter.javacore.*;

public class MethodInvocationStatementConverter {

	private MethodInvocationStatementConverter() {
	}
	
	public static void convertMethodInvocationStatement(JavaCodeBlock pCodeBlock, String pStatement) throws JavaCoreException, ConverterException  {
		// Replace special java script methods
		String value = pStatement.substring(0, pStatement.length()-1);
		JavaMethodInvocation methodInvocation = convertSpecialCases(pCodeBlock, value);
		if (methodInvocation != null) {
			// Detected special case
			pCodeBlock.createMethodInvocationStatement(methodInvocation);
			return;
		}
		
		throw new ConverterException("Connot handle normal method invications at the moment");
		// TODO Handle 'normal' methods
	}
	
	private static JavaMethodInvocation convertSpecialCases(JavaCodeBlock pCodeBlock, String pStatement) throws ConverterException, JavaCoreException {
		if (pStatement.startsWith("console.log(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(12, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("java.lang.System.out", "println", value);
		}
		if (pStatement.startsWith("window.alert(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(13, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showMessageDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("window.confirm(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(15, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showConfirmDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("window.prompt(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(14, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showInputDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("alert(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(6, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showMessageDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("confirm(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(8, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showConfirmDialog", JavaConstantValue.NULL, value);
		}
		if (pStatement.startsWith("prompt(") && pStatement.endsWith(")")) {
			String message = pStatement.substring(7, pStatement.length()-1);
			IJavaValue value = TypeDeclarationChanger.getValueObject(message, pCodeBlock);
			return pCodeBlock.createMethodInvocation("javax.swing.JOptionPane", "showInputDialog", JavaConstantValue.NULL, value);
		}
		return null;
	}
}
