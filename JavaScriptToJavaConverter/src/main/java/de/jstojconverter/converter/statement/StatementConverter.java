package de.jstojconverter.converter.statement;

import de.jstojconverter.ConverterException;
import de.jstojconverter.TypeDeclarationChanger;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.info.JavaVariableInfo;
import de.jstojconverter.javacore.operators.JavaAssignmentOperator;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.old.jsparser.JsStatementTypeDetection;
import de.jstojconverter.util.Log;
import de.jstojconverter.util.StringUtils;
import de.jstojconverter.javacore.*;
import de.jstojconverter.javacore.info.*;

public class StatementConverter {
	
	public static final String LOG_TAG = "StatementConverter";

	private StatementConverter() {
	}
	
	public static void convertStatement(JavaCodeBlock pCodeBlock, String pStatement) throws ConverterException, JavaCoreException {
		String statement = StringUtils.removeSpaceAndTabOutside(pStatement);
		
		if (JsStatementTypeDetection.isDeclarationStatement(statement)) {
			convertDeclarationStatement(pCodeBlock, statement);
			return;
		}
		if (JsStatementTypeDetection.isDeclarationWithAssignmentStatement(statement)) {
			convertDeclarationWithAssignmentStatement(pCodeBlock, statement);
			return;
		}
		if (JsStatementTypeDetection.isAssignmentStatement(statement)) {
			convertAssignmentStatement(pCodeBlock, statement);
			return;
		}
		if (JsStatementTypeDetection.isFunctionInvocationStatement(statement)) {
			MethodInvocationStatementConverter.convertMethodInvocationStatement(pCodeBlock, pStatement);
			return;
		}
		
		throw new ConverterException("Cannot detect statement type");
	}

	private static void convertAssignmentStatement(JavaCodeBlock pCodeBlock,
			String pStatement) throws ConverterException, JavaCoreException {
		String[] data = StatementSplitter.splitAssignmentStatement(pStatement);
		String name = data[0];
		JavaAssignmentOperator operator = JavaAssignmentOperator.getOperator(data[1]);
		String valueString = data[2];
		IJavaValue value = TypeDeclarationChanger.getValueObject(valueString, pCodeBlock);
		
		if (!pCodeBlock.getInfo().existVariable(name)) {
			Log.i(LOG_TAG, "Creating new global variable for undefined variable.");
			pCodeBlock.getParentClass().createField(name, value.getType());
		} else {
			JavaVariableInfo variableInfo = pCodeBlock.getInfo().getVariable(name);
			
			if (variableInfo.getType().equals("undefined")) {
				variableInfo.changeType(value.getType());
			}
			
			// TODO Implement JavaScript feature so that variables can change their type
			if (!value.getType().equals(pCodeBlock.getInfo().getVariable(name).getType())) {
				throw new ConverterException("Type mismatch: Cannot convert from " + value.getType() + 
						" to " + pCodeBlock.getInfo().getVariable(name).getType() + " || Will be fixed with type changing function");
			}
		}
		
		pCodeBlock.createAssignmentStatement(name, operator, value);
	}
	
	private static void convertDeclarationStatement(JavaCodeBlock pCodeBlock, String pStatement) throws ConverterException, JavaInfoException {
		String[] data = StatementSplitter.splitDeclarationStatement(pStatement);
		String name = data[0];
		String type = "undefined";
		pCodeBlock.createDeclarationStatement(name, type);
	}
	
	private static void convertDeclarationWithAssignmentStatement(JavaCodeBlock pCodeBlock,
			String pStatement) throws ConverterException, JavaCoreException {
		String[] data = StatementSplitter.splitDeclarationWithAssignmentStatement(pStatement);
		String name = data[0];
		String valueString = data[1];
		
		IJavaValue value = TypeDeclarationChanger.getValueObject(valueString, pCodeBlock);
		pCodeBlock.createDeclarationStatement(name, value.getType(), value);
	}
}
