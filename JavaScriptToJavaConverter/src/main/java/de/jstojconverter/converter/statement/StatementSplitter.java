package de.jstojconverter.converter.statement;

import de.jstojconverter.javacore.operators.JavaAssignmentOperator;
import de.jstojconverter.util.StringUtils;

public class StatementSplitter {

	private StatementSplitter() {
	}
	
	/**
	 * Splits the assignment statement in two parts.
	 * 
	 * statement[0]: The variable name.
	 * statement[1]: The assignment operator. 
	 * statement[2]: The assignment value.
	 * 
	 * @param pStatement
	 * @return
	 */
	public static String[] splitAssignmentStatement(String pStatement) {
		String[] statementData = new String[3];
		int variableNameEnd = pStatement.indexOf(' ') - 1;
		statementData[0] = pStatement.substring(0, variableNameEnd+1);
		
		String assignmentOperator = StringUtils.getFirstValidValue(pStatement, 0, JavaAssignmentOperator.stringValues());
		statementData[1] = assignmentOperator;
		int operatorIndex = StringUtils.firstValidIndexOf(pStatement, assignmentOperator);
		int operatorLength = assignmentOperator.length();
		statementData[2] = pStatement.substring(operatorIndex + operatorLength, pStatement.length()-1);
		statementData[2] = StringUtils.removeSpaceAndTabOutside(statementData[2]);
		return statementData;
	}

	/**
	 * Splits the declaration statement in just one part.
	 * 
	 * statement[0]: The variable name.
	 * 
	 * @param pStatement
	 * @return
	 */
	public static String[] splitDeclarationStatement(String pStatement) {
		String[] statementData = new String[1];
		String declaration = pStatement.substring(4);
		
		int variableNameEnd = declaration.length() - 2;
		statementData[0] = declaration.substring(0, variableNameEnd+1);
		return statementData;
	}

	/**
	 * Splits the declaration statement in two parts.
	 * 
	 * statement[0]: The variable name.
	 * statement[1]: The variable value.
	 * 
	 * @param pStatement
	 * @return
	 */
	public static String[] splitDeclarationWithAssignmentStatement(String pStatement) {
		String[] statementData = new String[2];
		String declaration = pStatement.substring(4);
		
		int variableNameEnd = declaration.indexOf(' ') - 1;
		statementData[0] = declaration.substring(0, variableNameEnd+1);
		
		int equalSignIndex = declaration.indexOf('=');
		statementData[1] = declaration.substring(equalSignIndex + 1, declaration.length()-1);
		statementData[1] = StringUtils.removeSpaceAndTabOutside(statementData[1]);
		return statementData;
	}
}
