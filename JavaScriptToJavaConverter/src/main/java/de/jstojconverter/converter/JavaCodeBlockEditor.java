package de.jstojconverter.converter;

import java.util.List;

import de.jstojconverter.ConverterException;
import de.jstojconverter.TypeDeclarationChanger;
import de.jstojconverter.converter.statement.StatementConverter;
import de.jstojconverter.javacore.JavaCodeBlock;
import de.jstojconverter.javacore.JavaCoreException;
import de.jstojconverter.javacore.JavaElseCondition;
import de.jstojconverter.javacore.JavaElseIfCondition;
import de.jstojconverter.javacore.JavaIfCondition;
import de.jstojconverter.javacore.JavaWhileLoop;
import de.jstojconverter.javacore.value.IJavaValue;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.util.StringUtils;

/**
 * @author Felix Jordan
 * @since 27.07.2015 - 21:59:42
 * @version 1.0
 */
public class JavaCodeBlockEditor {
	
	private JavaCodeBlock mCodeBlock;
	
	public JavaCodeBlockEditor(JavaCodeBlock pCodeBlock) {
		mCodeBlock = pCodeBlock;
	}
	
	public void addJsContent(JsReaderContent pJsContent) throws JavaCoreException, ConverterException {
		if (pJsContent.getType() == JsReaderContent.TYPE_STATEMENT) {
			convertStatement(pJsContent);
			return;
		}
		if (pJsContent.getType() == JsReaderContent.TYPE_IF_CONDITION) {
			convertIfCondition(pJsContent);
			return;
		}
		if (pJsContent.getType() == JsReaderContent.TYPE_ELSE_CONDITION) {
			convertElseCondition(pJsContent);
			return;
		}
		if (pJsContent.getType() == JsReaderContent.TYPE_ELE_IF_CONDITION) {
			convertElseIfCondition(pJsContent);
			return;
		}
		if (pJsContent.getType() == JsReaderContent.TYPE_WHILE_LOOP) {
			convertWhileLoop(pJsContent);
			return;
		}
		
		if (pJsContent.getType() == JsReaderContent.TYPE_FUNCTION_DECLARATION) {
			throw new JavaCoreException("Cannot add a function to a code block!");
		}
		throw new JavaCoreException("Unknown type of JsContent object (" + pJsContent.getType() + ")");
	}
	
	private void convertWhileLoop(JsReaderContent pJsContent) throws JavaCoreException, ConverterException {
		String text = StringUtils.removeSpaceAndTabOutside(pJsContent.getText().substring(5));
		String condition = StringUtils.removeSpaceAndTabOutside(text.substring(1, text.length()-1));
		IJavaValue conditionValue = TypeDeclarationChanger.getValueObject(condition, mCodeBlock);
		JavaWhileLoop whileLoop = mCodeBlock.createWhile(conditionValue);
		List<JsReaderContent> contentList = pJsContent.getContent();
		
		JavaCodeBlockEditor editor = new JavaCodeBlockEditor(whileLoop);
		for (JsReaderContent content : contentList) {
			editor.addJsContent(content);
		}
	}

	private void convertElseIfCondition(JsReaderContent pJsContent) throws JavaCoreException, ConverterException {
		String text = StringUtils.removeSpaceAndTabOutside(pJsContent.getText().substring(7));
		String condition = StringUtils.removeSpaceAndTabOutside(text.substring(1, text.length()-1));
		IJavaValue conditionValue = TypeDeclarationChanger.getValueObject(condition, mCodeBlock);
		JavaElseIfCondition elseIfCondition = mCodeBlock.createElseIf(conditionValue);
		List<JsReaderContent> contentList = pJsContent.getContent();
		
		JavaCodeBlockEditor editor = new JavaCodeBlockEditor(elseIfCondition);
		for (JsReaderContent content : contentList) {
			editor.addJsContent(content);
		}
	}

	private void convertElseCondition(JsReaderContent pJsContent) throws JavaCoreException, ConverterException {
		JavaElseCondition elseCondition = mCodeBlock.createElse();
		List<JsReaderContent> contentList = pJsContent.getContent();
		
		JavaCodeBlockEditor editor = new JavaCodeBlockEditor(elseCondition);
		for (JsReaderContent content : contentList) {
			editor.addJsContent(content);
		}
	}

	private void convertStatement(JsReaderContent pJsContent) throws JavaCoreException, ConverterException {
		String statement = StringUtils.removeSpaceAndTabOutside(pJsContent.getText());
		StatementConverter.convertStatement(mCodeBlock, statement);
	}
	
	private void convertIfCondition(JsReaderContent pJsContent) throws JavaCoreException, ConverterException {
		String text = StringUtils.removeSpaceAndTabOutside(pJsContent.getText().substring(2));
		String condition = StringUtils.removeSpaceAndTabOutside(text.substring(1, text.length()-1));
		IJavaValue conditionValue = TypeDeclarationChanger.getValueObject(condition, mCodeBlock);
		if (!conditionValue.getType().equals("boolean")) {
			throw new JavaCoreException("Condition type must be type boolean");
		}
		JavaIfCondition ifCondition = mCodeBlock.createIf(conditionValue);
		List<JsReaderContent> contentList = pJsContent.getContent();
		
		JavaCodeBlockEditor editor = new JavaCodeBlockEditor(ifCondition);
		for (JsReaderContent content : contentList) {
			editor.addJsContent(content);
		}
	}
}
