package de.jstojconverter;

public class ConverterException extends Exception {
	
	private static final long serialVersionUID = 82258393006755812L;

	public ConverterException() {
	}

	public ConverterException(String pMessage) {
		super(pMessage);
	}

	public ConverterException(Throwable pThrowable) {
		super(pThrowable);
	}

	public ConverterException(String pMessage, Throwable pThrowable) {
		super(pMessage, pThrowable);
	}
}
