package de.jstojconverter;

import de.jstojconverter.converter.statement.StatementConverter;
import de.jstojconverter.javacore.JavaClass;
import de.jstojconverter.javacore.JavaConstructor;
import de.jstojconverter.javacore.JavaFile;
import de.jstojconverter.javacore.JavaMethod;
import de.jstojconverter.javacore.util.JavaParameter;
import de.jstojconverter.jsreader.JsReaderContent;
import de.jstojconverter.javacore.*;
import de.jstojconverter.converter.*;

public class SplitStatementTest {
	
	public static final int TYPE_DECLERATION = 0;
	public static final int TYPE_ASSIGNMENT = 1;
	public static final int TYPE_METHOD_INVOKATION = 2;
	
	public static void main(String[] args) {
		new SplitStatementTest();
	}

	public SplitStatementTest() {
		
		try {
			JavaFile classFile = new JavaFile("de.jstojconverter.codetest", "CodeTest");
			JavaClass testClass = classFile.getJavaClass();
			JavaMethod mainMethod = testClass.createMethod("public", "static", "main", "void", new JavaParameter("pArgs", "java.lang.String[]"));
			JavaConstructor constructor = testClass.createConstructor();
			
			JavaCodeBlockEditor constructorEditor = new JavaCodeBlockEditor(constructor);
			
			constructorEditor.addJsContent(new JsReaderContent("var foo;", JsReaderContent.TYPE_STATEMENT));
			constructorEditor.addJsContent(new JsReaderContent("var a = 42;", JsReaderContent.TYPE_STATEMENT));
			constructorEditor.addJsContent(new JsReaderContent("var b = 10;", JsReaderContent.TYPE_STATEMENT));
			
			constructorEditor.addJsContent(new JsReaderContent("foo = 'foo';", JsReaderContent.TYPE_STATEMENT));
			constructorEditor.addJsContent(new JsReaderContent("autoGlobal = 'Hello Wolrd!';", JsReaderContent.TYPE_STATEMENT));
			
			constructorEditor.addJsContent(new JsReaderContent("var result = a + b + foo;", JsReaderContent.TYPE_STATEMENT));
			
			constructorEditor.addJsContent(new JsReaderContent("console.log(22 + (b += 20) * 5);", JsReaderContent.TYPE_STATEMENT));
			
			constructorEditor.addJsContent(new JsReaderContent("result = \"boo\";", JsReaderContent.TYPE_STATEMENT));
			constructorEditor.addJsContent(new JsReaderContent("console.log(result);", JsReaderContent.TYPE_STATEMENT));
			//StatementConverter.convertStatement(mVariableTypeMap, "result = 42;");
			
			constructorEditor.addJsContent(new JsReaderContent("a += 21;", JsReaderContent.TYPE_STATEMENT));
			
			constructorEditor.addJsContent(new JsReaderContent("a = 22 + (b += 20) * 5;", JsReaderContent.TYPE_STATEMENT));
			
			// Bracket statement execution
			constructorEditor.addJsContent(new JsReaderContent("var c = (3 + (10 - (3 + 6) + 'foo'));", JsReaderContent.TYPE_STATEMENT));
			
			
			constructorEditor.addJsContent(new JsReaderContent("var test;", JsReaderContent.TYPE_STATEMENT));
			constructorEditor.addJsContent(new JsReaderContent("var d = 50 + (a += 10);", JsReaderContent.TYPE_STATEMENT));
			// Correct statement is executed from left to right
			constructorEditor.addJsContent(new JsReaderContent("var otherTest = test = 5 + 'foo';", JsReaderContent.TYPE_STATEMENT));
			
			
			constructorEditor.addJsContent(new JsReaderContent("window.prompt('foo');", JsReaderContent.TYPE_STATEMENT));
			constructorEditor.addJsContent(new JsReaderContent("var input = window.prompt('foo');", JsReaderContent.TYPE_STATEMENT));
			
			constructorEditor.addJsContent(new JsReaderContent("if (true)", JsReaderContent.TYPE_IF_CONDITION));
			
			
			if (true) {
				System.out.println(classFile.getSourceCode());
				return;
			}
			
			int b;
			int a = b = 10;
			
			StatementConverter.convertStatement(constructor, "var size = object.getSize();");
			
			StatementConverter.convertStatement(constructor, "var testArray = [42, 10, 42 + 10];");
			StatementConverter.convertStatement(constructor, "console.log(testArray);");
			
			StatementConverter.convertStatement(constructor, "window.alert(testArray);");
			
			StatementConverter.convertStatement(constructor, "alert('this is another alert!');");
		} catch (ConverterException e) {
			throw new RuntimeException("Failed to convert JavaScript into Java", e);
		} catch (JavaCoreException e) {
			throw new RuntimeException("Failed to convert JavaScript into Java", e);
		}
	}
}
