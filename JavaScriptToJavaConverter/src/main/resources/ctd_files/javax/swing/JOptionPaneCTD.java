package ctd_files.javax.swing;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 14:23:23
 * @version 1.0
 */
public class JOptionPaneCTD {
	
	private static final String DATA = 
			"CLASS:\n"
			+"public javax.swing.JOptionPane;\n"
			+"\n"
			+"FIELDS:\n"
			+"\n"
			+"CONSTRUCTORS:\n"
			+"\n"
			+"METHODS:\n"
			+"public static void showMessageDialog(java.awt.Component, java.lang.Object);\n"
			+"public static int showConfirmDialog(java.awt.Component, java.lang.Object);\n"
			+"public static java.lang.String showInputDialog(java.awt.Component, java.lang.Object);\n"
			+"\n"
			+"INNER-CLASSES:";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}