package ctd_files.java.lang;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 14:23:23
 * @version 1.0
 */
public class SystemCTD {
	
	private static final String DATA = 
			"CLASS:\n"
			+"public final java.lang.System;\n"
			+"\n"
			+"FIELDS:\n"
			+"public static final java.io.PrintStream out;\n"
			+"public static final java.io.PrintStream err;\n"
			+"\n"
			+"CONSTRUCTORS:\n"
			+"\n"
			+"METHODS:\n"
			+"\n"
			+"INNER-CLASSES:";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}