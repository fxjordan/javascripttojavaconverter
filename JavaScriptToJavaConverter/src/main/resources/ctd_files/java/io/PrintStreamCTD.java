package ctd_files.java.io;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 14:23:23
 * @version 1.0
 */
public class PrintStreamCTD {
	
	private static final String DATA = 
			"CLASS:\n"
			+"public final java.io.PrintStream;\n"
			+"\n"
			+"FIELDS:\n"
			+"\n"
			+"CONSTRUCTORS:\n"
			+"\n"
			+"METHODS:\n"
			+"public void print(short);\n"
			+"public void print(int);\n"
			+"public void print(long);\n"
			+"public void print(float);\n"
			+"public void print(double);\n"
			+"public void print(boolean);\n"
			+"public void print(char);\n"
			+"public void print(java.lang.String);\n"
			+"public void println(short);\n"
			+"public void println(int);\n"
			+"public void println(long);\n"
			+"public void println(float);\n"
			+"public void println(double);\n"
			+"public void println(boolean);\n"
			+"public void println(char);\n"
			+"public void println(java.lang.String);\n"
			+"\n"
			+"\n"
			+"INNER-CLASSES:";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}