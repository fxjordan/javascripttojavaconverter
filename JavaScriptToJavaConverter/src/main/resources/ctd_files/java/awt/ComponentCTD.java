package ctd_files.java.awt;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 14:23:23
 * @version 1.0
 */
public class ComponentCTD {
	
	private static final String DATA = 
			"CLASS:\n"
			+"public java.awt.Component;\n"
			+"\n"
			+"FIELDS:\n"
			+"\n"
			+"CONSTRUCTORS:\n"
			+"\n"
			+"METHODS:\n"
			+"\n"
			+"INNER-CLASSES:";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}