package ctd_files;


import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 14:23:23
 * @version 1.0
 */
public class ClassesLIST {
	
	private static final String DATA = 
			"de/jstojconverter/jswrapper/Global\n"
			+"de/jstojconverter/jswrapper/Number\n"
			+"java/awt/Component\n"
			+"java/io/PrintStream\n"
			+"java/lang/Object\n"
			+"java/lang/String\n"
			+"java/lang/Double\n"
			+"java/lang/System\n"
			+"javax/swing/JOptionPane";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}