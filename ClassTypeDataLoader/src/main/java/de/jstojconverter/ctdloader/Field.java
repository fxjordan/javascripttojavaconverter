package de.jstojconverter.ctdloader;

public class Field {
	
	private String mVisibility;
	private String[] mModifiers;
	private String mType;
	private String mName;
	
	protected Field(String pVisibility, String[] pModifiers, String pType, String pName) {
		mVisibility = pVisibility;
		mModifiers = pModifiers;
		mType = pType;
		mName = pName;
	}
	
	public String getVisibility() {
		return mVisibility;
	}
	
	public String[] getModifiers() {
		return mModifiers;
	}
	
	public String getType() {
		return mType;
	}
	
	public String getName() {
		return mName;
	}
}
