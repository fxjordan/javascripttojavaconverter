package de.jstojconverter.ctdloader;

import java.util.ArrayList;
import java.util.List;

public class Class {
	
	private String mPackage;
	private String mName;
	private String mVisibility;
	private String[] mModifiers;
	private List<Field> mFields;
	private List<Constructor> mConstructors;
	private List<Method> mMethods;
	private List<Class> mInnerClasses;

	protected Class(String pPackage, String pName, String pVisibility, String[] pModifiers) {
		mPackage = pPackage;
		mName = pName;
		mVisibility = pVisibility;
		mModifiers = pModifiers;
		mFields = new ArrayList<Field>();
		mConstructors = new ArrayList<Constructor>();
		mMethods = new ArrayList<Method>();
		mInnerClasses = new ArrayList<Class>();
	}
	
	public String getPackage() {
		return mPackage;
	}
	
	public String getName() {
		return mName;
	}
	
	public String getVisibility() {
		return mVisibility;
	}
	
	public String[] getModifiers() {
		return mModifiers;
	}
	
	public List<Field> getFields() {
		return mFields;
	}
	
	public List<Constructor> getConstructors() {
		return mConstructors;
	}
	
	public List<Method> getMethods() {
		return mMethods;
	}
	
	public List<Class> getInnerClasses() {
		return mInnerClasses;
	}
	
	protected void addField(Field pField) {
		mFields.add(pField);
	}
	
	protected void addConstructor(Constructor pConstructor) {
		mConstructors.add(pConstructor);
	}
	
	protected void addMethod(Method pMethod) {
		mMethods.add(pMethod);
	}
	
	protected void addInnerClass(Class pClass) {
		mInnerClasses.add(pClass);
	}
}
