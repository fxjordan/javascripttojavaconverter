package de.jstojconverter.ctdloader;

import java.util.ArrayList;
import java.util.List;

public class Method {
	
	private String mVisibility;
	private String[] mModifiers;
	private String mReturnType;
	private String mName;
	private String[] mParameters;

	protected Method(String pVisibility, String[] pModifiers, String pReturnType, String pName, String[] pParameters) {
		mVisibility = pVisibility;
		mModifiers = pModifiers;
		mReturnType = pReturnType;
		mName = pName;
		List<String> parameters = new ArrayList<String>();
		for (String parameter : pParameters) {
			if (!parameter.isEmpty()) {
				parameters.add(parameter);
			}
		}
		mParameters = new String[parameters.size()];
		mParameters = parameters.toArray(mParameters);
	}
	
	public String getVisibility() {
		return mVisibility;
	}
	
	public String[] getModifiers() {
		return mModifiers;
	}
	
	public String getReturnType() {
		return mReturnType;
	}
	
	public String getName() {
		return mName;
	}
	
	public String[] getParameters() {
		return mParameters;
	}
}
