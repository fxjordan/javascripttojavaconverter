package de.jstojconverter.ctdloader;

import java.util.ArrayList;
import java.util.List;

public class Constructor {

	private String mVisibility;
	private String mName;
	private String[] mParameters;

	protected Constructor(String pVisibility, String pName, String[] pParameters) {
		mVisibility = pVisibility;
		mName = pName;
		List<String> parameters = new ArrayList<String>();
		for (String parameter : pParameters) {
			if (!parameter.isEmpty()) {
				parameters.add(parameter);
			}
		}
		mParameters = new String[parameters.size()];
		mParameters = parameters.toArray(mParameters);
	}
	
	public String getVisibility() {
		return mVisibility;
	}
	
	public String getName() {
		return mName;
	}
	
	public String[] getParameters() {
		return mParameters;
	}
}
