package de.jstojconverter.ctdloader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import de.jstojconverter.util.Log;
import de.jstojconverter.util.StringUtils;

public class ClassTypeDataLoader {
	
	public static final String LOG_TAG = "ClassTypeDataLoader";
	
	private static final String FIELDS_TAG = "FIELDS:";
	private static final String CONSTRUCTORS_TAG = "CONSTRUCTORS:";
	private static final String METHODS_TAG = "METHODS:";
	private static final String INNER_CLASSES_TAG = "INNER-CLASSES:";
	
	private String mContent;
	
	public ClassTypeDataLoader(InputStream pInputStream) {
		StringBuilder contentBuilder = new StringBuilder();
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(pInputStream));
			String line = null;
			while ((line = reader.readLine()) != null) {
				contentBuilder.append(line);
			}
			mContent = contentBuilder.toString();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Cannot load ctd file", e);
		} catch (IOException e) {
			throw new RuntimeException("Cannot load ctd file", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Log.e(LOG_TAG, "Cannot close InputStream");
					e.printStackTrace();
				}
			}
		}
	}

	public ClassTypeDataLoader(File pFile) {
		StringBuilder contentBuilder = new StringBuilder();
		BufferedReader reader = null;
		try {
			InputStream in = new FileInputStream(pFile);
			reader = new BufferedReader(new InputStreamReader(in));
			String line = null;
			while ((line = reader.readLine()) != null) {
				contentBuilder.append(line);
			}
			mContent = contentBuilder.toString();
		} catch (FileNotFoundException e) {
			throw new RuntimeException("Cannot load ctd file", e);
		} catch (IOException e) {
			throw new RuntimeException("Cannot load ctd file", e);
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					Log.e(LOG_TAG, "Cannot close InputStream");
					e.printStackTrace();
				}
			}
		}
	}
	
	public Class parseClass() {
		Log.d(LOG_TAG, "====================");
		int firstSemicolon = mContent.indexOf(';');
		String classInfo = StringUtils.removeSpaceAndTabOutside(mContent.substring(6, firstSemicolon));
		Class clazz = createClassObject(classInfo);
		
		int fieldStartIndex = mContent.indexOf(FIELDS_TAG);
		if (fieldStartIndex == -1) {
			throw new RuntimeException("Missing 'FIELDS:' tag in ctd file!");
		}
		fieldStartIndex += FIELDS_TAG.length();
		
		int fieldEndIndex = mContent.indexOf(CONSTRUCTORS_TAG);
		if (fieldEndIndex == -1) {
			throw new RuntimeException("Missing 'CONSTRUCTORS:' tag in ctd file!");
		}
		fieldEndIndex -= 1;
		
		String fieldData = StringUtils.removeSpaceAndTabOutside(mContent.substring(fieldStartIndex, fieldEndIndex + 1));
		parseFields(clazz, fieldData);
		
		fieldEndIndex += CONSTRUCTORS_TAG.length() + 1;
		
		int constructorEndIndex = mContent.indexOf(METHODS_TAG);
		if (constructorEndIndex == -1) {
			throw new RuntimeException("Missing 'METHODS:' tag in ctd file!");
		}
		constructorEndIndex -= 1;
		
		if (constructorEndIndex - fieldEndIndex > 1) {
			String constructorData = StringUtils.removeSpaceAndTabOutside(mContent.substring(fieldEndIndex, constructorEndIndex));
			parseConstructors(clazz, constructorData);
		}
		
		constructorEndIndex += METHODS_TAG.length() + 1;
		
		int methodEndIndex = mContent.indexOf(INNER_CLASSES_TAG);
		if (methodEndIndex == -1) {
			throw new RuntimeException("Missing 'INNER-CLASSES:' tag in ctd file!");
		}
		methodEndIndex -= 1;
		
		if (methodEndIndex - constructorEndIndex > 1) {
			String methodData = StringUtils.removeSpaceAndTabOutside(mContent.substring(constructorEndIndex, methodEndIndex));
			parseMethods(clazz, methodData);
		}
		
		return clazz;
	}
	
	private void parseMethods(Class pClass, String pMethodData) {
		Log.d(LOG_TAG, "Parsing methods...");
		String[] methods = pMethodData.split(";");
		for (int i=0; i<methods.length; i++) {
			methods[i] = StringUtils.removeSpaceAndTabOutside(methods[i]);
			Method method = createMethod(methods[i]);
			pClass.addMethod(method);
		}
	}
	
	private Method createMethod(String pMethodString) {
		Log.d(LOG_TAG, "==");
		int parametersStartIndex = pMethodString.indexOf('(');
		String[] parts = pMethodString.substring(0, parametersStartIndex).split(" ");
		String name = parts[parts.length-1];
		Log.d(LOG_TAG, "Method name: " + name);
		
		String returnType = parts[parts.length-2];
		Log.d(LOG_TAG, "Method return type: " + returnType);
		
		String parameterData = pMethodString.substring(parametersStartIndex+1, pMethodString.length()-1);
		String[] parameters = parameterData.split(",");
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Parameters: ");
		for (int i=0; i<parameters.length; i++) {
			parameters[i] = StringUtils.removeSpaceAndTabOutside(parameters[i]);
			messageBuilder.append(" " + parameters[i]);
		}
		Log.d(LOG_TAG, messageBuilder.toString());
		
		if (parts.length == 2) {
			return new Method(null, null, returnType, name, parameters);
		}
		
		String visibility = parts[0];
		Log.d(LOG_TAG, "Visibility: " + visibility);
		if (parts.length == 3) {
			return new Method(visibility, null, returnType, name, parameters);
		}
		
		String[] modifiers = new String[parts.length-3];
		messageBuilder.setLength(0);
		messageBuilder.append("Modifiers:");
		for (int i=0; i<modifiers.length; i++) {
			modifiers[i] = StringUtils.removeSpaceAndTabOutside(parts[i+1]);
			messageBuilder.append(" " + modifiers[i]);
		}
		Log.d(LOG_TAG, messageBuilder.toString());
		return new Method(visibility, modifiers, returnType, name, parameters);
	}
	
	private void parseConstructors(Class pClass, String pConstructorData) {
		Log.d(LOG_TAG, "Parsing constructors...");
		String[] constructors = pConstructorData.split(";");
		for (int i=0; i<constructors.length; i++) {
			constructors[i] = StringUtils.removeSpaceAndTabOutside(constructors[i]);
			Constructor constructor = createConstructor(constructors[i]);
			pClass.addConstructor(constructor);
		}
	}

	private Constructor createConstructor(String pConstructorString) {
		Log.d(LOG_TAG, "==");
		int parametersStartIndex = pConstructorString.indexOf('(');
		String[] parts = pConstructorString.substring(0, parametersStartIndex).split(" ");
		String type = parts[parts.length-1];
		Log.d(LOG_TAG, "Constructor type: " + type);
		
		String parameterData = pConstructorString.substring(parametersStartIndex+1, pConstructorString.length()-1);
		String[] parameters = parameterData.split(",");
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Parameters:");
		for (int i=0; i<parameters.length; i++) {
			parameters[i] = StringUtils.removeSpaceAndTabOutside(parameters[i]);
			messageBuilder.append(" " + parameters[i]);
		}
		Log.d(LOG_TAG, messageBuilder.toString());
		
		if (parts.length == 1) {
			return new Constructor(null, type, parameters);
		}
		
		String visibility = parts[0];
		Log.d(LOG_TAG, "Visibility: " + visibility);
		return new Constructor(visibility, type, parameters);
	}

	private void parseFields(Class pClass, String pFieldData) {
		Log.d(LOG_TAG, "Parsing fields...");
		String[] fields = pFieldData.split(";");
		for (int i=0; i<fields.length; i++) {
			fields[i] = StringUtils.removeSpaceAndTabOutside(fields[i]);
			if (fields[i].isEmpty()) {
				continue;
			}
			Field field = createField(fields[i]);
			pClass.addField(field);
		}
	}
	
	private Field createField(String pFieldString) {
		Log.d(LOG_TAG, "==");
		String[] parts = pFieldString.substring(0, pFieldString.length()).split(" ");
		String name = parts[parts.length-1];
		Log.d(LOG_TAG, "Field name: " + name);
		String type = parts[parts.length-2];
		Log.d(LOG_TAG, "Field type: " + type);
		
		if (parts.length == 2) {
			return new Field(null, null, type, name);
		}
		
		String visibility = parts[0];
		Log.d(LOG_TAG, "Visibility: " + visibility);
		if (parts.length == 3) {
			return new Field(visibility, null, type, name);
		}
		
		String[] modifiers = new String[parts.length-3];
		StringBuilder messageBuilder = new StringBuilder();
		messageBuilder.append("Modifiers:");
		for (int i=0; i<modifiers.length; i++) {
			modifiers[i] = StringUtils.removeSpaceAndTabOutside(parts[i+1]);
			messageBuilder.append(" " + modifiers[i]);
		}
		Log.d(LOG_TAG, messageBuilder.toString());
		return new Field(visibility, modifiers, type, name);
	}

	private Class createClassObject(String pClassInfo) {
		Log.d(LOG_TAG, "Parsing new class type data...");
		String[] infoParts = pClassInfo.split(" ");
		String fullClassName = StringUtils.removeSpaceAndTabOutside(infoParts[infoParts.length-1]);
		
		int lastDotIndex = fullClassName.lastIndexOf('.');
		String className = fullClassName.substring(lastDotIndex+1);
		Log.d(LOG_TAG, "Class name: " + className);
		String classPackage = fullClassName.substring(0, lastDotIndex);
		Log.d(LOG_TAG, "Package name: " + classPackage);
		
		if (infoParts.length == 1) {
			return new Class(classPackage, className, null, null);
		}
		
		String visibility = StringUtils.removeSpaceAndTabOutside(infoParts[0]);
		Log.d(LOG_TAG, "Visibility: " + visibility);
		if (infoParts.length == 2) {
			return new Class(classPackage, className, visibility, null);
		}
		
		String[] modifiers = new String[infoParts.length-2];
		StringBuilder messagebuBuilder = new StringBuilder();
		messagebuBuilder.append("Modifiers:");
		for (int i=0; i<modifiers.length; i++) {
			modifiers[i] = StringUtils.removeSpaceAndTabOutside(infoParts[i+1]);
			messagebuBuilder.append(" " + modifiers[i]);
		}
		Log.d(LOG_TAG, messagebuBuilder.toString());
		return new Class(classPackage, className, visibility, modifiers);
	}
}
