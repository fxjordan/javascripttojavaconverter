package de.jstojconverter.ctdloader;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import de.jstojconverter.util.JavaTypeUtils;
import de.jstojconverter.util.ThreeMethodLoader;

public class CTDManager extends ThreeMethodLoader<Class> {
	
	public static final String LOG_TAG = "CTDManager";
	
	private static final String DATA_FILE_ENDING = ".ctd";
	private static final String LIST_FILE_NAME = "classes.list";
	private static final String DATA_CLASS_NAME_ENDINGS = "CTD";
	private static final String LIST_CLASS_NAME = "ClassesLIST";
	
	private Map<String, List<Class>> mClassDataMap;

	public CTDManager() {
		super(DATA_FILE_ENDING, LIST_FILE_NAME, DATA_CLASS_NAME_ENDINGS, LIST_CLASS_NAME);
		mClassDataMap = new HashMap<String, List<Class>>();
	}
	
	@Override
	public List<Class> onPreLoadFolder(String pExternalFolder, String pResourcePath) {
		String key = getKey(pExternalFolder, pResourcePath);
		if (mClassDataMap.containsKey(key)) {
			return mClassDataMap.get(key);
		}
		return null;
	}

	@Override
	public Class onProcessLoadedItem(InputStream pInputStream) {
		return new ClassTypeDataLoader(pInputStream).parseClass();
	}

	@Override
	public void onPostLoadFolder(List<Class> pResult, String pExternalFolder, String pResourcePath) {
		mClassDataMap.put(getKey(pExternalFolder, pResourcePath), pResult);
	}
	
	private String getKey(String pExternalFolder, String pResourcePath) {
		return pExternalFolder + "&&&" + pResourcePath;
	}
	
	public Class getForName(String pName) {
		Iterator<List<Class>> iterator = mClassDataMap.values().iterator();
		while (iterator.hasNext()) {
			List<Class> classes = iterator.next();
			for (Class clazz : classes) {
				if (JavaTypeUtils.composeType(clazz.getPackage(), clazz.getName()).equals(pName)) {
					return clazz;
				}
			}
		}
		return null;
	}
}
