package ctd_test_files;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 23:36:51
 * @version 1.0
 */
public class ClassesLIST {
	
	private static final String DATA = 
			"java/lang/Object\n"
			+ "java/lang/String";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}

