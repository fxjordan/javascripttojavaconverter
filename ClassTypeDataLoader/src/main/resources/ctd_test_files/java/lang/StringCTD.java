package ctd_test_files.java.lang;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 23:31:43
 * @version 1.0
 */
public class StringCTD {
	
	private static final String DATA = 
			"CLASS:\n"
			+ "public final java.lang.String;\n"
			+ "\n"
			+ "FIELDS:\n"
			+ "\n"
			+ "CONSTRUCTORS:\n"
			+ "public String();\n"
			+ "public String(String);\n"
			+ "\n"
			+ "METHODS:\n"
			+ "public char charAt(int);\n"
			+ "public int indexOf(char);\n"
			+ "public int indexOf(String);\n"
			+ "\n"
			+ "INNER-CLASSES:";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}

