package ctd_test_files.java.lang;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

/**
 * Utility class if the project is used with AIDE, because there can be not
 * file read.
 * 
 * @author Felix Jordan
 * @since 23.07.2015 - 23:33:54
 * @version 1.0
 */
public class ObjectCTD {
	
	private static final String DATA = 
			"CLASS:\n"
			+ "public java.lang.Object;\n"
			+ "\n"
			+ "FIELDS:\n"
			+ "private static final String SUPER_FOO;\n"
			+ "\n"
			+ "private String mFoo;\n"
			+ "private int mBoo;\n"
			+ "\n"
			+ "CONSTRUCTORS:\n"
			+ "public Object();\n"
			+ "\n"
			+ "METHODS:\n"
			+ "public boolean equals(Object);\n"
			+ "public String toString();\n"
			+ "\n"
			+ "INNER-CLASSES:\n";
	
	public static InputStream getData() {
		return new ByteArrayInputStream(DATA.getBytes(Charset.forName("UTF-8")));
	}
}


