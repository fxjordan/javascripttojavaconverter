package de.jstojconverter.ctdloader;

public class ClassTypeDataLoaderTest {
	
	public static final String LOG_TAG = "ClassTypeDataLoaderTest";
	
	private static final String EXTERNAL_FOLDER = "C:/Users/Felix/git/JavaScriptToJavaConverter/"
			+ "ClassTypeDataLoader/src/main/resources/ctd_test_files";
	private static final String INTERNAL_RESOURCE_PATH = "ctd_test_files";
	
	public static void main(String[] pArgs) {
		new ClassTypeDataLoaderTest();
	}
	
	public ClassTypeDataLoaderTest() {
		CTDManager ctdManager = new CTDManager();
		
		ctdManager.load(EXTERNAL_FOLDER, INTERNAL_RESOURCE_PATH);
	}
}
